# -*- coding: utf-8 -*-

import os
import sys

dirname = os.path.dirname(os.path.abspath(__file__))
sys.path.append(dirname)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")

from wsgi import application