# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.models import Group

from profile.models import Auth, Profile
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import (
    AdminPasswordChangeForm, UserChangeForm, UserCreationForm,
)
from django.contrib.auth.admin import UserAdmin


class ProfileInline(admin.StackedInline):
    model = Profile
    fk_name = 'auth'
    can_delete = False
    max_num = 1
    verbose_name_plural = ''

    suit_classes = 'suit-tab suit-tab-profile'

    fieldsets = [
        ('  ', {
                'fields': [
                    'email',
                ]
            }
        ),
        ('  ', {
                'fields': [
                    'first_name',
                    'middle_name',
                    'last_name',
                ]
            }
        ),
    ]



class AuthAdmin(UserAdmin):
    inlines = [ProfileInline]

    form = UserChangeForm
    add_form = UserCreationForm
    change_password_form = AdminPasswordChangeForm
    filter_horizontal = ('groups', 'user_permissions',)


    def get_form(self, request, obj=None, **kwargs):
        """
        Use special form during user creation
        """
        defaults = {}
        if obj is None:
            defaults['form'] = self.add_form
        defaults.update(kwargs)
        return super(AuthAdmin, self).get_form(request, obj, **defaults)


    fieldsets = [
        (None, {
                'classes': ('suit-tab', 'suit-tab-general',),
                'fields': [
                    'username',
                    'email',
                    'password',
                ]
            }
        ),
        (None, {
                'classes': ('suit-tab', 'suit-tab-general',),
                'fields': [
                    'is_superuser',
                    'is_staff',
                    'is_active',
                ]
            }
        ),
        (None, {
                'classes': ('suit-tab', 'suit-tab-general',),
                'fields': [
                    'last_login',
                ]
            }
        ),
        (None, {
                'classes': ('suit-tab', 'suit-tab-general',),
                'fields': [
                    'groups',
                    'user_permissions',
                ]
            }
        ),
    ]

    list_display = ('username', 'email', 'get_profile_info', 'last_login', 'is_active')

    suit_form_tabs = (('general', 'Аутентификационные данные'), ('profile', 'Данные профиля'),)

    def get_profile_info(self, obj):
        return '%s %s' % (obj.profile.first_name, obj.profile.last_name)
    get_profile_info.short_description = _('username')

admin.site.register(Auth, AuthAdmin)



# class AuthGroupAdmin(admin.ModelAdmin):
#     fieldsets = [
#         (None, {
#                 'classes': ('suit-tab', 'suit-tab-general',),
#                 'fields': [
#                     'name',
#                     'slug',
#                     'permissions',
#                 ]
#             }
#         ),
#     ]

#     list_display = ('name',)

# admin.site.register(AuthGroup, AuthGroupAdmin)



# admin.site.unregister(Group)