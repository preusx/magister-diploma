# -*- coding: utf-8 -*-
from datetime import datetime

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin,\
    BaseUserManager, GroupManager, Group
from django.core.mail import send_mail
from django.db import models as m
from django.core import validators
from django.utils.translation import ugettext_lazy as _



class AuthGroupManager(GroupManager):
    """
    The manager for the profile's Group model.
    """
    use_in_migrations = True

    def get_by_natural_key(self, slug):
        return self.get(slug=slug)



class AuthGroup(Group):
    """
    Some rework of the auth Group model.
    """
    name = m.CharField(_('name'), max_length=80)
    slug = m.SlugField(_('slug'), max_length=80, unique=True)

    objects = AuthGroupManager()


    class Meta:
        app_label = 'profile'


    def natural_key(self):
        return (self.slug,)