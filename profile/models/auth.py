# -*- coding: utf-8 -*-
from datetime import datetime

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin,\
    BaseUserManager
from django.core.mail import send_mail
from django.db import models as m
from django.core import validators
from django.utils.translation import ugettext_lazy as _



class AuthManager(BaseUserManager):
    # def _create_user(self, email, password,
    #                  is_staff, is_superuser, **extra_fields):
    #     """
    #     Creates and saves a User with the given email and password.
    #     """
    #     now = datetime.now()
    #     if not email:
    #         raise ValueError('The given email must be set')
    #     email = self.normalize_email(email)
    #     user = self.model(email=email, is_staff=is_staff, is_active=True,
    #         is_superuser=is_superuser, last_login=now, **extra_fields)
    #     user.set_password(password)
    #     user.save(using=self._db)

    #     return user


    # def create_user(self, email, password=None, **extra_fields):
    #     return self._create_user(email, password, False, False,
    #                              **extra_fields)


    # def create_superuser(self, email, password, **extra_fields):
    #     return self._create_user(email, password, True, True,
    #                              **extra_fields)

    def _create_user(self, username, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        user = self.model(username=username, email=email,
                          is_staff=is_staff, is_superuser=is_superuser,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        return self._create_user(username, email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, username, email=None, password=None, **extra_fields):
        return self._create_user(username, email, password, True, True,
                                 **extra_fields)



class AuthAbstract(AbstractBaseUser, PermissionsMixin):
    username = m.CharField(
    _('username'),
        max_length=30,
        unique=True,
        help_text=_('Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                _('Enter a valid username. This value may contain only '
                  'letters, numbers ' 'and @/./+/-/_ characters.')
            ),
        ],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    email = m.EmailField(_('email address'), blank=True)

    is_staff = m.BooleanField(_('staff status'), default=False,
         help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = m.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))

    objects = AuthManager()
    backend = 'django.contrib.auth.backends.ModelBackend'

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []


    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True


    def get_full_name(self):
        """
        Returns the email.
        """
        return self.email


    def get_short_name(self):
        """
        Returns the email.
        """
        return self.email


    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])


    def is_authenticated(self):
        return True



class Auth(AuthAbstract):
    """
    Concrete class of AuthAbstract.

    Use this if you don't need to extend AuthAbstract.
    """

    class Meta(AuthAbstract.Meta):
        app_label = 'profile'
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'
        swappable = 'AUTH_USER_MODEL'