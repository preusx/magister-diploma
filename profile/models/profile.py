# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.utils.translation import ugettext_lazy as _
from django.conf import settings



class Profile(m.Model):
    first_name = m.CharField(u'Имя', max_length = 150, null=True, blank=True)
    middle_name = m.CharField(u'Отчество', max_length = 150, null=True, blank=True)
    last_name = m.CharField(u'Фамилия', max_length = 150, null=True, blank=True)

    email = m.EmailField(_(u'email address'), blank=True, max_length=254)

    auth = m.OneToOneField(settings.AUTH_USER_MODEL,
        related_name='profile',
        unique=True,
        verbose_name=_(u'user'),
        parent_link=True,
        on_delete=m.SET_NULL,
        null=True)

    class Meta():
        verbose_name = u'Профиль'
        verbose_name_plural = u'Профили'
        app_label = 'profile'