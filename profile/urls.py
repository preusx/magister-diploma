# -*- coding: utf-8 -*-
from django.conf.urls import include, url, patterns
from django.contrib.auth import views as auth_views

from .views import login

urlpatterns = [
    url('^auth/', include([
        url('^social/', include('social.apps.django_app.urls', namespace='social')),
        url(r'^login/$', login, name='login'),
        url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    ])),
]

