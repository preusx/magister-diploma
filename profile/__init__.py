# -*- coding: utf-8 -*-
from django.apps import AppConfig

default_app_config = 'profile.ProfileAppConfig'


class ProfileAppConfig(AppConfig):
    name = 'profile'
    verbose_name = u'Профиль'
