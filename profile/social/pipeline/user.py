# -*- coding: utf-8 -*-
import datetime
from profile.models import Profile
from django.contrib.auth.models import Group
from django.contrib.auth import logout


def user_profile_data(backend, user, response, details, *args, **kwargs):
    """Update user details using data from provider."""

    if user:
        is_new = kwargs.get('is_new')

        if is_new:
            user.groups.add(Group.objects.get(name='user'))

            attrs = {'auth': user}

            if backend.name == 'vk-oauth2':
                # We should check values before this, but for the example
                # is fine
                vk_data = {
                    'first_name': response['first_name'],
                    'last_name': response['last_name'],
                }
                attrs = dict(attrs.items() + vk_data.items())

            Profile.objects.create(
                **attrs
            )



def logout_existing(backend, uid, user=None, *args, **kwargs):
    provider = backend.name
    social = backend.strategy.storage.user.get_social_auth(provider, uid)
    if social:
        if user and social.user != user:
            logout(backend.strategy.request)
            #msg = 'This {0} account is already in use.'.format(provider)
            #raise AuthAlreadyAssociated(backend, msg)
        elif not user:
            user = social.user
    return {'social': social,
            'user': user,
            'is_new': user is None,
            'new_association': False}