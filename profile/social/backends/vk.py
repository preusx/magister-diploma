# -*- coding: utf-8 -*-

from social.backends import vk

class VKOAuth2WithEmail(vk.VKOAuth2):
    AUTHORIZATION_URL = 'http://oauth.vk.com/authorize?scope=email'