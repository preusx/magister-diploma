from django.shortcuts import render

from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.contrib.auth import login as auth_login
from django.contrib.auth.forms import AuthenticationForm

from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status

from api.serializers import UserSerializer

# Create your views here.

@sensitive_post_parameters()
@csrf_protect
@never_cache
@api_view(['POST'])
@permission_classes([])
def login(request):
    print(request.DATA)

    form = AuthenticationForm(request, data=request.DATA)

    if form.is_valid():
        # Okay, security check complete. Log the user in.
        auth_login(request, form.get_user())

        serializer = UserSerializer(form.get_user())

        return Response(serializer.data)
    else:
        return Response({}, status=status.HTTP_401_UNAUTHORIZED)
