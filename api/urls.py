# -*- coding: utf-8 -*-
from django.conf.urls import include, url

from rest_framework import routers
import api.views as v
import api.models as m

router = routers.DefaultRouter()

router.register(r'specialization', v.SpecializationView, base_name='api.specialization')
# router.register(r'speciality', v.SpecialityView, base_name='api.speciality')
router.register(r'discipline', v.DisciplineView, base_name='api.discipline')
router.register(r'user_discipline_list', v.UserDisciplineListView, base_name='api.user_discipline_list')

urlpatterns = [
    url(r'^v1/', include([
        url(r'^', include(router.urls)),
        url(r'^user/', v.user),
    ])),
]
