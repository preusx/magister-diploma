# -*- coding: utf-8 -*-
from __future__ import absolute_import
from rest_framework import serializers as s

import profile.models as m
from .base import DetailSerializer



class ProfileSerializer(DetailSerializer):
    class Meta:
        model = m.Profile

        fields = ('id', 'first_name', 'middle_name', 'last_name')
        depth = 0



class UserSerializer(DetailSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = m.Auth

        fields = ('id', 'username', 'email', 'profile')
        depth = 1







