# -*- coding: utf-8 -*-
from rest_framework import serializers as s

import curriculum.models as m
from .base import DetailSerializer, ListSerializer
from .discipline import DisciplineSpecializationListSerializer
from .speciality import SpecialityDetailSerializer



class SpecializationListSerializer(ListSerializer):
    class Meta:
        model = m.Specialization

        fields = ('id', 'name', 'hint', 'description')
        depth = 0



class SpecializationDetailSerializer(DetailSerializer):
    speciality = SpecialityDetailSerializer(many=False)
    relations = DisciplineSpecializationListSerializer(many=True)

    class Meta:
        model = m.Specialization

        fields = ('id', 'name', 'hint', 'description', 'speciality', 'relations')
        depth = 2







