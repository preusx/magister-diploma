# -*- coding: utf-8 -*-
from rest_framework import serializers as s

import curriculum.models as m
from .base import DetailSerializer, ListSerializer
from .discipline import DisciplineSpecialityListSerializer



class SpecialityListSerializer(ListSerializer):
    class Meta:
        model = m.Speciality

        fields = ('id', 'name', 'hint', 'description')
        depth = 0



class SpecialityDetailSerializer(DetailSerializer):
    relations = DisciplineSpecialityListSerializer(many=True)

    class Meta:
        model = m.Speciality

        fields = ('id', 'name', 'hint', 'description', 'relations')
        depth = 2







