# -*- coding: utf-8 -*-
from rest_framework import serializers as s

import curriculum.models as m
from .base import DetailSerializer, ListSerializer


# class RelationField(s.Field):
#     def to_representation(self, obj):
#         return "rgb(%d, %d, %d)" % (obj.red, obj.green, obj.blue)

#     def to_internal_value(self, data):
#         data = data.strip('rgb(').rstrip(')')
#         red, green, blue = [int(col) for col in data.split(',')]
#         return Color(red, green, blue)



class DisciplineListSerializer(ListSerializer):
    class Meta:
        model = m.Discipline
        fields = ('id', 'name', 'hint', 'description')
        depth = 1



class DisciplineListParentSerializer(ListSerializer):
    id = s.IntegerField(source='out_of.id')
    name = s.CharField(source='out_of.name')
    hint = s.CharField(source='out_of.hint')

    class Meta:
        model = m.DisciplineRelation
        fields = ('id', 'name', 'hint', 'type')
        depth = 1



class DisciplineListChildrenSerializer(DisciplineListParentSerializer):
    id = s.IntegerField(source='into.id')
    name = s.CharField(source='into.name')
    hint = s.CharField(source='into.hint')



class DisciplineListNotionSerializer(ListSerializer):
    disciplinegoal = DisciplineListSerializer(many=True)
    disciplineobject = DisciplineListSerializer(many=True)

    class Meta:
        model = m.Notion
        fields = ('id', 'name', 'disciplinegoal', 'disciplineobject')
        depth = 1



class DisciplineDetailSerializer(DetailSerializer):
    # relations = DisciplineListSerializer(many=True, read_only=True)

    # parent = DisciplineListParentSerializer(many=True, read_only=True, source='children')
    # children = DisciplineListChildrenSerializer(many=True, read_only=True, source='parent')

    class Meta:
        model = m.Discipline

        fields = ('id', 'name', 'hint', 'description') #, 'parent', 'children')
        depth = 2



class DisciplineSpecialityConnectionSerializer(ListSerializer):
    class Meta:
        model = m.SpecialityToDiscipline
        fields = ('type',  'weight', 'semester_from', 'semester_to', 'category',)
        depth = 1



class DisciplineSpecialityListSerializer(DisciplineDetailSerializer):
    goal = DisciplineListNotionSerializer(many=True)
    object = DisciplineListNotionSerializer(many=True)
    c = DisciplineSpecialityConnectionSerializer(many=True, source='speciality_connection')

    class Meta(DisciplineDetailSerializer.Meta):
        fields = ('id', 'name', 'hint', 'description', 'object', 'goal',
            # 'parent', 'children',
            'c',)
        depth = 2



class DisciplineSpecializationListSerializer(DisciplineSpecialityListSerializer):
    c = DisciplineSpecialityConnectionSerializer(many=True, source='specialization_connection')







