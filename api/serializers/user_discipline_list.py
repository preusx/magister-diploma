# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from rest_framework import serializers as s

import curriculum.models as m
from .base import DetailSerializer, ListSerializer
from .specialization import SpecializationListSerializer, SpecializationDetailSerializer



class UserDisciplineListListSerializer(ListSerializer):
    specialization = SpecializationListSerializer()

    class Meta:
        model = m.UserDisciplineList
        fields = ('id', 'name', 'description', 'specialization')
        depth = 1



class UserDisciplineListDetailSerializer(DetailSerializer):
    specialization_info = SpecializationDetailSerializer(read_only=True, source='specialization')

    specialization = s.PrimaryKeyRelatedField(queryset=m.Specialization.objects.all())
    user = s.PrimaryKeyRelatedField(queryset=get_user_model().objects.all())
    disciplines = s.PrimaryKeyRelatedField(many=True, queryset=m.Discipline.objects.all())

    class Meta:
        model = m.UserDisciplineList
        # fields = (
        #     'id',
        #     'specialization',
        #     'specialization_info',
        #     'user',
        #     'disciplines',
        #     'name',
        #     'description',
        #     )
        depth = 3







