# -*- coding: utf-8 -*-
from rest_framework import viewsets, serializers, generics, status
from rest_framework.response import Response
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.db.models import Q, Count, Min
import django_filters as df

from .base import ListView
import curriculum.models as m
from api.serializers import SpecialityListSerializer,\
    SpecialityDetailSerializer

import datetime



# class SpecialityFilter(df.FilterSet):
#     class Meta:
#         model = m.Speciality
#         fields = ('',)



class SpecialityView(ListView):
    queryset = m.Speciality.objects\
        .all()
    serializer_class = SpecialityListSerializer
    # filter_class = SpecialityFilter
    atom_serializer_class = SpecialityDetailSerializer

    ordering_fields = ('',)
    search_fields = ('',)