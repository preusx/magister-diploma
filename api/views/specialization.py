# -*- coding: utf-8 -*-
from rest_framework import viewsets, serializers, generics, status
from rest_framework.response import Response
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.db.models import Q, Count, Min
import django_filters as df

from .base import ListView
import curriculum.models as m
from api.serializers import SpecializationListSerializer,\
    SpecializationDetailSerializer

import datetime



# class SpecializationFilter(df.FilterSet):
#     class Meta:
#         model = m.Specialization
#         fields = ('',)



class SpecializationView(ListView):
    queryset = m.Specialization.objects\
        .all()
    serializer_class = SpecializationListSerializer
    # filter_class = SpecializationFilter
    atom_serializer_class = SpecializationDetailSerializer

    ordering_fields = ('',)
    search_fields = ('',)