# -*- coding: utf-8 -*-
from .speciality import SpecialityView
from .specialization import SpecializationView
from .discipline import DisciplineView
from .user_discipline_list import UserDisciplineListView
from .user import current_user as user