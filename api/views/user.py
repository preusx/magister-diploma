# -*- coding: utf-8 -*-
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status

from api.serializers import UserSerializer


@api_view(['GET', 'POST'])
@permission_classes([])
def current_user(request):
    if(request.user.is_authenticated()):
        serializer = UserSerializer(request.user)

        return Response(serializer.data)
    else:
        return Response({}, status=status.HTTP_401_UNAUTHORIZED)