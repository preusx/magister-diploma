# -*- coding: utf-8 -*-
from rest_framework import viewsets, serializers, generics, status
from rest_framework.response import Response
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.db.models import Q, Count, Min
import django_filters as df

from .base import ListView
import curriculum.models as m
from api.serializers import DisciplineListSerializer,\
    DisciplineDetailSerializer

import datetime



# class DisciplineFilter(df.FilterSet):
#     class Meta:
#         model = m.Discipline
#         fields = ('',)



class DisciplineView(ListView):
    queryset = m.Discipline.objects\
        .all()
    serializer_class = DisciplineListSerializer
    # filter_class = DisciplineFilter
    atom_serializer_class = DisciplineDetailSerializer

    ordering_fields = ('',)
    search_fields = ('',)