# -*- coding: utf-8 -*-

from django.conf import settings

from rest_framework import status, fields
from rest_framework import serializers as s, generics, viewsets, mixins,\
    filters, status, fields, pagination
from rest_framework.response import Response
from rest_framework.settings import import_from_string
from rest_framework.templatetags.rest_framework import replace_query_param

import django_filters as df

from django.core.paginator import Paginator



#
# Pagination Fields
#

# class FirstPageLink(s.Field):
#     """
#     Field that returns a link to the first page in paginated results.
#     """
#     page_field = 'page'

#     def to_native(self, value):
#         if value.number == 1:
#             return None
#         page = None
#         request = self.context.get('request')
#         url = request and request.build_absolute_uri() or ''
#         return replace_query_param(url, self.page_field, page)



# class LastPageLink(s.Field):
#     """
#     Field that returns a link to the last page in paginated results.
#     """
#     page_field = 'page'

#     def to_native(self, value):
#         if value.number == value.paginator.num_pages:
#             return None
#         page = value.paginator.num_pages
#         request = self.context.get('request')
#         url = request and request.build_absolute_uri() or ''
#         return replace_query_param(url, self.page_field, page)



# class FirstPageNum(s.Field):
#     page_field = 'page'

#     def to_native(self, value):
#         if value.number == 1:
#             return None
#         else:
#             return 1


# class NextPageNum(s.Field):
#     page_field = 'page'

#     def to_native(self, value):
#         if not value.has_next():
#             return None
#         else:
#             return value.next_page_number()


# class PreviousPageNum(s.Field):
#     page_field = 'page'

#     def to_native(self, value):
#         if not value.has_previous():
#             return None
#         else:
#             return value.previous_page_number()


# class LastPageNum(s.Field):
#     page_field = 'page'

#     def to_native(self, value):
#         if value.number == value.paginator.num_pages:
#             return None
#         else:
#             return value.paginator.num_pages



# #
# # Pagination class
# #

# class LinksSerializer(s.Serializer):
#     first_link = FirstPageLink(source='*')
#     last_link = LastPageLink(source='*')



# class PositionsSerializer(s.Serializer):
#     first = FirstPageNum(source='*')
#     next = NextPageNum(source='*')
#     prev = PreviousPageNum(source='*')
#     last = LastPageNum(source='*')



# class PaginationListSerializer(s.Serializer):
#     links = LinksSerializer(source='*')
#     positions = PositionsSerializer(source='*')
#     pages = s.Field(source='paginator.num_pages')
#     count = s.Field(source='paginator.count')
#     page = s.Field(source='number')



# class ModelPaginationSerializer(pagination.BasePaginationSerializer):
#     pagination = PaginationListSerializer(source='*')

#     results_field = 'objects'



# def paginate(obj, pg = None, page_by=None):
#     if not page_by:
#         page_by = settings.REST_FRAMEWORK['PAGINATE_BY']

#     paginator = Paginator(obj, page_by)

#     if pg == None or pg < 0 or pg > paginator.num_pages:
#         return paginator.page(1)
#     else:
#         return paginator.page(pg)



#
# Base Views
#

class BaseView(mixins.ListModelMixin,
                mixins.CreateModelMixin,
                mixins.RetrieveModelMixin,
                mixins.UpdateModelMixin,
                mixins.DestroyModelMixin,
                viewsets.GenericViewSet):
    # pagination_serializer_class = ModelPaginationSerializer

    filter_backends = (filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter)


    def get_serializer(self, instance=None, partial=False, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """

        if self.action == 'list':
            serializer_class = self.get_serializer_class()
        elif self.action:
            serializer_class = self.atom_serializer_class
        else:
            serializer_class = self.atom_serializer_class

        context = self.get_serializer_context()

        return serializer_class(instance, partial=partial, *args, **kwargs)


    # def update(self, request, *args, **kwargs):
    #     response = super(BaseView, self).update(request, *args, **kwargs)

    #     if response.status_code == status.HTTP_200_OK:
    #         return self.retrieve(request, *args, **kwargs)

    #     return response


    # def create(self, request, *args, **kwargs):
    #     slug = request.DATA.get('slug', None)

    #     if slug:
    #         model = self.get_queryset().model
    #         count = model.objects\
    #             .filter(slug__istartswith=slug)\
    #             .count()

    #         if count > 0:
    #             slug += str(count)

    #         request.DATA['slug'] = slug

    #     return super(BaseView, self).create(request, *args, **kwargs)



class ListView(BaseView):
    pass