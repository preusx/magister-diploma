# -*- coding: utf-8 -*-
from rest_framework import viewsets, serializers, generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.db.models import Q, Count, Min
import django_filters as df

from .base import ListView
import curriculum.models as m
from api.serializers import UserDisciplineListListSerializer,\
    UserDisciplineListDetailSerializer

import datetime



# class UserDisciplineListFilter(df.FilterSet):
#     class Meta:
#         model = m.UserDisciplineList
#         fields = ('',)



class UserDisciplineListView(ListView):
    model = m.UserDisciplineList
    serializer_class = UserDisciplineListListSerializer
    # filter_class = UserDisciplineListFilter
    atom_serializer_class = UserDisciplineListDetailSerializer

    permission_classes = [IsAuthenticated]

    ordering_fields = ('',)
    search_fields = ('',)

    def get_queryset(self):
        return self.model.objects\
            .filter(user_id=self.request.user.id)\
            .all()