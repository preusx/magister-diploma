(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

require("./auth/module");
require("./pages/specialties/module");
require("./pages/user-list/module");
var app = angular.module("application", ["auth", "pages.specialties", "pages.user-list", "ngSanitize", "ngResource", "ngCookies"]).constant("pathResource", "/api/v1/").directive("historyBack", require("./historyBack.js")).directive("statePendingClass", require("./statePendingClass.js")).factory("Data", require("./Data.js")).factory("DataSort", require("./DataSort.js")).factory("DataConvert", require("./DataConvert.js")).factory("HTTPErrorHandler", require("./HTTPErrorHandler.js")).service("Api", require("./Api.js")).filter("default", function () {
    return function (input, arg) {
        return !!input.trim() ? input : arg;
    };
}).config((function () {
    var diFunction = function diFunction($urlRouterProvider, $httpProvider) {
        $urlRouterProvider.otherwise("/specialties");
        $httpProvider.interceptors.push("HTTPErrorHandler");
    };
    diFunction.$inject = ["$urlRouterProvider", "$httpProvider"];
    return diFunction;
})());

},{"./Api.js":2,"./Data.js":3,"./DataConvert.js":4,"./DataSort.js":5,"./HTTPErrorHandler.js":6,"./auth/module":8,"./historyBack.js":11,"./pages/specialties/module":20,"./pages/user-list/module":27,"./statePendingClass.js":29}],2:[function(require,module,exports){
"use strict";

module.exports = (function () {
    var diFunction = function Api($resource, $cookies, pathResource) {
        var self = this;
        var apiResource = function apiResource(url, paramDefaults, actions, options) {
            var ng = angular,
                methods = {
                query: {
                    method: "GET",
                    headers: {
                        "X-CSRFToken": function XCSRFToken() {
                            return $cookies.csrftoken;
                        }
                    }
                },
                all: {
                    method: "GET",
                    params: { page_size: 9999999 },
                    headers: {
                        "X-CSRFToken": function XCSRFToken() {
                            return $cookies.csrftoken;
                        }
                    }
                },
                "delete": {
                    method: "DELETE",
                    headers: {
                        "X-CSRFToken": function XCSRFToken() {
                            return $cookies.csrftoken;
                        }
                    }
                },
                create: {
                    method: "POST",
                    headers: {
                        "X-CSRFToken": function XCSRFToken() {
                            return $cookies.csrftoken;
                        }
                    }
                },
                update: {
                    method: "PUT",
                    headers: {
                        "X-CSRFToken": function XCSRFToken() {
                            return $cookies.csrftoken;
                        }
                    }
                },
                patch: {
                    method: "PATCH",
                    headers: {
                        "X-CSRFToken": function XCSRFToken() {
                            return $cookies.csrftoken;
                        }
                    }
                }
            };
            return $resource(pathResource + url, paramDefaults || {}, ng.extend(ng.copy(methods), actions || {}), options);
        };
        var apiResourceDefault = function apiResourceDefault(name) {
            return apiResource(name + "/:id/:controller/?", {
                id: "@id",
                controller: "@controller"
            });
        };
        this.resource = apiResource;
        this.resourceDefault = apiResourceDefault;
        this.specialization = this.resourceDefault("specialization");
        this.discipline = this.resourceDefault("discipline");
        this.user_discipline_list = this.resourceDefault("user_discipline_list");
    };
    diFunction.$inject = ["$resource", "$cookies", "pathResource"];
    return diFunction;
})();

},{}],3:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var cfg = require("./config");

var Specialty = function Specialty(id, name) {
    _classCallCheck(this, Specialty);

    this.id = id;
    this.name = name;
    this.description = "" + "<p>Визуализация графа внутренних и внешних связей дисциплин в рамках учебного плана.</p>";
};

var Discipline = function Discipline(id, name) {
    _classCallCheck(this, Discipline);

    this.id = id;
    this.name = name;
    this.description = "" + "<p>Визуализация графа внутренних и внешних связей дисциплин в рамках учебного плана.</p>";
    this.weight = Math.round(Math.random() * (Math.random() * 2) + 1);
    this.pos = 0;
    this.start = 0;
    this.end = 0;
};

var id__prefix = "ID",
    id__counter = 0;
var newID = function newID() {
    id__counter++;
    return id__prefix + id__counter.toString();
};
var Data = module.exports = function () {
    var specIds = [],
        discIds = [];
    var data = {
        data: {
            specs: {},
            discs: {}
        },
        connections: {
            from: {},
            to: {}
        },
        getSpecialty: function getSpecialty(id$2) {
            return this.data.specs[id$2];
        },
        getSpecialtyDisciplineList: function getSpecialtyDisciplineList(id$2) {
            var discs = {};
            var connections = {
                from: {},
                to: {}
            };
            for (var _i = 0, _l = this.connections.from[id$2].length; _i < _l; _i++) {
                var disc = this.connections.from[id$2][_i];
                discs[disc] = angular.copy(this.data.discs[disc]);
                discs[disc].cType = "main";
                discs[disc].selected = true;
                if (typeof connections.from[disc] == "undefined") {
                    connections.from[disc] = [];
                }
                if (typeof connections.to[disc] == "undefined") {
                    connections.to[disc] = [];
                }
                for (var _i$2 = 0, _l$2 = this.connections.from[disc].length; _i$2 < _l$2; _i$2++) {
                    var semDisc = this.connections.from[disc][_i$2];
                    if (typeof discs[semDisc] === "undefined") {
                        discs[semDisc] = angular.copy(this.data.discs[semDisc]);
                        discs[semDisc].cType = "secondary";
                        discs[semDisc].selected = false;
                        if (typeof connections.to[semDisc] == "undefined") {
                            connections.to[semDisc] = [];
                            connections.from[semDisc] = [];
                        }
                        connections.to[semDisc].push(disc);
                        connections.from[disc].push(semDisc);
                    }
                }
                for (var _i$3 = 0, _l$3 = this.connections.to[disc].length; _i$3 < _l$3; _i$3++) {
                    var semDisc = this.connections.to[disc][_i$3];
                    if (typeof discs[semDisc] === "undefined" && typeof this.data.discs[semDisc] !== "undefined") {
                        discs[semDisc] = angular.copy(this.data.discs[semDisc]);
                        discs[semDisc].cType = "secondary";
                        discs[semDisc].selected = false;
                        if (typeof connections.from[semDisc] == "undefined") {
                            connections.to[semDisc] = [];
                            connections.from[semDisc] = [];
                        }
                        connections.from[semDisc].push(disc);
                        connections.to[disc].push(semDisc);
                    }
                }
            }
            return {
                discs: discs,
                connections: connections
            };
        }
    };
    var names = ["Эпидемиология и государственные регистры", "Молекулярно-генетические технологии", "Фундаментальная диабетология", "Современные алгоритмы лечения", "Вопросы диагностики", "Диабетическая ретинопатия", "Диабетическая нефропатия", "Артериальная гипертензия и сердечно-сосудистые осложнения", "Диабетическая нейропатия", "Синдром диабетической стопы", "Острые осложнения сахарного диабета", "Сахарный диабет у детей и подростков", "Помповая инсулинотерапия", "Непрерывное мониторирование гликемии", "Ожирение и метаболический синдром", "Профилактика сахарного диабета и его осложнений", "Репродуктивное здоровье и сахарный диабет", "Сахарный диабет и беременность", "Гестационный сахарный диабет", "Программы обучения больных сахарным диабетом", "Проблемы гликемического контроля", "Сочетанная эндокринная патология", "Особенности соматических заболеваний", "Нарушения кальций-фосфорного обмена", "Сахарный диабет и онкология", "Редкие формы сахарного диабета", "Визуализация графа внутренних и внешних связей"];
    var nl = names.length,
        ni = 0;
    var sc = cfg.specialty.count,
        dc = cfg.discipline.count,
        s2dc = cfg.connections;
    for (var i = 0; i < sc; i++) {
        var id = newID();
        ni++;
        data.data.specs[id] = new Specialty(id, names[ni % nl]);
        data.connections.from[id] = [];
        data.connections.to[id] = [];
        specIds.push(id);
    }
    for (var i = 0; i < dc; i++) {
        var id = newID();
        ni++;
        data.data.discs[id] = new Discipline(id, names[ni % nl]);
        data.connections.from[id] = [];
        data.connections.to[id] = [];
        discIds.push(id);
    }
    for (var i in data.data.specs) {
        for (var j = 0; j < s2dc; j++) {
            var rand = Math.round(Math.random() * (dc - 1));
            while (data.connections.from[i].indexOf(discIds[rand]) >= 0) {
                rand = Math.round(Math.random() * (dc - 1));
            }
            // Setting up the specialty->discipline connections
            data.connections.from[i].push(discIds[rand]);
            data.connections.to[discIds[rand]].push(i);
        }
    }
    for (var j = 0; j < dc / 1.7; j++) {
        var rand = Math.round(Math.random() * (dc - 1)),
            rand2 = Math.round(Math.random() * (dc - 1));
        if (!(data.connections.from[discIds[rand]].indexOf(discIds[rand2]) >= 0 && data.connections.to[discIds[rand2]].indexOf(discIds[rand]) >= 0)) {
            data.connections.from[discIds[rand]].push(discIds[rand2]);
            data.connections.to[discIds[rand2]].push(discIds[rand]);
        }
    }
    return data;
};

},{"./config":10}],4:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var cfg = require("./config");
function pushSingle(array, item) {
    if (array.indexOf(item) === -1) {
        array.push(item);
    }
}

var Discipline = function Discipline(data, selected) {
    _classCallCheck(this, Discipline);

    this.id = "" + data.id;
    this.name = data.name;
    this.hint = data.hint;
    this.description = data.description;
    this.selected = selected;
    var co = data.c ? data.c[0] ? data.c[0] : {} : {};
    this.type = co.type || "o";
    this.weight = Math.ceil((co.weight || 3) / 3);
    this.semester_from = (co.semester_from || 0) - 1;
    this.semester_to = (co.semester_to || 0) - 1;
    this.category = co.category || {};
    this.pos = 0;
    this.start = 0;
    this.end = 0;
};

module.exports = function DataConvert() {
    return function (data) {
        var result = {
            disciplines: {},
            categories: {},
            connections: {
                from: {},
                to: {},
                types: {}
            }
        };
        var // console.log(data);
        convertDiscipline = function convertDiscipline(disc, selected) {
            if (typeof result.disciplines[disc.id] == "undefined") {
                var cDisc = result.disciplines[disc.id] = new Discipline(disc, selected);
                if (typeof result.categories[cDisc.category.id] == "undefined" && typeof cDisc.category.id !== "undefined") {
                    result.categories[cDisc.category.id] = cDisc.category;
                }
                if (typeof result.connections.types[cDisc.type] == "undefined") {
                    result.connections.types[cDisc.type] = [];
                }
                result.connections.types[cDisc.type].push(cDisc.id);
            } else {
                if (selected) {
                    result.disciplines[disc.id].selected = true;
                }
                if (((disc.c || [])[0] || {}).type == "m") {
                    result.disciplines[disc.id].type = "p";
                }
            }
        };
        var addNext = [];
        for (var _i = 0, _l = data.length; _i < _l; _i++) {
            var item = data[_i];
            convertDiscipline(item, true);
            if (typeof result.connections.from[item.id] == "undefined") {
                result.connections.from[item.id] = [];
            }
            if (typeof result.connections.to[item] == "undefined") {
                result.connections.to[item.id] = [];
            }
        }
        for (var _i$2 = 0, _l$2 = data.length; _i$2 < _l$2; _i$2++) {
            var item = data[_i$2];
            for (var _i$3 = 0, _l$3 = item.goal.length; _i$3 < _l$3; _i$3++) {
                var pNotion = item.goal[_i$3];
                for (var _i$4 = 0, _l$4 = pNotion.disciplineobject.length; _i$4 < _l$4; _i$4++) {
                    var pItem = pNotion.disciplineobject[_i$4];
                    if (pItem.id == item.id) continue;
                    convertDiscipline(pItem, false);
                    if (typeof result.connections.from[pItem.id] == "undefined") {
                        result.connections.from[pItem.id] = [];
                    }
                    if (typeof result.connections.to[pItem.id] == "undefined") {
                        result.connections.to[pItem.id] = [];
                    }
                    pushSingle(result.connections.from[item.id], pItem.id);
                    pushSingle(result.connections.to[pItem.id], item.id);
                }
                for (var _i$5 = 0, _l$5 = pNotion.disciplinegoal.length; _i$5 < _l$5; _i$5++) {
                    var pItem = pNotion.disciplinegoal[_i$5];
                    if (pItem.id == item.id) continue;
                    convertDiscipline(pItem, false);
                    if (typeof result.connections.from[pItem.id] == "undefined") {
                        result.connections.from[pItem.id] = [];
                    }
                    if (typeof result.connections.to[pItem.id] == "undefined") {
                        result.connections.to[pItem.id] = [];
                    }
                }
            }
            for (var _i$6 = 0, _l$6 = item.object.length; _i$6 < _l$6; _i$6++) {
                var cNotion = item.object[_i$6];
                for (var _i$7 = 0, _l$7 = cNotion.disciplinegoal.length; _i$7 < _l$7; _i$7++) {
                    var cItem = cNotion.disciplinegoal[_i$7];
                    if (cItem.id == item.id) continue;
                    convertDiscipline(cItem, false);
                    if (typeof result.connections.from[cItem.id] == "undefined") {
                        result.connections.from[cItem.id] = [];
                    }
                    if (typeof result.connections.to[cItem.id] == "undefined") {
                        result.connections.to[cItem.id] = [];
                    }
                    pushSingle(result.connections.to[item.id], cItem.id);
                    pushSingle(result.connections.from[cItem.id], item.id);
                }
                for (var _i$8 = 0, _l$8 = cNotion.disciplineobject.length; _i$8 < _l$8; _i$8++) {
                    var cItem = cNotion.disciplineobject[_i$8];
                    if (cItem.id == item.id) continue;
                    convertDiscipline(cItem, false);
                    if (typeof result.connections.from[cItem.id] == "undefined") {
                        result.connections.from[cItem.id] = [];
                    }
                    if (typeof result.connections.to[cItem.id] == "undefined") {
                        result.connections.to[cItem.id] = [];
                    }
                }
            }
        }
        return result;
    };
};

},{"./config":10}],5:[function(require,module,exports){
"use strict";

Object.filter = function (obj, func) {
    obj = JSON.parse(JSON.stringify(obj));
    for (var key in obj) {
        if (obj.hasOwnProperty(key) && !func(obj[key], key)) {
            delete obj[key];
        }
    }
    return obj;
};
module.exports = function DataSort() {
    return function (data, cfg) {
        var table = {};
        var semesterWeights = newArray(cfg.semester.count + 2, 0);
        var semesterDiscs = newArray(cfg.semester.count + 2, 0);
        var cdata = JSON.parse(JSON.stringify(data));
        cdata.disciplines = Object.filter(cdata.disciplines, function (elem) {
            return elem.selected;
        });
        var discs = cdata.disciplines;
        var inSemesters = newArray(cfg.semester.count + 2, []);
        var queue = Object.keys(cdata.disciplines);
        var dataWeight = queue.reduce(function (o, n) {
            return o + cdata.disciplines[n].weight;
        }, 0);
        var currentSemester = 0,
            semesterWidth = 0;
        var started = [];
        var connectionsFilter = function connectionsFilter(list) {
            for (var i$2 in list) {
                if (!list.hasOwnProperty(i$2)) continue;
                if (typeof cdata.disciplines[i$2] === "undefined") {
                    delete list[i$2];
                } else {
                    for (var j$2 = 0; j$2 < list[i$2].length; j$2++) {
                        if (typeof cdata.disciplines[list[i$2][j$2]] === "undefined") {
                            list[i$2].splice(j$2, 1);
                            j$2--;
                        }
                    }
                }
            }
            return list;
        };
        var // console.log(cdata);
        connections = {
            from: connectionsFilter(cdata.connections.from),
            to: connectionsFilter(cdata.connections.to)
        };
        sort.call(queue, comparator);
        for ( // queue.sort(comparator);
        // console.log(queue);
        // console.log(data.connections);
        var i = 0; i < queue.length; i++) {
            // Filling the table row with null's.
            table[queue[i]] = newArray(cfg.semester.count * cfg.semester.weight, 0);
            if ( // Filling up a hard positioned stuff.
            discs[queue[i]].semester_from >= 0) {
                if (discs[queue[i]].semester_to === -1) {
                    discs[queue[i]].semester_to = discs[queue[i]].semester_from;
                }
                var // Setting the semesters weights.
                dSemesterWeight = discs[queue[i]].weight / (discs[queue[i]].semester_to - discs[queue[i]].semester_from + 1) | 0;
                dSemesterWeight = Math.ceil(dSemesterWeight / 3) * 3;
                discs[queue[i]].start = discs[queue[i]].semester_from * cfg.semester.weight;
                discs[queue[i]].end = (discs[queue[i]].semester_to + 1) * cfg.semester.weight;
                for (var k = discs[queue[i]].semester_from; k <= discs[queue[i]].semester_to; k++) {
                    semesterDiscs[k]++;
                    semesterWeights[k] += dSemesterWeight;
                }
                dataWeight -= discs[queue[i]].weight;
                discs[queue[i]].weight = 0;
            }
        }
        while (dataWeight > 0) {
            var change = false;
            for ( // Each element
            var i = 0; i < queue.length; i++) {
                if (discs[queue[i]].weight > 0) {
                    if ( // Checking if there are some space in current semester.
                    semesterWeights[currentSemester] >= cfg.semester.weight) {
                        currentSemester++;
                        break;
                    } else if (semesterWeights[currentSemester] < cfg.semester.weight && semesterDiscs[currentSemester] >= cfg.semester.limit) {
                        if (inSemesters[currentSemester].indexOf(queue[i]) < 0) {
                            continue;
                        }
                    }
                    if ( // Checking if there are any unfinished parents.
                    arrayInArray(connections.to[queue[i]], started)) {
                        break;
                    }
                    var pos = 0,
                        startedNow = false;
                    if ( // Checking if this is a new discipline and starting it or it has
                    // been already started.
                    // And setting up current position relatively to that.
                    started.indexOf(queue[i]) < 0) {
                        startedNow = true;
                        pos = pmax(queue[i]);
                        started.push(queue[i]);
                    } else {
                        pos = discs[queue[i]].end;
                    }
                    if ( // Checking if current discipline is new in thois semester or not.
                    inSemesters[currentSemester].indexOf(queue[i]) < 0) {
                        semesterDiscs[currentSemester]++;
                        inSemesters[currentSemester].push(queue[i]);
                    }
                    if (pos < currentSemester * cfg.semester.weight) {
                        pos = currentSemester * cfg.semester.weight;
                    }
                    if (startedNow) {
                        discs[queue[i]].start = pos;
                        discs[queue[i]].semesterWeight = {};
                    }
                    discs[queue[i]].pos = pos;
                    discs[queue[i]].weight -= 1;
                    table[queue[i]][pos] = true;
                    discs[queue[i]].end = pos + 1;
                    discs[queue[i]].semesterWeight[currentSemester] = (discs[queue[i]].semesterWeight[currentSemester] || 0) + 1;
                    semesterWeights[currentSemester]++;
                    change = true;
                    if (semesterWidth < discs[queue[i]].end - currentSemester * cfg.semester.weight) {
                        semesterWidth = discs[queue[i]].end - currentSemester * cfg.semester.weight;
                    }
                    if (discs[queue[i]].weight == 0) {
                        started.splice(started.indexOf(queue[i]), 1);
                    }
                    dataWeight--;
                }
            }
            if (!change) {
                currentSemester++;
            }
        }
        var diff = cfg.semester.weight - semesterWidth;
        for ( // console.log(diff);
        var i = 0; i < queue.length; i++) {
            for (var j = 0; j < cfg.semester.count; j++) {
                table[queue[i]].splice((j + 1) * semesterWidth, diff);
            }
            discs[queue[i]].start -= (discs[queue[i]].start / cfg.semester.weight | 0) * diff;
            discs[queue[i]].start -= discs[queue[i]].start % semesterWidth;
            discs[queue[i]].end -= (discs[queue[i]].end / cfg.semester.weight | 0) * diff;
            if (discs[queue[i]].end % semesterWidth > 0) {
                discs[queue[i]].end += semesterWidth - discs[queue[i]].end % semesterWidth;
            }
            discs[queue[i]].weight = data.disciplines[queue[i]].weight * 3;
        }
        semesterWeights.splice(cfg.semester.count, cfg.semester.count);
        semesterDiscs.splice(cfg.semester.count, cfg.semester.count);
        return {
            table: table,
            discs: discs,
            weights: semesterWeights,
            discCount: semesterDiscs,
            width: semesterWidth
        };
        function pmax(id) {
            var maxID = connections.to[id][0];
            for (var i$2 = 1; i$2 < connections.to[id].length; i$2++) {
                var nid = connections.to[id][i$2];
                if (discs[maxID].end < discs[nid].end) {
                    maxID = nid;
                }
            }
            if (typeof maxID == "undefined") {
                return 0;
            } else {
                return discs[maxID].end;
            }
        }
        function comparator(a, b) {
            var to = connections.to,
                from = connections.from;
            var r = from[b].indexOf(a) >= 0 ? 1 : from[a].indexOf(b) >= 0 ? -1 : to[a].length > to[b].length ? 1 : from[a].length > from[b].length ? -1 : 0;
            return r;
        }
        function sort(cmp) {
            for (var i$2 = 0, l = this.length; i$2 < l; i$2++) {
                for (var j$2 = 0; j$2 < l; j$2++) {
                    if (i$2 !== j$2) {
                        if (cmp(this[i$2], this[j$2]) === -1 && i$2 > j$2 || cmp(this[i$2], this[j$2]) === 1 && i$2 < j$2) {
                            var tmp = this[i$2];
                            this[i$2] = this[j$2];
                            this[j$2] = tmp;
                        }
                    }
                }
            }
        }
    };
};
function newArray(count, def) {
    var arr = new Array(count);
    for (var i = 0; i < count; i++) {
        arr[i] = JSON.parse(JSON.stringify(def));
    }
    return arr;
}
function arrayInArray(one, two) {
    if (one.length === 0 || two.length === 0) {
        return false;
    }
    if (one.length < two.length) {
        var tmp = two;
        two = one;
        one = tmp;
    }
    for (var i = 0, l = one.length; i < l; i++) {
        if (two.indexOf(one[i]) >= 0) {
            return true;
        }
    }
    return false;
}

},{}],6:[function(require,module,exports){
"use strict";

module.exports = (function () {
    var diFunction = function HTTPErrorHandler($q, $log, $injector) {
        return {
            responseError: function responseError(rejection) {
                if ( // notifications.add({
                //   'title': 'HTTP ' + rejection.config.method + ' Error:',
                //   'type': 'error',
                //   'content': rejection.config.url + '\n'
                //     + rejection.status + ': ' + rejection.statusText,
                // });
                rejection.status == 403) {
                    $injector.get("$state").go("auth", {}, { location: "replace" });
                }
                return $q.reject(rejection);
            }
        };
    };
    diFunction.$inject = ["$q", "$log", "$injector"];
    return diFunction;
})();

},{}],7:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var AuthCtrl = (function () {
    function AuthCtrl($rootScope, $scope, $http, $location, $state, $cookies) {
        _classCallCheck(this, AuthCtrl);

        var self = this;
        this.submit = function () {
            $http({
                method: "post",
                url: "/profile/auth/login/",
                data: self.loginData,
                headers: {
                    "X-CSRFToken": function XCSRFToken() {
                        return $cookies.csrftoken;
                    }
                }
            }).success(function (data, status, headers, config) {
                $rootScope.currentUser = data;
                $state.go("specialties", {}, { location: "replace" });
            }).error(function (data, status, headers, config) {});
        };
        this.loginData = {
            username: "",
            password: ""
        };
        $scope.AuthCtrl = this;
    }

    AuthCtrl.resolver = function resolver() {
        return {};
    };

    return AuthCtrl;
})();

AuthCtrl.$inject = ["$rootScope", "$scope", "$http", "$location", "$state", "$cookies"];
module.exports = AuthCtrl;

},{}],8:[function(require,module,exports){
"use strict";

angular.module("auth", ["ui.router"]).config(require("./routes")).run(function ($rootScope, $injector) {
    // console.log(window.location.origin + '/api/v1/user');
    $injector.get("$http")({
        method: "get",
        url: "/api/v1/user",
        headers: {
            "X-CSRFToken": function XCSRFToken() {
                return $injector.get("$cookies").csrftoken;
            }
        }
    }).success(function (data, status, headers, config) {
        $rootScope.currentUser = data;
    });
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams) {
        var requireLogin = (toState.data || {}).requireLogin;
        if (requireLogin && typeof $rootScope.currentUser === "undefined") {
            event.preventDefault();
            $injector.get("$state").go("auth", {}, { location: "replace" });
        }
    });
});

},{"./routes":9}],9:[function(require,module,exports){
"use strict";

var AuthCtrl = require("./AuthCtrl.js");
module.exports = (function () {
    var diFunction = function diFunction($stateProvider) {
        $stateProvider.state("auth", {
            url: "/auth/login",
            views: {
                "body@": {
                    controller: AuthCtrl,
                    resolve: AuthCtrl.resolver(),
                    templateUrl: "/s/html/templates/login.html"
                }
            }
        });
    };
    diFunction.$inject = ["$stateProvider"];
    return diFunction;
})();

},{"./AuthCtrl.js":7}],10:[function(require,module,exports){
"use strict";

var data = module.exports = {
    semester: {
        count: 8,
        weight: 10,
        limit: 8
    },
    discipline: { count: 52 },
    connections: 40,
    specialty: { count: 5 }
};

},{}],11:[function(require,module,exports){
"use strict";

module.exports = (function () {
    var diFunction = function diFunction($window) {
        return {
            link: function link(s) {
                (function ($element) {
                    $element.on("click", function () {
                        $window.history.back();
                    });
                }).apply(s, Array.prototype.slice.call(arguments, 1));
            }
        };
    };
    diFunction.$inject = ["$window"];
    return diFunction;
})();

},{}],12:[function(require,module,exports){
"use strict";

module.exports = (function () {
    var diFunction = function diFunction($document) {
        return {
            require: "^interactParent",
            link: function link(s) {
                (function ($element, $attrs, ctrl) {
                    ctrl.object = $element;
                }).apply(s, Array.prototype.slice.call(arguments, 1));
            }
        };
    };
    diFunction.$inject = ["$document"];
    return diFunction;
})();

},{}],13:[function(require,module,exports){
"use strict";

function transformMatrix(transformString) {
    if (transformString !== "none") {
        var arr = transformString.replace("matrix(", "").replace(")", "").split(", ").map(function (e) {
            return parseFloat(e);
        });
    } else {
        var arr = [1, 0, 0, 1, 0, 0];
    }
    return arr;
}
module.exports = (function () {
    var diFunction = function diFunction($document) {
        return {
            controller: function controller() {
                var self = this;
                this.zoom = zoom;
                this.mousedown = mousedown;
                this.mousemove = mousemove;
                function zoom(event) {
                    if (!event.ctrlKey) {
                        var scaleFactor = 0.05,
                            d = event.deltaY;
                        var matrix = transformMatrix(self.object.css("transform"));
                        var scale = matrix[0];
                        var offset = self.object.offset();
                        d = d > 0 && scale < 2 ? d : d < 0 && scale > 0.4 ? d : 0;
                        matrix[0] = scale + scaleFactor * d;
                        matrix[3] = matrix[0];
                        var origin = (event.clientX - offset.left | 0).toString() + "px " + (event.clientY - offset.top | 0).toString() + "px";
                        if (d !== 0) {
                            self.object.css({
                                "transform-origin": origin,
                                "-webkit-transform-origin": origin
                            });
                            self.object.css("transform", "matrix(" + matrix.join(",") + ")");
                        }
                    }
                }
                function mousedown(event) {
                    self.last = event;
                    $document.on("mousemove", mousemove);
                    $document.on("mouseup", mouseup);
                }
                function mousemove(event) {
                    var matrix = transformMatrix(self.object.css("transform"));
                    matrix[4] -= self.last.clientX - event.clientX;
                    matrix[5] -= self.last.clientY - event.clientY;
                    self.object.css("transform", "matrix(" + matrix.join(",") + ")");
                    self.last = event;
                }
                function mouseup(event) {
                    $document.off("mousemove", mousemove);
                }
            },
            link: function link(s) {
                (function ($element, $attrs, ctrl) {
                    ctrl.parent = $element;
                    $element.mousewheel(ctrl.zoom);
                    $element.on("mousedown", ctrl.mousedown);
                }).apply(s, Array.prototype.slice.call(arguments, 1));
            }
        };
    };
    diFunction.$inject = ["$document"];
    return diFunction;
})();

},{}],14:[function(require,module,exports){
"use strict";

var app = module.exports = angular.module("interact", []).directive("interactParent", require("./directives/interactParent.js")).directive("interactObject", require("./directives/interactObject.js"));

},{"./directives/interactObject.js":12,"./directives/interactParent.js":13}],15:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var SpecialtiesListCtrl = (function () {
    function SpecialtiesListCtrl($scope, data) {
        _classCallCheck(this, SpecialtiesListCtrl);

        $scope.specs = data.results;
    }

    SpecialtiesListCtrl.resolver = function resolver() {
        return {
            data: (function () {
                var diFunction = function diFunction($state, $stateParams, Api) {
                    return Api.specialization.query($stateParams).$promise;
                };
                diFunction.$inject = ["$state", "$stateParams", "Api"];
                return diFunction;
            })()
        };
    };

    return SpecialtiesListCtrl;
})();

SpecialtiesListCtrl.$inject = ["$scope", "data"];
module.exports = SpecialtiesListCtrl;

},{}],16:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var cfg = require("../../config");

var SpecialtyCtrl = (function () {
    function SpecialtyCtrl($scope, data, DataSort, DataConvert) {
        _classCallCheck(this, SpecialtyCtrl);

        this.s = $scope;
        this.s.spec = data;
        this.s.specialtyCtrl = this;
        var convertedData = // var convertedData = DataConvert(data.relations);
        DataConvert([].concat(data.speciality.relations, data.relations));
        this.connections = convertedData.connections;
        this.disciplines = convertedData.disciplines;
        this.categories = convertedData.categories;
        for (var i in this.categories) {
            if (!this.categories.hasOwnProperty(i)) continue;
            this.categories[i].list = [];
        }
        this.sorted = DataSort(convertedData, cfg);
        var defaultCategory = (this.categories[Object.keys(this.categories)[0]] || {}).id;
        for (var i in this.sorted.discs) {
            if (!this.sorted.discs.hasOwnProperty(i)) continue;
            this.categories[this.sorted.discs[i].category.id || defaultCategory].list.push(this.sorted.discs[i].id);
        }
    }

    SpecialtyCtrl.resolver = function resolver() {
        return {
            // spec: function($state, $stateParams, Data) {
            //   return Data.getSpecialty($stateParams.id);
            // },
            data: function data($state, $stateParams, Api) {
                return Api.specialization.get($stateParams).$promise;
            }
        };
    };

    return SpecialtyCtrl;
})();

SpecialtyCtrl.$inject = ["$scope", "data", "DataSort", "DataConvert"];
module.exports = SpecialtyCtrl;

},{"../../config":10}],17:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var SpecialtyDListCtrl = (function () {
    function SpecialtyDListCtrl($scope) {
        _classCallCheck(this, SpecialtyDListCtrl);

        var self = this;
        this.s = $scope;
        if (!this.s.specialtyCtrl) {
            return;
        }this.connections = this.s.specialtyCtrl.connections;
        this.s.specialtyCtrl.drawer = this;
        this.config = {
            offset: 15,
            voffset: 6,
            current: 0,
            append: 6
        };
        this.canvas = {};
        this.itemsDrawn = false;
        this.itemsLeft = Object.keys(this.s.specialtyCtrl.sorted.discs);
        this.flow = Object.keys(this.s.specialtyCtrl.sorted.discs);
        this.items = // console.log(@flow);
        {};
    }

    SpecialtyDListCtrl.prototype.clear = function clear() {
        this.config.current = 0;
        this.itemsDrawn = false;
        if (typeof this.canvas.paper !== "undefined") {
            this.canvas.paper.canvas.parentNode.removeChild(this.canvas.paper.canvas);
        }
        delete this.canvas.paper;
    };

    SpecialtyDListCtrl.prototype.draw = function draw() {
        if (!this.itemsDrawn && (this.itemsLeft.length === 0 && !!this.canvas.wrapper)) {
            if (typeof this.canvas.paper !== "undefined") {
                this.clear();
            }
            this.canvas.wrapper.height(this.canvas.wrapper.parent().height());
            this.canvas.paper = new Raphael(this.canvas.wrapper[0]);
            this.itemsDrawn = // var flow = @flow;
            true;
            for (var _i = 0, _l = this.flow.length; _i < _l; _i++) {
                var i = this.flow[_i];
                for (var _i$2 = 0, _l$2 = this.connections.to[i].length; _i$2 < _l$2; _i$2++) {
                    var j = this.connections.to[i][_i$2];
                    if (this.flow.indexOf("" + j) >= 0) {
                        this.drawConnectionDeffered(j, i);
                    }
                }
            }
        }
    };

    SpecialtyDListCtrl.prototype.drawConnectionDeffered = function drawConnectionDeffered(i, j) {
        var self = this;
        setTimeout(function () {
            self.drawConnection(i, j);
        }, 10);
    };

    SpecialtyDListCtrl.prototype.drawConnection = function drawConnection(a, b) {
        var $from = $(this.items[a][0]);
        var $to = $(this.items[b][0]);
        var offset = this.canvas.wrapper.offset(),
            from = {
            offset: $from.offset(),
            width: $from.width(),
            height: $from.outerHeight(false)
        },
            to = {
            offset: $to.offset(),
            width: $to.width(),
            height: $to.outerHeight(false)
        };
        var direction = from.offset.left < to.offset.left,
            tmp;
        var // if(direction) {
        //   tmp = from;
        //   from = to;
        //   to = tmp;
        // }
        draw = {
            from: {
                x: from.offset.left - offset.left + this.config.offset + this.config.voffset,
                y: from.offset.top - offset.top + from.height
            },
            to: {
                x: to.offset.left - offset.left + this.config.offset,
                y: to.offset.top - offset.top + to.height
            }
        };
        var colors = ["#bbb", "#cc7", "#c77", "#7c7", "#7cc", "#c7c", "#7cc"];
        this.drawArrowPath(draw.from.x, draw.from.y, draw.to.x, draw.to.y, 5, direction).attr({
            stroke: colors[this.config.current % colors.length],
            "stroke-width": 1
        }).toBack();
    };

    SpecialtyDListCtrl.prototype.drawArrowPath = function drawArrowPath(x1, y1, x2, y2, arrowSize, dir) {
        if (typeof arrowSize === "undefined") {
            arrowSize = 0;
        }
        if (typeof dir === "undefined") {
            dir = x1 < x2;
        }
        var arrowPath = "",
            arrowDir = dir > 0 ? -1 : 1;
        if (x1 < x2) {
            var tx = x1,
                ty = y1;
            x1 = x2;
            x2 = tx;
            y1 = y2;
            y2 = ty;
        }
        this.config.current += this.config.append;
        var o = 0 - this.config.current - this.config.offset;
        var f = dir > 0 ? 8 : 11,
            t = dir <= 0 ? 8 : 11;
        if (arrowSize) {
            var x = x2,
                y = y2;
            if (arrowDir < 0) {
                x = x1;
                y = y1;
            }
            arrowPath = "M" + (x - arrowSize) + " " + (y + arrowSize) + "L" + x + " " + y + "L" + (x + arrowSize) + " " + (y + arrowSize);
        }
        return this.canvas.paper.path("M" + x1 + " " + y1 + "L" + x1 + " " + (y1 + f) + "L" + (x1 + o) + " " + (y1 + f) + "L" + (x1 + o) + " " + (y2 + t) + "L" + x2 + " " + (y2 + t) + "L" + x2 + " " + y2 + arrowPath).toBack();
    };

    /**
    * Linking some element to the discipline id.
    */

    SpecialtyDListCtrl.prototype.setDisciplineItem = function setDisciplineItem(id, $element) {
        var self = this,
            index = this.itemsLeft.indexOf(id);
        if (index >= 0) {
            this.itemsLeft.splice(index, 1);
        }
        this.items[id] = $element;
        $element.on("$destroy", function () {
            delete self.items[id];
            self.draw();
        });
        this.draw();
    };

    /**
    * Linking the canvas.
    */

    SpecialtyDListCtrl.prototype.setCanvas = function setCanvas($element) {
        var self = this;
        this.canvas.wrapper = $element;
        $element.on("$destroy", function () {
            delete self.canvas.wrapper;
            delete self.canvas.paper;
        });
        this.draw();
    };

    return SpecialtyDListCtrl;
})();

SpecialtyDListCtrl.$inject = ["$scope"];
module.exports = function () {
    return {
        controller: SpecialtyDListCtrl,
        link: function link(s) {
            (function ($element, $attrs) {
                if (!this.specialtyCtrl) return;
            }).apply(s, Array.prototype.slice.call(arguments, 1));
        }
    };
};

},{}],18:[function(require,module,exports){
"use strict";

module.exports = function () {
    return {
        require: "^specialtyDList",
        link: function link(s) {
            (function ($element, $attrs, ctrl) {
                if (!ctrl) return;
                ctrl.setDisciplineItem($attrs.specialtyDListItem, $element);
            }).apply(s, Array.prototype.slice.call(arguments, 1));
        }
    };
};

},{}],19:[function(require,module,exports){
"use strict";

module.exports = function () {
    return {
        require: "^specialtyDList",
        link: function link(s) {
            (function ($element, $attrs, ctrl) {
                if (!ctrl) return;
                $element.height($element.parent().height());
                ctrl.setCanvas($element);
            }).apply(s, Array.prototype.slice.call(arguments, 1));
        }
    };
};

},{}],20:[function(require,module,exports){
"use strict";

require("../../interact/module");
angular.module("pages.specialties", ["ui.router", "interact"]).config(require("./routes")).directive("specialtyDList", require("./directives/specialtyDList.js")).directive("specialtyDListItem", require("./directives/specialtyDListItem.js")).directive("specialtyDListMap", require("./directives/specialtyDListMap.js"));

},{"../../interact/module":14,"./directives/specialtyDList.js":17,"./directives/specialtyDListItem.js":18,"./directives/specialtyDListMap.js":19,"./routes":21}],21:[function(require,module,exports){
"use strict";

var SpecialtiesListCtrl = require("./SpecialtiesListCtrl.js");
var SpecialtyCtrl = require("./SpecialtyCtrl.js");
module.exports = (function () {
    var diFunction = function diFunction($stateProvider) {
        $stateProvider.state("specialties", {
            url: "/specialties",
            views: {
                "body@": {
                    controller: SpecialtiesListCtrl,
                    resolve: SpecialtiesListCtrl.resolver(),
                    templateUrl: "/s/html/templates/specialties.html"
                }
            }
        }).state("specialties.item", {
            url: "/item/:id",
            views: {
                "body@": {
                    controller: SpecialtyCtrl,
                    resolve: SpecialtyCtrl.resolver(),
                    templateUrl: "/s/html/templates/specialty.html"
                }
            }
        });
    };
    diFunction.$inject = ["$stateProvider"];
    return diFunction;
})();

},{"./SpecialtiesListCtrl.js":15,"./SpecialtyCtrl.js":16}],22:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var cfg = require("../../config");

var UserListCtrl = (function () {
    function UserListCtrl($scope, data, DataConvert, $injector) {
        _classCallCheck(this, UserListCtrl);

        var self = this;
        this.s = $scope;
        this.require = $injector.get;
        this.s.item = data;
        this.s.UserListCtrl = this;
        var convertedData = DataConvert([].concat(data.specialization_info.speciality.relations, data.specialization_info.relations));
        this.data = data;
        this.disciplines_list = convertedData.disciplines;
        this.disciplines = {
            creditsLeft: cfg.semester.weight * cfg.semester.count,
            creditsStack: [],
            list: [],
            selected: []
        };
        for (var _i = 0, _l = this.data.disciplines.length; _i < _l; _i++) {
            var i = this.data.disciplines[_i];
            this.disciplines.selected.push(this.disciplines_list[i]);
        }
        for (var i in this.disciplines_list) {
            if (this.disciplines_list[i].selected && this.disciplines_list[i].type == "m") {
                this.disciplines.creditsLeft -= this.disciplines_list[i].weight;
            }
            if (this.disciplines_list[i].type == "o") {
                if (!(this.data.disciplines.indexOf(this.disciplines_list[i].id | 0) >= 0)) {
                    this.disciplines.list.push(this.disciplines_list[i]);
                } else {
                    this.disciplines.creditsLeft -= this.disciplines_list[i].weight;
                }
            }
        }
        this.updateData();
    }

    UserListCtrl.prototype.addDiscipline = function addDiscipline(position) {
        var discipline = this.disciplines.list.splice(position, 1)[0];
        if (this.disciplines.creditsLeft - discipline.weight < 0) {
            alert("Дуже багато дисциплін.");
            return;
        }
        this.disciplines.creditsLeft -= discipline.weight;
        this.disciplines.selected.push(discipline);
        this.updateData();
    };

    UserListCtrl.prototype.removeDiscipline = function removeDiscipline(position) {
        var discipline = this.disciplines.selected.splice(position, 1)[0];
        if (discipline.weight > 0) {
            this.disciplines.creditsLeft += discipline.weight;
            this.disciplines.creditsStack.push(discipline.weight);
        }
        this.disciplines.list.push(discipline);
        this.updateData();
    };

    UserListCtrl.prototype.updateData = function updateData() {
        this.data.disciplines = [];
        for (var _i = 0, _l = this.disciplines.selected.length; _i < _l; _i++) {
            var i = this.disciplines.selected[_i];
            this.data.disciplines.push(i.id);
        }
    };

    UserListCtrl.prototype.submit = function submit() {
        var state = this.require("$state"),
            rt = this.require("$rootScope");
        rt.$emit("form:start");
        this.data.$update(function () {
            rt.$emit("form:end");
            state.go(state.current, {}, { reload: true });
        });
    };

    UserListCtrl.prototype.remove = function remove() {
        var state = this.require("$state"),
            rt = this.require("$rootScope");
        rt.$emit("form:start");
        this.data.$delete(function () {
            rt.$emit("form:end");
            state.go("user-list", {}, { reload: true });
        });
    };

    UserListCtrl.resolver = function resolver() {
        return {
            // spec: function($state, $stateParams, Data) {
            //   return Data.getSpecialty($stateParams.id);
            // },
            data: function data($state, $stateParams, Api) {
                return Api.user_discipline_list.get($stateParams).$promise;
            }
        };
    };

    return UserListCtrl;
})();

UserListCtrl.$inject = ["$scope", "data", "DataConvert", "$injector"];
module.exports = UserListCtrl;

},{"../../config":10}],23:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var UserListListCtrl = (function () {
    function UserListListCtrl($scope, data) {
        _classCallCheck(this, UserListListCtrl);

        $scope.lists = data.results;
    }

    UserListListCtrl.resolver = function resolver() {
        return {
            data: (function () {
                var diFunction = function diFunction($state, $stateParams, Api) {
                    return Api.user_discipline_list.query($stateParams).$promise;
                };
                diFunction.$inject = ["$state", "$stateParams", "Api"];
                return diFunction;
            })()
        };
    };

    return UserListListCtrl;
})();

UserListListCtrl.$inject = ["$scope", "data"];
module.exports = UserListListCtrl;

},{}],24:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var cfg = require("../../config");

var UserListNewCtrl = (function () {
    function UserListNewCtrl($scope, data, DataConvert, $injector) {
        _classCallCheck(this, UserListNewCtrl);

        var self = this;
        this.s = $scope;
        this.require = $injector.get;
        this.s.item = data;
        this.s.UserListCtrl = this;
        var convertedData = DataConvert([].concat(data.specialization_info.speciality.relations, data.specialization_info.relations));
        this.data = data;
        this.disciplines_list = convertedData.disciplines;
        this.disciplines = {
            creditsLeft: cfg.semester.weight * cfg.semester.count,
            creditsStack: [],
            list: [],
            selected: []
        };
        for (var _i = 0, _l = this.data.disciplines.length; _i < _l; _i++) {
            var i = this.data.disciplines[_i];
            this.disciplines.selected.push(this.disciplines_list[i]);
        }
        for (var i in this.disciplines_list) {
            if (this.disciplines_list[i].selected && this.disciplines_list[i].type == "m") {
                this.disciplines.creditsLeft -= this.disciplines_list[i].weight;
            }
            if (this.disciplines_list[i].type == "o") {
                if (!(this.data.disciplines.indexOf(this.disciplines_list[i].id) >= 0) && !this.disciplines_list[i].selected) {
                    this.disciplines.list.push(this.disciplines_list[i]);
                } else if (this.disciplines_list[i].selected) {
                    this.disciplines.selected.push(this.disciplines_list[i]);
                    this.disciplines.creditsLeft -= this.disciplines_list[i].weight;
                } else {
                    this.disciplines.creditsLeft -= this.disciplines_list[i].weight;
                }
            }
        }
        this.updateData();
    }

    UserListNewCtrl.prototype.addDiscipline = function addDiscipline(position) {
        var discipline = this.disciplines.list.splice(position, 1)[0];
        if (this.disciplines.creditsLeft - discipline.weight < 0) {
            alert("Слишком много дисциплин.");
            return;
        }
        this.disciplines.creditsLeft -= discipline.weight;
        this.disciplines.selected.push(discipline);
        this.updateData();
    };

    UserListNewCtrl.prototype.removeDiscipline = function removeDiscipline(position) {
        var discipline = this.disciplines.selected.splice(position, 1)[0];
        if (discipline.weight > 0) {
            this.disciplines.creditsLeft += discipline.weight;
            this.disciplines.creditsStack.push(discipline.weight);
        }
        this.disciplines.list.push(discipline);
        this.updateData();
    };

    UserListNewCtrl.prototype.updateData = function updateData() {
        this.data.disciplines = [];
        for (var _i = 0, _l = this.disciplines.selected.length; _i < _l; _i++) {
            var i = this.disciplines.selected[_i];
            this.data.disciplines.push(i.id);
        }
    };

    UserListNewCtrl.prototype.submit = function submit() {
        var state = this.require("$state"),
            rt = this.require("$rootScope");
        rt.$emit("form:start");
        this.data.$create(function (data) {
            rt.$emit("form:end");
            state.go("user-list.item", { id: data.id }, { reload: true });
        });
    };

    UserListNewCtrl.resolver = function resolver() {
        return {
            // spec: function($state, $stateParams, Data) {
            //   return Data.getSpecialty($stateParams.id);
            // },
            specialization: function specialization($state, $stateParams, Api) {
                return Api.specialization.get($stateParams).$promise;
            },
            data: function data($state, $stateParams, Api, specialization, $rootScope) {
                return new Api.user_discipline_list({
                    name: "",
                    description: "",
                    disciplines: [],
                    specialization: specialization.id,
                    specialization_info: specialization,
                    user: $rootScope.currentUser.id
                });
            }
        };
    };

    return UserListNewCtrl;
})();

UserListNewCtrl.$inject = ["$scope", "data", "DataConvert", "$injector"];
module.exports = UserListNewCtrl;

},{"../../config":10}],25:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var cfg = require("../../config");

var UserListViewCtrl = (function () {
    function UserListViewCtrl($scope, data, DataSort, DataConvert) {
        _classCallCheck(this, UserListViewCtrl);

        this.s = $scope;
        this.s.data = data;
        this.s.spec = data.specialization_info;
        this.s.specialtyCtrl = this;
        var convertedData = // var convertedData = DataConvert(@s.spec.relations);
        DataConvert([].concat(this.s.spec.speciality.relations, this.s.spec.relations));
        this.connections = convertedData.connections;
        this.disciplines = convertedData.disciplines;
        for (var i in this.disciplines) {
            if (this.disciplines[i].type == "o") {
                this.disciplines[i].selected = false;
            }
        }
        for (var _i = 0, _l = data.disciplines.length; _i < _l; _i++) {
            var i = data.disciplines[_i];
            this.disciplines[i].selected = true;
        }
        var defaultCategory = "user--choice";
        this.categories = convertedData.categories;
        this.categories[defaultCategory] = {
            id: defaultCategory,
            name: "Вибір студента"
        };
        for (var i in this.categories) {
            if (!this.categories.hasOwnProperty(i)) continue;
            this.categories[i].list = [];
        }
        this.sorted = DataSort(convertedData, cfg);
        for (var i in this.sorted.discs) {
            if (!this.sorted.discs.hasOwnProperty(i)) continue;
            this.categories[this.sorted.discs[i].category.id || defaultCategory].list.push(this.sorted.discs[i].id);
        }
    }

    UserListViewCtrl.resolver = function resolver() {
        return {
            // spec: function($state, $stateParams, Data) {
            //   return Data.getSpecialty($stateParams.id);
            // },
            data: function data($state, $stateParams, Api) {
                return Api.user_discipline_list.get($stateParams).$promise;
            }
        };
    };

    return UserListViewCtrl;
})();

UserListViewCtrl.$inject = ["$scope", "data", "DataSort", "DataConvert"];
module.exports = UserListViewCtrl;

},{"../../config":10}],26:[function(require,module,exports){
"use strict";

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var SelectCtrl = function SelectCtrl($scope) {
    _classCallCheck(this, SelectCtrl);
};

SelectCtrl.$inject = ["$scope"];
module.exports = function () {
    return {
        controller: SelectCtrl,
        link: function link(s) {
            (function ($element, $attrs, ctrl) {
                if (!ctrl) return;
            }).apply(s, Array.prototype.slice.call(arguments, 1));
        }
    };
};

},{}],27:[function(require,module,exports){
"use strict";

angular.module("pages.user-list", ["ui.router"]).config(require("./routes")).directive("selectList", require("./directives/selectList.js"));

},{"./directives/selectList.js":26,"./routes":28}],28:[function(require,module,exports){
"use strict";

var UserListListCtrl = require("./UserListListCtrl.js");
var UserListCtrl = require("./UserListCtrl.js");
var UserListNewCtrl = require("./UserListNewCtrl.js");
var UserListViewCtrl = require("./UserListViewCtrl.js");
module.exports = (function () {
    var diFunction = function diFunction($stateProvider) {
        $stateProvider.state("user-list", {
            url: "/user-list",
            views: {
                "body@": {
                    controller: UserListListCtrl,
                    resolve: UserListListCtrl.resolver(),
                    templateUrl: "/s/html/templates/user-list.html"
                }
            }
        }).state("user-list.item-new", {
            url: "/item/new/:id",
            views: {
                "body@": {
                    controller: UserListNewCtrl,
                    resolve: UserListNewCtrl.resolver(),
                    templateUrl: "/s/html/templates/user-list_item_new.html"
                }
            }
        }).state("user-list.item", {
            url: "/item/:id",
            views: {
                "body@": {
                    controller: UserListCtrl,
                    resolve: UserListCtrl.resolver(),
                    templateUrl: "/s/html/templates/user-list_item.html"
                }
            }
        }).state("user-list.item-view", {
            url: "/item/:id/view",
            views: {
                "body@": {
                    controller: UserListViewCtrl,
                    resolve: UserListViewCtrl.resolver(),
                    templateUrl: "/s/html/templates/user-list_item_view.html"
                }
            }
        });
    };
    diFunction.$inject = ["$stateProvider"];
    return diFunction;
})();

},{"./UserListCtrl.js":22,"./UserListListCtrl.js":23,"./UserListNewCtrl.js":24,"./UserListViewCtrl.js":25}],29:[function(require,module,exports){
"use strict";

module.exports = (function () {
    var diFunction = function diFunction($rootScope) {
        return {
            link: function link(s) {
                (function ($element, $attrs) {
                    $rootScope.$on("$stateChangeStart", start);
                    $rootScope.$on("form:start", start);
                    $rootScope.$on("$stateChangeSuccess", end);
                    $rootScope.$on("$stateChangeError", end);
                    $rootScope.$on("form:end", start);
                    function start(event, toState) {
                        $element.addClass($attrs.statePendingClass);
                    }
                    function end(event, toState) {
                        $element.removeClass($attrs.statePendingClass);
                    }
                }).apply(s, Array.prototype.slice.call(arguments, 1));
            }
        };
    };
    diFunction.$inject = ["$rootScope"];
    return diFunction;
})();

},{}]},{},[1])


//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJkZXYvanMvbWFpbi5qcyIsImRldi9qcy9BcGkuanMiLCJkZXYvanMvRGF0YS5qcyIsImRldi9qcy9EYXRhQ29udmVydC5qcyIsImRldi9qcy9EYXRhU29ydC5qcyIsImRldi9qcy9IVFRQRXJyb3JIYW5kbGVyLmpzIiwiZGV2L2pzL2F1dGgvQXV0aEN0cmwuanMiLCJkZXYvanMvYXV0aC9tb2R1bGUuanMiLCJkZXYvanMvYXV0aC9yb3V0ZXMuanMiLCJkZXYvanMvY29uZmlnLmpzIiwiZGV2L2pzL2hpc3RvcnlCYWNrLmpzIiwiZGV2L2pzL2ludGVyYWN0L2RpcmVjdGl2ZXMvaW50ZXJhY3RPYmplY3QuanMiLCJkZXYvanMvaW50ZXJhY3QvZGlyZWN0aXZlcy9pbnRlcmFjdFBhcmVudC5qcyIsImRldi9qcy9pbnRlcmFjdC9tb2R1bGUuanMiLCJkZXYvanMvcGFnZXMvc3BlY2lhbHRpZXMvU3BlY2lhbHRpZXNMaXN0Q3RybC5qcyIsImRldi9qcy9wYWdlcy9zcGVjaWFsdGllcy9TcGVjaWFsdHlDdHJsLmpzIiwiZGV2L2pzL3BhZ2VzL3NwZWNpYWx0aWVzL2RpcmVjdGl2ZXMvc3BlY2lhbHR5RExpc3QuanMiLCJkZXYvanMvcGFnZXMvc3BlY2lhbHRpZXMvZGlyZWN0aXZlcy9zcGVjaWFsdHlETGlzdEl0ZW0uanMiLCJkZXYvanMvcGFnZXMvc3BlY2lhbHRpZXMvZGlyZWN0aXZlcy9zcGVjaWFsdHlETGlzdE1hcC5qcyIsImRldi9qcy9wYWdlcy9zcGVjaWFsdGllcy9tb2R1bGUuanMiLCJkZXYvanMvcGFnZXMvc3BlY2lhbHRpZXMvcm91dGVzLmpzIiwiZGV2L2pzL3BhZ2VzL3VzZXItbGlzdC9Vc2VyTGlzdEN0cmwuanMiLCJkZXYvanMvcGFnZXMvdXNlci1saXN0L1VzZXJMaXN0TGlzdEN0cmwuanMiLCJkZXYvanMvcGFnZXMvdXNlci1saXN0L1VzZXJMaXN0TmV3Q3RybC5qcyIsImRldi9qcy9wYWdlcy91c2VyLWxpc3QvVXNlckxpc3RWaWV3Q3RybC5qcyIsImRldi9qcy9wYWdlcy91c2VyLWxpc3QvZGlyZWN0aXZlcy9zZWxlY3RMaXN0LmpzIiwiZGV2L2pzL3BhZ2VzL3VzZXItbGlzdC9tb2R1bGUuanMiLCJkZXYvanMvcGFnZXMvdXNlci1saXN0L3JvdXRlcy5qcyIsImRldi9qcy9zdGF0ZVBlbmRpbmdDbGFzcy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDO0FBQ3pCLE9BQU8sQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO0FBQ3RDLE9BQU8sQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO0FBQ3BDLElBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQ3BDLE1BQU0sRUFDTixtQkFBbUIsRUFDbkIsaUJBQWlCLEVBQ2pCLFlBQVksRUFDWixZQUFZLEVBQ1osV0FBVyxDQUNkLENBQUMsQ0FBQyxRQUFRLENBQUMsY0FBYyxFQUFFLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsbUJBQW1CLEVBQUUsT0FBTyxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxPQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxZQUFZO0FBQ3BhLFdBQU8sVUFBVSxLQUFLLEVBQUUsR0FBRyxFQUFFO0FBQ3pCLGVBQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFDO0tBQ3ZDLENBQUM7Q0FDTCxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUEsWUFBWTtBQUNsQixRQUFJLFVBQVUsR0FBRyxvQkFBVSxrQkFBa0IsRUFBRSxhQUFhLEVBQUU7QUFDMUQsMEJBQWtCLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBQzdDLHFCQUFhLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0tBQ3ZELENBQUM7QUFDRixjQUFVLENBQUMsT0FBTyxHQUFHLENBQ2pCLG9CQUFvQixFQUNwQixlQUFlLENBQ2xCLENBQUM7QUFDRixXQUFPLFVBQVUsQ0FBQztDQUNyQixDQUFBLEVBQUUsQ0FBQyxDQUFDOzs7OztBQ3hCTCxNQUFNLENBQUMsT0FBTyxHQUFHLENBQUEsWUFBWTtBQUN6QixRQUFJLFVBQVUsR0FBRyxTQUFTLEdBQUcsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRTtBQUM3RCxZQUFJLElBQUksR0FBRyxJQUFJLENBQUM7QUFDaEIsWUFBSSxXQUFXLEdBQUcscUJBQVUsR0FBRyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFO0FBQzlELGdCQUFJLEVBQUUsR0FBRyxPQUFPO2dCQUFFLE9BQU8sR0FBRztBQUNwQixxQkFBSyxFQUFFO0FBQ0gsMEJBQU0sRUFBRSxLQUFLO0FBQ2IsMkJBQU8sRUFBRTtBQUNMLHFDQUFhLEVBQUUsc0JBQVk7QUFDdkIsbUNBQU8sUUFBUSxVQUFhLENBQUM7eUJBQ2hDO3FCQUNKO2lCQUNKO0FBQ0QsbUJBQUcsRUFBRTtBQUNELDBCQUFNLEVBQUUsS0FBSztBQUNiLDBCQUFNLEVBQUUsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFO0FBQzlCLDJCQUFPLEVBQUU7QUFDTCxxQ0FBYSxFQUFFLHNCQUFZO0FBQ3ZCLG1DQUFPLFFBQVEsVUFBYSxDQUFDO3lCQUNoQztxQkFDSjtpQkFDSjtBQUNELHdCQUFRLEVBQUU7QUFDTiwwQkFBTSxFQUFFLFFBQVE7QUFDaEIsMkJBQU8sRUFBRTtBQUNMLHFDQUFhLEVBQUUsc0JBQVk7QUFDdkIsbUNBQU8sUUFBUSxVQUFhLENBQUM7eUJBQ2hDO3FCQUNKO2lCQUNKO0FBQ0Qsc0JBQU0sRUFBRTtBQUNKLDBCQUFNLEVBQUUsTUFBTTtBQUNkLDJCQUFPLEVBQUU7QUFDTCxxQ0FBYSxFQUFFLHNCQUFZO0FBQ3ZCLG1DQUFPLFFBQVEsVUFBYSxDQUFDO3lCQUNoQztxQkFDSjtpQkFDSjtBQUNELHNCQUFNLEVBQUU7QUFDSiwwQkFBTSxFQUFFLEtBQUs7QUFDYiwyQkFBTyxFQUFFO0FBQ0wscUNBQWEsRUFBRSxzQkFBWTtBQUN2QixtQ0FBTyxRQUFRLFVBQWEsQ0FBQzt5QkFDaEM7cUJBQ0o7aUJBQ0o7QUFDRCxxQkFBSyxFQUFFO0FBQ0gsMEJBQU0sRUFBRSxPQUFPO0FBQ2YsMkJBQU8sRUFBRTtBQUNMLHFDQUFhLEVBQUUsc0JBQVk7QUFDdkIsbUNBQU8sUUFBUSxVQUFhLENBQUM7eUJBQ2hDO3FCQUNKO2lCQUNKO2FBQ0osQ0FBQztBQUNOLG1CQUFPLFNBQVMsQ0FBQyxZQUFZLEdBQUcsR0FBRyxFQUFFLGFBQWEsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLE9BQU8sSUFBSSxFQUFFLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztTQUNsSCxDQUFDO0FBQ0YsWUFBSSxrQkFBa0IsR0FBRyw0QkFBVSxJQUFJLEVBQUU7QUFDckMsbUJBQU8sV0FBVyxDQUFDLElBQUksR0FBRyxvQkFBb0IsRUFBRTtBQUM1QyxrQkFBRSxFQUFFLEtBQUs7QUFDVCwwQkFBVSxFQUFFLGFBQWE7YUFDNUIsQ0FBQyxDQUFDO1NBQ04sQ0FBQztBQUNGLFlBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDO0FBQzVCLFlBQUksQ0FBQyxlQUFlLEdBQUcsa0JBQWtCLENBQUM7QUFDMUMsWUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLENBQUM7QUFDN0QsWUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDO0FBQ3JELFlBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLHNCQUFzQixDQUFDLENBQUM7S0FDNUUsQ0FBQztBQUNGLGNBQVUsQ0FBQyxPQUFPLEdBQUcsQ0FDakIsV0FBVyxFQUNYLFVBQVUsRUFDVixjQUFjLENBQ2pCLENBQUM7QUFDRixXQUFPLFVBQVUsQ0FBQztDQUNyQixDQUFBLEVBQUUsQ0FBQzs7Ozs7OztBQzNFSixJQUFJLEdBQUcsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7O0lBQ3hCLFNBQVMsR0FDQSxTQURULFNBQVMsQ0FDQyxFQUFFLEVBQUUsSUFBSSxFQUFFOzBCQURwQixTQUFTOztBQUVQLFFBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDO0FBQ2IsUUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7QUFDakIsUUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLEdBQUcsMEZBQXdiLENBQUM7Q0FDcGQ7O0lBRUMsVUFBVSxHQUNELFNBRFQsVUFBVSxDQUNBLEVBQUUsRUFBRSxJQUFJLEVBQUU7MEJBRHBCLFVBQVU7O0FBRVIsUUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUM7QUFDYixRQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztBQUNqQixRQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsR0FBRywwRkFBd2IsQ0FBQztBQUNqZCxRQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUEsQUFBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0FBQ2xFLFFBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO0FBQ2IsUUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7QUFDZixRQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQztDQUNoQjs7QUFFTCxJQUFJLFVBQVUsR0FBRyxJQUFJO0lBQUUsV0FBVyxHQUFHLENBQUMsQ0FBQztBQUN2QyxJQUFJLEtBQUssR0FBRyxpQkFBWTtBQUNwQixlQUFXLEVBQUUsQ0FBQztBQUNkLFdBQU8sVUFBVSxHQUFHLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQztDQUM5QyxDQUFDO0FBQ0YsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLE9BQU8sR0FBRyxZQUFZO0FBQ3BDLFFBQUksT0FBTyxHQUFHLEVBQUU7UUFBRSxPQUFPLEdBQUcsRUFBRSxDQUFDO0FBQy9CLFFBQUksSUFBSSxHQUFHO0FBQ1AsWUFBSSxFQUFFO0FBQ0YsaUJBQUssRUFBRSxFQUFFO0FBQ1QsaUJBQUssRUFBRSxFQUFFO1NBQ1o7QUFDRCxtQkFBVyxFQUFFO0FBQ1QsZ0JBQUksRUFBRSxFQUFFO0FBQ1IsY0FBRSxFQUFFLEVBQUU7U0FDVDtBQUNELG9CQUFZLEVBQUUsc0JBQVUsSUFBSSxFQUFFO0FBQzFCLG1CQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2hDO0FBQ0Qsa0NBQTBCLEVBQUUsb0NBQVUsSUFBSSxFQUFFO0FBQ3hDLGdCQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7QUFDZixnQkFBSSxXQUFXLEdBQUc7QUFDZCxvQkFBSSxFQUFFLEVBQUU7QUFDUixrQkFBRSxFQUFFLEVBQUU7YUFDVCxDQUFDO0FBQ0YsaUJBQUssSUFBSSxFQUFFLEdBQUcsQ0FBQyxFQUFFLEVBQUUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRTtBQUNyRSxvQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7QUFDM0MscUJBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7QUFDbEQscUJBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDO0FBQzNCLHFCQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztBQUM1QixvQkFBSSxPQUFPLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksV0FBVyxFQUFFO0FBQzlDLCtCQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztpQkFDL0I7QUFDRCxvQkFBSSxPQUFPLFdBQVcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksV0FBVyxFQUFFO0FBQzVDLCtCQUFXLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztpQkFDN0I7QUFDRCxxQkFBSyxJQUFJLElBQUksR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxJQUFJLEdBQUcsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO0FBQy9FLHdCQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUNoRCx3QkFBSSxPQUFPLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxXQUFXLEVBQUU7QUFDdkMsNkJBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDeEQsNkJBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDO0FBQ25DLDZCQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztBQUNoQyw0QkFBSSxPQUFPLFdBQVcsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksV0FBVyxFQUFFO0FBQy9DLHVDQUFXLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQztBQUM3Qix1Q0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUM7eUJBQ2xDO0FBQ0QsbUNBQVcsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ25DLG1DQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDeEM7aUJBQ0o7QUFDRCxxQkFBSyxJQUFJLElBQUksR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxJQUFJLEdBQUcsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO0FBQzdFLHdCQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUM5Qyx3QkFBSSxPQUFPLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxXQUFXLElBQUksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxXQUFXLEVBQUU7QUFDMUYsNkJBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDeEQsNkJBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDO0FBQ25DLDZCQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztBQUNoQyw0QkFBSSxPQUFPLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksV0FBVyxFQUFFO0FBQ2pELHVDQUFXLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQztBQUM3Qix1Q0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUM7eUJBQ2xDO0FBQ0QsbUNBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3JDLG1DQUFXLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDdEM7aUJBQ0o7YUFDSjtBQUNELG1CQUFPO0FBQ0gscUJBQUssRUFBRSxLQUFLO0FBQ1osMkJBQVcsRUFBRSxXQUFXO2FBQzNCLENBQUM7U0FDTDtLQUNKLENBQUM7QUFDRixRQUFJLEtBQUssR0FBRyxDQUNSLDBDQUFtTyxFQUNuTyxxQ0FBME0sRUFDMU0sOEJBQXFLLEVBQ3JLLCtCQUFzSyxFQUN0SyxxQkFBK0csRUFDL0csMkJBQW1KLEVBQ25KLDBCQUE2SSxFQUM3SSwyREFBK1QsRUFDL1QsMEJBQTZJLEVBQzdJLDZCQUEwSixFQUMxSixxQ0FBcU0sRUFDck0sc0NBQWlNLEVBQ2pNLDBCQUE2SSxFQUM3SSxzQ0FBZ04sRUFDaE4sbUNBQXlMLEVBQ3pMLGlEQUFtUSxFQUNuUSwyQ0FBb08sRUFDcE8sZ0NBQXVLLEVBQ3ZLLDhCQUFnSyxFQUNoSyw4Q0FBc1AsRUFDdFAsa0NBQXdMLEVBQ3hMLGtDQUF3TCxFQUN4TCxzQ0FBZ04sRUFDaE4scUNBQXFNLEVBQ3JNLDZCQUFxSixFQUNySixnQ0FBdUssRUFDdkssZ0RBQTZQLENBQ2hRLENBQUM7QUFDRixRQUFJLEVBQUUsR0FBRyxLQUFLLENBQUMsTUFBTTtRQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7QUFDOUIsUUFBSSxFQUFFLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxLQUFLO1FBQUUsRUFBRSxHQUFHLEdBQUcsQ0FBQyxVQUFVLENBQUMsS0FBSztRQUFFLElBQUksR0FBRyxHQUFHLENBQUMsV0FBVyxDQUFDO0FBQ2hGLFNBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDekIsWUFBSSxFQUFFLEdBQUcsS0FBSyxFQUFFLENBQUM7QUFDakIsVUFBRSxFQUFFLENBQUM7QUFDTCxZQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLFNBQVMsQ0FBQyxFQUFFLEVBQUUsS0FBSyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQ3hELFlBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztBQUMvQixZQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7QUFDN0IsZUFBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUNwQjtBQUNELFNBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDekIsWUFBSSxFQUFFLEdBQUcsS0FBSyxFQUFFLENBQUM7QUFDakIsVUFBRSxFQUFFLENBQUM7QUFDTCxZQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLFVBQVUsQ0FBQyxFQUFFLEVBQUUsS0FBSyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQ3pELFlBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztBQUMvQixZQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7QUFDN0IsZUFBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUNwQjtBQUNELFNBQUssSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7QUFDM0IsYUFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUMzQixnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQSxBQUFDLENBQUMsQ0FBQztBQUNoRCxtQkFBTyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFO0FBQ3pELG9CQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQSxBQUFDLENBQUMsQ0FBQzthQUMvQzs7QUFFRCxnQkFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0FBQzdDLGdCQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDOUM7S0FDSjtBQUNELFNBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEdBQUcsR0FBRyxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQy9CLFlBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUEsQUFBQyxDQUFDO1lBQUUsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUEsQUFBQyxDQUFDLENBQUM7QUFDOUYsWUFBSSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQSxBQUFDLEVBQUU7QUFDekksZ0JBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztBQUMxRCxnQkFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQzNEO0tBQ0o7QUFDRCxXQUFPLElBQUksQ0FBQztDQUNmLENBQUM7Ozs7Ozs7QUM1SkYsSUFBSSxHQUFHLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0FBQzlCLFNBQVMsVUFBVSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUU7QUFDN0IsUUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO0FBQzVCLGFBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDcEI7Q0FDSjs7SUFDSyxVQUFVLEdBQ0QsU0FEVCxVQUFVLENBQ0EsSUFBSSxFQUFFLFFBQVEsRUFBRTswQkFEMUIsVUFBVTs7QUFFUixRQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO0FBQ3ZCLFFBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztBQUN0QixRQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7QUFDdEIsUUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO0FBQ3BDLFFBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO0FBQ3pCLFFBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUM7QUFDbEQsUUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQztBQUMzQixRQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQSxHQUFJLENBQUMsQ0FBQyxDQUFDO0FBQzlDLFFBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxFQUFFLENBQUMsYUFBYSxJQUFJLENBQUMsQ0FBQSxHQUFJLENBQUMsQ0FBQztBQUNqRCxRQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsRUFBRSxDQUFDLFdBQVcsSUFBSSxDQUFDLENBQUEsR0FBSSxDQUFDLENBQUM7QUFDN0MsUUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQztBQUNsQyxRQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQztBQUNiLFFBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO0FBQ2YsUUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7Q0FDaEI7O0FBRUwsTUFBTSxDQUFDLE9BQU8sR0FBRyxTQUFTLFdBQVcsR0FBRztBQUNwQyxXQUFPLFVBQVUsSUFBSSxFQUFFO0FBQ25CLFlBQUksTUFBTSxHQUFHO0FBQ1QsdUJBQVcsRUFBRSxFQUFFO0FBQ2Ysc0JBQVUsRUFBRSxFQUFFO0FBQ2QsdUJBQVcsRUFBRTtBQUNULG9CQUFJLEVBQUUsRUFBRTtBQUNSLGtCQUFFLEVBQUUsRUFBRTtBQUNOLHFCQUFLLEVBQUUsRUFBRTthQUNaO1NBQ0osQ0FBQztBQUNGO0FBQ0EseUJBQWlCLEdBQUcsMkJBQVUsSUFBSSxFQUFFLFFBQVEsRUFBRTtBQUMxQyxnQkFBSSxPQUFPLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLFdBQVcsRUFBRTtBQUNuRCxvQkFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0FBQ3pFLG9CQUFJLE9BQU8sTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLFdBQVcsSUFBSSxPQUFPLEtBQUssQ0FBQyxRQUFRLENBQUMsRUFBRSxLQUFLLFdBQVcsRUFBRTtBQUN4RywwQkFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7aUJBQ3pEO0FBQ0Qsb0JBQUksT0FBTyxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksV0FBVyxFQUFFO0FBQzVELDBCQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO2lCQUM3QztBQUNELHNCQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUN2RCxNQUFNO0FBQ0gsb0JBQUksUUFBUSxFQUFFO0FBQ1YsMEJBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7aUJBQy9DO0FBQ0Qsb0JBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFBLENBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFBLENBQUUsSUFBSSxJQUFJLEdBQUcsRUFBRTtBQUN2QywwQkFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztpQkFDMUM7YUFDSjtTQUNKLENBQUM7QUFDRixZQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7QUFDakIsYUFBSyxJQUFJLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRTtBQUM5QyxnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ3BCLDZCQUFpQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztBQUM5QixnQkFBSSxPQUFPLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxXQUFXLEVBQUU7QUFDeEQsc0JBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDekM7QUFDRCxnQkFBSSxPQUFPLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLFdBQVcsRUFBRTtBQUNuRCxzQkFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUN2QztTQUNKO0FBQ0QsYUFBSyxJQUFJLElBQUksR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxHQUFHLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBRTtBQUN4RCxnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3RCLGlCQUFLLElBQUksSUFBSSxHQUFHLENBQUMsRUFBRSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxHQUFHLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBRTtBQUM3RCxvQkFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUM5QixxQkFBSyxJQUFJLElBQUksR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxHQUFHLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBRTtBQUM1RSx3QkFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO0FBQzNDLHdCQUFJLEtBQUssQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLEVBQUUsRUFDbkIsU0FBUztBQUNiLHFDQUFpQixDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztBQUNoQyx3QkFBSSxPQUFPLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxXQUFXLEVBQUU7QUFDekQsOEJBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7cUJBQzFDO0FBQ0Qsd0JBQUksT0FBTyxNQUFNLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksV0FBVyxFQUFFO0FBQ3ZELDhCQUFNLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDO3FCQUN4QztBQUNELDhCQUFVLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUN2RCw4QkFBVSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQ3hEO0FBQ0QscUJBQUssSUFBSSxJQUFJLEdBQUcsQ0FBQyxFQUFFLElBQUksR0FBRyxPQUFPLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxJQUFJLEdBQUcsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO0FBQzFFLHdCQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3pDLHdCQUFJLEtBQUssQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLEVBQUUsRUFDbkIsU0FBUztBQUNiLHFDQUFpQixDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztBQUNoQyx3QkFBSSxPQUFPLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxXQUFXLEVBQUU7QUFDekQsOEJBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7cUJBQzFDO0FBQ0Qsd0JBQUksT0FBTyxNQUFNLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksV0FBVyxFQUFFO0FBQ3ZELDhCQUFNLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDO3FCQUN4QztpQkFDSjthQUNKO0FBQ0QsaUJBQUssSUFBSSxJQUFJLEdBQUcsQ0FBQyxFQUFFLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUFJLEdBQUcsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO0FBQy9ELG9CQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ2hDLHFCQUFLLElBQUksSUFBSSxHQUFHLENBQUMsRUFBRSxJQUFJLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsSUFBSSxHQUFHLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBRTtBQUMxRSx3QkFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN6Qyx3QkFBSSxLQUFLLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxFQUFFLEVBQ25CLFNBQVM7QUFDYixxQ0FBaUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7QUFDaEMsd0JBQUksT0FBTyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksV0FBVyxFQUFFO0FBQ3pELDhCQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDO3FCQUMxQztBQUNELHdCQUFJLE9BQU8sTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLFdBQVcsRUFBRTtBQUN2RCw4QkFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztxQkFDeEM7QUFDRCw4QkFBVSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7QUFDckQsOEJBQVUsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUMxRDtBQUNELHFCQUFLLElBQUksSUFBSSxHQUFHLENBQUMsRUFBRSxJQUFJLEdBQUcsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxJQUFJLEdBQUcsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO0FBQzVFLHdCQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDM0Msd0JBQUksS0FBSyxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsRUFBRSxFQUNuQixTQUFTO0FBQ2IscUNBQWlCLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0FBQ2hDLHdCQUFJLE9BQU8sTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLFdBQVcsRUFBRTtBQUN6RCw4QkFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztxQkFDMUM7QUFDRCx3QkFBSSxPQUFPLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxXQUFXLEVBQUU7QUFDdkQsOEJBQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7cUJBQ3hDO2lCQUNKO2FBQ0o7U0FDSjtBQUNELGVBQU8sTUFBTSxDQUFDO0tBQ2pCLENBQUM7Q0FDTCxDQUFDOzs7OztBQ2pJRixNQUFNLENBQUMsTUFBTSxHQUFHLFVBQVUsR0FBRyxFQUFFLElBQUksRUFBRTtBQUNqQyxPQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7QUFDdEMsU0FBSyxJQUFJLEdBQUcsSUFBSSxHQUFHLEVBQUU7QUFDakIsWUFBSSxHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRTtBQUNqRCxtQkFBTyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDbkI7S0FDSjtBQUNELFdBQU8sR0FBRyxDQUFDO0NBQ2QsQ0FBQztBQUNGLE1BQU0sQ0FBQyxPQUFPLEdBQUcsU0FBUyxRQUFRLEdBQUc7QUFDakMsV0FBTyxVQUFVLElBQUksRUFBRSxHQUFHLEVBQUU7QUFDeEIsWUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO0FBQ2YsWUFBSSxlQUFlLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztBQUMxRCxZQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQ3hELFlBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0FBQzdDLGFBQUssQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLFVBQVUsSUFBSSxFQUFFO0FBQ2pFLG1CQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7U0FDeEIsQ0FBQyxDQUFDO0FBQ0gsWUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQztBQUM5QixZQUFJLFdBQVcsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ3ZELFlBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQzNDLFlBQUksVUFBVSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxFQUFFO0FBQzFDLG1CQUFPLENBQUMsR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztTQUMxQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQ04sWUFBSSxlQUFlLEdBQUcsQ0FBQztZQUFFLGFBQWEsR0FBRyxDQUFDLENBQUM7QUFDM0MsWUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO0FBQ2pCLFlBQUksaUJBQWlCLEdBQUcsMkJBQVUsSUFBSSxFQUFFO0FBQ3BDLGlCQUFLLElBQUksR0FBRyxJQUFJLElBQUksRUFBRTtBQUNsQixvQkFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQ3pCLFNBQVM7QUFDYixvQkFBSSxPQUFPLEtBQUssQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssV0FBVyxFQUFFO0FBQy9DLDJCQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDcEIsTUFBTTtBQUNILHlCQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtBQUM3Qyw0QkFBSSxPQUFPLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssV0FBVyxFQUFFO0FBQzFELGdDQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztBQUN6QiwrQkFBRyxFQUFFLENBQUM7eUJBQ1Q7cUJBQ0o7aUJBQ0o7YUFDSjtBQUNELG1CQUFPLElBQUksQ0FBQztTQUNmLENBQUM7QUFDRjtBQUNBLG1CQUFXLEdBQUc7QUFDVixnQkFBSSxFQUFFLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO0FBQy9DLGNBQUUsRUFBRSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQztTQUM5QyxDQUFDO0FBQ0YsWUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7QUFDN0I7OztBQUdJLFlBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7QUFFbEMsaUJBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7QUFDeEU7QUFDSSxpQkFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsSUFBSSxDQUFDLEVBQUU7QUFDcEMsb0JBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsS0FBSyxDQUFDLENBQUMsRUFBRTtBQUNwQyx5QkFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDO2lCQUMvRDtBQUNEO0FBQ0EsK0JBQWUsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUEsQUFBQyxHQUFHLENBQUMsQ0FBQztBQUNqSCwrQkFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNyRCxxQkFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO0FBQzVFLHFCQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUEsR0FBSSxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztBQUM5RSxxQkFBSyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxFQUFFLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQy9FLGlDQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztBQUNuQixtQ0FBZSxDQUFDLENBQUMsQ0FBQyxJQUFJLGVBQWUsQ0FBQztpQkFDekM7QUFDRCwwQkFBVSxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFDckMscUJBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2FBQzlCO1NBQ0o7QUFDRCxlQUFPLFVBQVUsR0FBRyxDQUFDLEVBQUU7QUFDbkIsZ0JBQUksTUFBTSxHQUFHLEtBQUssQ0FBQztBQUNuQjtBQUNJLGdCQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDbEMsb0JBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7QUFDNUI7QUFDSSxtQ0FBZSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFO0FBQ3pELHVDQUFlLEVBQUUsQ0FBQztBQUNsQiw4QkFBTTtxQkFDVCxNQUFNLElBQUksZUFBZSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLGFBQWEsQ0FBQyxlQUFlLENBQUMsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtBQUN2SCw0QkFBSSxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRTtBQUNwRCxxQ0FBUzt5QkFDWjtxQkFDSjtBQUNEO0FBQ0ksZ0NBQVksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxFQUFFO0FBQ2pELDhCQUFNO3FCQUNUO0FBQ0Qsd0JBQUksR0FBRyxHQUFHLENBQUM7d0JBQUUsVUFBVSxHQUFHLEtBQUssQ0FBQztBQUNoQzs7O0FBR0ksMkJBQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQy9CLGtDQUFVLEdBQUcsSUFBSSxDQUFDO0FBQ2xCLDJCQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ3JCLCtCQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUMxQixNQUFNO0FBQ0gsMkJBQUcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO3FCQUM3QjtBQUNEO0FBQ0ksK0JBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQ3BELHFDQUFhLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQztBQUNqQyxtQ0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDL0M7QUFDRCx3QkFBSSxHQUFHLEdBQUcsZUFBZSxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFO0FBQzdDLDJCQUFHLEdBQUcsZUFBZSxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO3FCQUMvQztBQUNELHdCQUFJLFVBQVUsRUFBRTtBQUNaLDZCQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztBQUM1Qiw2QkFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7cUJBQ3ZDO0FBQ0QseUJBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO0FBQzFCLHlCQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztBQUM1Qix5QkFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztBQUM1Qix5QkFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDO0FBQzlCLHlCQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUEsR0FBSSxDQUFDLENBQUM7QUFDN0csbUNBQWUsQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDO0FBQ25DLDBCQUFNLEdBQUcsSUFBSSxDQUFDO0FBQ2Qsd0JBQUksYUFBYSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsZUFBZSxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFO0FBQzdFLHFDQUFhLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxlQUFlLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7cUJBQy9FO0FBQ0Qsd0JBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7QUFDN0IsK0JBQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztxQkFDaEQ7QUFDRCw4QkFBVSxFQUFFLENBQUM7aUJBQ2hCO2FBQ0o7QUFDRCxnQkFBSSxDQUFDLE1BQU0sRUFBRTtBQUNULCtCQUFlLEVBQUUsQ0FBQzthQUNyQjtTQUNKO0FBQ0QsWUFBSSxJQUFJLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsYUFBYSxDQUFDO0FBQy9DO0FBQ0ksWUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQ2xDLGlCQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDekMscUJBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFBLEdBQUksYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ3pEO0FBQ0QsaUJBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQSxHQUFJLElBQUksQ0FBQztBQUNsRixpQkFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQztBQUMvRCxpQkFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFBLEdBQUksSUFBSSxDQUFDO0FBQzlFLGdCQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsYUFBYSxHQUFHLENBQUMsRUFBRTtBQUN6QyxxQkFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxhQUFhLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxhQUFhLENBQUM7YUFDOUU7QUFDRCxpQkFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7U0FDbEU7QUFDRCx1QkFBZSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQy9ELHFCQUFhLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDN0QsZUFBTztBQUNILGlCQUFLLEVBQUUsS0FBSztBQUNaLGlCQUFLLEVBQUUsS0FBSztBQUNaLG1CQUFPLEVBQUUsZUFBZTtBQUN4QixxQkFBUyxFQUFFLGFBQWE7QUFDeEIsaUJBQUssRUFBRSxhQUFhO1NBQ3ZCLENBQUM7QUFDRixpQkFBUyxJQUFJLENBQUMsRUFBRSxFQUFFO0FBQ2QsZ0JBQUksS0FBSyxHQUFHLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDbEMsaUJBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtBQUN0RCxvQkFBSSxHQUFHLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNsQyxvQkFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUU7QUFDbkMseUJBQUssR0FBRyxHQUFHLENBQUM7aUJBQ2Y7YUFDSjtBQUNELGdCQUFJLE9BQU8sS0FBSyxJQUFJLFdBQVcsRUFBRTtBQUM3Qix1QkFBTyxDQUFDLENBQUM7YUFDWixNQUFNO0FBQ0gsdUJBQU8sS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQzthQUMzQjtTQUNKO0FBQ0QsaUJBQVMsVUFBVSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUU7QUFDdEIsZ0JBQUksRUFBRSxHQUFHLFdBQVcsQ0FBQyxFQUFFO2dCQUFFLElBQUksR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDO0FBQ2pELGdCQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ2hKLG1CQUFPLENBQUMsQ0FBQztTQUNaO0FBQ0QsaUJBQVMsSUFBSSxDQUFDLEdBQUcsRUFBRTtBQUNmLGlCQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsRUFBRSxFQUFFO0FBQy9DLHFCQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsRUFBRSxFQUFFO0FBQzlCLHdCQUFJLEdBQUcsS0FBSyxHQUFHLEVBQUU7QUFDYiw0QkFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsRUFBRTtBQUMvRixnQ0FBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ3BCLGdDQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ3RCLGdDQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO3lCQUNuQjtxQkFDSjtpQkFDSjthQUNKO1NBQ0o7S0FDSixDQUFDO0NBQ0wsQ0FBQztBQUNGLFNBQVMsUUFBUSxDQUFDLEtBQUssRUFBRSxHQUFHLEVBQUU7QUFDMUIsUUFBSSxHQUFHLEdBQUcsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDM0IsU0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUM1QixXQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7S0FDNUM7QUFDRCxXQUFPLEdBQUcsQ0FBQztDQUNkO0FBQ0QsU0FBUyxZQUFZLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRTtBQUM1QixRQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO0FBQ3RDLGVBQU8sS0FBSyxDQUFDO0tBQ2hCO0FBQ0QsUUFBSSxHQUFHLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLEVBQUU7QUFDekIsWUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDO0FBQ2QsV0FBRyxHQUFHLEdBQUcsQ0FBQztBQUNWLFdBQUcsR0FBRyxHQUFHLENBQUM7S0FDYjtBQUNELFNBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDeEMsWUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRTtBQUMxQixtQkFBTyxJQUFJLENBQUM7U0FDZjtLQUNKO0FBQ0QsV0FBTyxLQUFLLENBQUM7Q0FDaEI7Ozs7O0FDck5ELE1BQU0sQ0FBQyxPQUFPLEdBQUcsQ0FBQSxZQUFZO0FBQ3pCLFFBQUksVUFBVSxHQUFHLFNBQVMsZ0JBQWdCLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUU7QUFDNUQsZUFBTztBQUNILDJCQUFpQix1QkFBVSxTQUFTLEVBQUU7QUFDbEM7Ozs7OztBQU1JLHlCQUFTLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtBQUN6Qiw2QkFBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO2lCQUNuRTtBQUNELHVCQUFPLEVBQUUsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDL0I7U0FDSixDQUFDO0tBQ0wsQ0FBQztBQUNGLGNBQVUsQ0FBQyxPQUFPLEdBQUcsQ0FDakIsSUFBSSxFQUNKLE1BQU0sRUFDTixXQUFXLENBQ2QsQ0FBQztBQUNGLFdBQU8sVUFBVSxDQUFDO0NBQ3JCLENBQUEsRUFBRSxDQUFDOzs7Ozs7O0lDdkJFLFFBQVE7QUFDQyxhQURULFFBQVEsQ0FDRSxVQUFVLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRTs4QkFEbEUsUUFBUTs7QUFFTixZQUFJLElBQUksR0FBRyxJQUFJLENBQUM7QUFDaEIsWUFBSSxDQUFDLE1BQU0sR0FBRyxZQUFZO0FBQ3RCLGlCQUFLLENBQUM7QUFDRixzQkFBTSxFQUFFLE1BQU07QUFDZCxtQkFBRyxFQUFFLHNCQUFzQjtBQUMzQixvQkFBSSxFQUFFLElBQUksQ0FBQyxTQUFTO0FBQ3BCLHVCQUFPLEVBQUU7QUFDTCxpQ0FBYSxFQUFFLHNCQUFZO0FBQ3ZCLCtCQUFPLFFBQVEsVUFBYSxDQUFDO3FCQUNoQztpQkFDSjthQUNKLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUU7QUFDaEQsMEJBQVUsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0FBQzlCLHNCQUFNLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQzthQUN6RCxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQ2pELENBQUMsQ0FBQztTQUNOLENBQUM7QUFDRixZQUFJLENBQUMsU0FBUyxHQUFHO0FBQ2Isb0JBQVEsRUFBRSxFQUFFO0FBQ1osb0JBQVEsRUFBRSxFQUFFO1NBQ2YsQ0FBQztBQUNGLGNBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0tBQzFCOztBQXhCQyxZQUFRLENBeUJILFFBQVEsR0FBQSxvQkFBRztBQUNkLGVBQU8sRUFBRSxDQUFDO0tBQ2I7O1dBM0JDLFFBQVE7OztBQTZCZCxRQUFRLENBQUMsT0FBTyxHQUFHLENBQ2YsWUFBWSxFQUNaLFFBQVEsRUFDUixPQUFPLEVBQ1AsV0FBVyxFQUNYLFFBQVEsRUFDUixVQUFVLENBQ2IsQ0FBQztBQUNGLE1BQU0sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDOzs7OztBQ3JDMUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxVQUFVLEVBQUUsU0FBUyxFQUFFOztBQUVuRyxhQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQ25CLGNBQU0sRUFBRSxLQUFLO0FBQ2IsV0FBRyxFQUFFLGNBQWM7QUFDbkIsZUFBTyxFQUFFO0FBQ0wseUJBQWEsRUFBRSxzQkFBWTtBQUN2Qix1QkFBTyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxVQUFhLENBQUM7YUFDakQ7U0FDSjtLQUNKLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUU7QUFDaEQsa0JBQVUsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0tBQ2pDLENBQUMsQ0FBQztBQUNILGNBQVUsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsVUFBVSxLQUFLLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRTtBQUNwRSxZQUFJLFlBQVksR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFBLENBQUUsWUFBWSxDQUFDO0FBQ3JELFlBQUksWUFBWSxJQUFJLE9BQU8sVUFBVSxDQUFDLFdBQVcsS0FBSyxXQUFXLEVBQUU7QUFDL0QsaUJBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztBQUN2QixxQkFBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO1NBQ25FO0tBQ0osQ0FBQyxDQUFDO0NBQ04sQ0FBQyxDQUFDOzs7OztBQ3BCSCxJQUFJLFFBQVEsR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7QUFDeEMsTUFBTSxDQUFDLE9BQU8sR0FBRyxDQUFBLFlBQVk7QUFDekIsUUFBSSxVQUFVLEdBQUcsb0JBQVUsY0FBYyxFQUFFO0FBQ3ZDLHNCQUFjLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtBQUN6QixlQUFHLEVBQUUsYUFBYTtBQUNsQixpQkFBSyxFQUFFO0FBQ0gsdUJBQU8sRUFBRTtBQUNMLDhCQUFVLEVBQUUsUUFBUTtBQUNwQiwyQkFBTyxFQUFFLFFBQVEsQ0FBQyxRQUFRLEVBQUU7QUFDNUIsK0JBQVcsRUFBRSw4QkFBOEI7aUJBQzlDO2FBQ0o7U0FDSixDQUFDLENBQUM7S0FDTixDQUFDO0FBQ0YsY0FBVSxDQUFDLE9BQU8sR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7QUFDeEMsV0FBTyxVQUFVLENBQUM7Q0FDckIsQ0FBQSxFQUFFLENBQUM7Ozs7O0FDaEJKLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLEdBQUc7QUFDeEIsWUFBUSxFQUFFO0FBQ04sYUFBSyxFQUFFLENBQUM7QUFDUixjQUFNLEVBQUUsRUFBRTtBQUNWLGFBQUssRUFBRSxDQUFDO0tBQ1g7QUFDRCxjQUFVLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFO0FBQ3pCLGVBQVcsRUFBRSxFQUFFO0FBQ2YsYUFBUyxFQUFFLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRTtDQUMxQixDQUFDOzs7OztBQ1RGLE1BQU0sQ0FBQyxPQUFPLEdBQUcsQ0FBQSxZQUFZO0FBQ3pCLFFBQUksVUFBVSxHQUFHLG9CQUFVLE9BQU8sRUFBRTtBQUNoQyxlQUFPO0FBQ0gsZ0JBQUksRUFBRSxjQUFVLENBQUMsRUFBRTtBQUNmLEFBQUMsaUJBQUEsVUFBVSxRQUFRLEVBQUU7QUFDakIsNEJBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFlBQVk7QUFDN0IsK0JBQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7cUJBQzFCLENBQUMsQ0FBQztpQkFDTixDQUFBLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUU7YUFDekQ7U0FDSixDQUFDO0tBQ0wsQ0FBQztBQUNGLGNBQVUsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztBQUNqQyxXQUFPLFVBQVUsQ0FBQztDQUNyQixDQUFBLEVBQUUsQ0FBQzs7Ozs7QUNkSixNQUFNLENBQUMsT0FBTyxHQUFHLENBQUEsWUFBWTtBQUN6QixRQUFJLFVBQVUsR0FBRyxvQkFBVSxTQUFTLEVBQUU7QUFDbEMsZUFBTztBQUNILG1CQUFPLEVBQUUsaUJBQWlCO0FBQzFCLGdCQUFJLEVBQUUsY0FBVSxDQUFDLEVBQUU7QUFDZixBQUFDLGlCQUFBLFVBQVUsUUFBUSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7QUFDL0Isd0JBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDO2lCQUMxQixDQUFBLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUU7YUFDekQ7U0FDSixDQUFDO0tBQ0wsQ0FBQztBQUNGLGNBQVUsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUNuQyxXQUFPLFVBQVUsQ0FBQztDQUNyQixDQUFBLEVBQUUsQ0FBQzs7Ozs7QUNiSixTQUFTLGVBQWUsQ0FBQyxlQUFlLEVBQUU7QUFDdEMsUUFBSSxlQUFlLEtBQUssTUFBTSxFQUFFO0FBQzVCLFlBQUksR0FBRyxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsRUFBRTtBQUMzRixtQkFBTyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDeEIsQ0FBQyxDQUFDO0tBQ04sTUFBTTtBQUNILFlBQUksR0FBRyxHQUFHLENBQ04sQ0FBQyxFQUNELENBQUMsRUFDRCxDQUFDLEVBQ0QsQ0FBQyxFQUNELENBQUMsRUFDRCxDQUFDLENBQ0osQ0FBQztLQUNMO0FBQ0QsV0FBTyxHQUFHLENBQUM7Q0FDZDtBQUNELE1BQU0sQ0FBQyxPQUFPLEdBQUcsQ0FBQSxZQUFZO0FBQ3pCLFFBQUksVUFBVSxHQUFHLG9CQUFVLFNBQVMsRUFBRTtBQUNsQyxlQUFPO0FBQ0gsc0JBQVUsRUFBRSxzQkFBWTtBQUNwQixvQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO0FBQ2hCLG9CQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztBQUNqQixvQkFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7QUFDM0Isb0JBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO0FBQzNCLHlCQUFTLElBQUksQ0FBQyxLQUFLLEVBQUU7QUFDakIsd0JBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO0FBQ2hCLDRCQUFJLFdBQVcsR0FBRyxJQUFJOzRCQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO0FBQ3pDLDRCQUFJLE1BQU0sR0FBRyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztBQUMzRCw0QkFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ3RCLDRCQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO0FBQ2xDLHlCQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUMxRCw4QkFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssR0FBRyxXQUFXLEdBQUcsQ0FBQyxDQUFDO0FBQ3BDLDhCQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ3RCLDRCQUFJLE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUEsQ0FBRSxRQUFRLEVBQUUsR0FBRyxLQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFBLENBQUUsUUFBUSxFQUFFLEdBQUcsSUFBSSxDQUFDO0FBQ3ZILDRCQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7QUFDVCxnQ0FBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7QUFDWixrREFBa0IsRUFBRSxNQUFNO0FBQzFCLDBEQUEwQixFQUFFLE1BQU07NkJBQ3JDLENBQUMsQ0FBQztBQUNILGdDQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7eUJBQ3BFO3FCQUNKO2lCQUNKO0FBQ0QseUJBQVMsU0FBUyxDQUFDLEtBQUssRUFBRTtBQUN0Qix3QkFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7QUFDbEIsNkJBQVMsQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0FBQ3JDLDZCQUFTLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztpQkFDcEM7QUFDRCx5QkFBUyxTQUFTLENBQUMsS0FBSyxFQUFFO0FBQ3RCLHdCQUFJLE1BQU0sR0FBRyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztBQUMzRCwwQkFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUM7QUFDL0MsMEJBQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO0FBQy9DLHdCQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7QUFDakUsd0JBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO2lCQUNyQjtBQUNELHlCQUFTLE9BQU8sQ0FBQyxLQUFLLEVBQUU7QUFDcEIsNkJBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDO2lCQUN6QzthQUNKO0FBQ0QsZ0JBQUksRUFBRSxjQUFVLENBQUMsRUFBRTtBQUNmLEFBQUMsaUJBQUEsVUFBVSxRQUFRLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTtBQUMvQix3QkFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUM7QUFDdkIsNEJBQVEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQy9CLDRCQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQzVDLENBQUEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBRTthQUN6RDtTQUNKLENBQUM7S0FDTCxDQUFDO0FBQ0YsY0FBVSxDQUFDLE9BQU8sR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ25DLFdBQU8sVUFBVSxDQUFDO0NBQ3JCLENBQUEsRUFBRSxDQUFDOzs7OztBQ3ZFSixJQUFJLEdBQUcsR0FBRyxNQUFNLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxPQUFPLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxPQUFPLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDOzs7Ozs7O0lDQWxNLG1CQUFtQjtBQUNWLGFBRFQsbUJBQW1CLENBQ1QsTUFBTSxFQUFFLElBQUksRUFBRTs4QkFEeEIsbUJBQW1COztBQUVqQixjQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7S0FDL0I7O0FBSEMsdUJBQW1CLENBSWQsUUFBUSxHQUFBLG9CQUFHO0FBQ2QsZUFBTztBQUNILGdCQUFJLEVBQUUsQ0FBQSxZQUFZO0FBQ2Qsb0JBQUksVUFBVSxHQUFHLG9CQUFVLE1BQU0sRUFBRSxZQUFZLEVBQUUsR0FBRyxFQUFFO0FBQ2xELDJCQUFPLEdBQUcsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDLFFBQVEsQ0FBQztpQkFDMUQsQ0FBQztBQUNGLDBCQUFVLENBQUMsT0FBTyxHQUFHLENBQ2pCLFFBQVEsRUFDUixjQUFjLEVBQ2QsS0FBSyxDQUNSLENBQUM7QUFDRix1QkFBTyxVQUFVLENBQUM7YUFDckIsQ0FBQSxFQUFFO1NBQ04sQ0FBQztLQUNMOztXQWxCQyxtQkFBbUI7OztBQW9CekIsbUJBQW1CLENBQUMsT0FBTyxHQUFHLENBQzFCLFFBQVEsRUFDUixNQUFNLENBQ1QsQ0FBQztBQUNGLE1BQU0sQ0FBQyxPQUFPLEdBQUcsbUJBQW1CLENBQUM7Ozs7Ozs7QUN4QnJDLElBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQzs7SUFDNUIsYUFBYTtBQUNKLGFBRFQsYUFBYSxDQUNILE1BQU0sRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRTs4QkFEL0MsYUFBYTs7QUFFWCxZQUFJLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQztBQUNoQixZQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7QUFDbkIsWUFBSSxDQUFDLENBQUMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO0FBQzVCLFlBQUksYUFBYTtBQUNqQixtQkFBVyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7QUFDbEUsWUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLENBQUMsV0FBVyxDQUFDO0FBQzdDLFlBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxDQUFDLFdBQVcsQ0FBQztBQUM3QyxZQUFJLENBQUMsVUFBVSxHQUFHLGFBQWEsQ0FBQyxVQUFVLENBQUM7QUFDM0MsYUFBSyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO0FBQzNCLGdCQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQ2xDLFNBQVM7QUFDYixnQkFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1NBQ2hDO0FBQ0QsWUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO0FBQzNDLFlBQUksZUFBZSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQSxDQUFFLEVBQUUsQ0FBQztBQUNsRixhQUFLLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFO0FBQzdCLGdCQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUNwQyxTQUFTO0FBQ2IsZ0JBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsSUFBSSxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQzNHO0tBQ0o7O0FBdEJDLGlCQUFhLENBdUJSLFFBQVEsR0FBQSxvQkFBRztBQUNkLGVBQU87Ozs7QUFJSCxnQkFBSSxFQUFFLGNBQVUsTUFBTSxFQUFFLFlBQVksRUFBRSxHQUFHLEVBQUU7QUFDdkMsdUJBQU8sR0FBRyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDO2FBQ3hEO1NBQ0osQ0FBQztLQUNMOztXQWhDQyxhQUFhOzs7QUFrQ25CLGFBQWEsQ0FBQyxPQUFPLEdBQUcsQ0FDcEIsUUFBUSxFQUNSLE1BQU0sRUFDTixVQUFVLEVBQ1YsYUFBYSxDQUNoQixDQUFDO0FBQ0YsTUFBTSxDQUFDLE9BQU8sR0FBRyxhQUFhLENBQUM7Ozs7Ozs7SUN6Q3pCLGtCQUFrQjtBQUNULGFBRFQsa0JBQWtCLENBQ1IsTUFBTSxFQUFFOzhCQURsQixrQkFBa0I7O0FBRWhCLFlBQUksSUFBSSxHQUFHLElBQUksQ0FBQztBQUNoQixZQUFJLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQztBQUNoQixZQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxhQUFhO0FBQ3JCLG1CQUFPO1NBQUEsQUFDWCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQztBQUNwRCxZQUFJLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0FBQ25DLFlBQUksQ0FBQyxNQUFNLEdBQUc7QUFDVixrQkFBTSxFQUFFLEVBQUU7QUFDVixtQkFBTyxFQUFFLENBQUM7QUFDVixtQkFBTyxFQUFFLENBQUM7QUFDVixrQkFBTSxFQUFFLENBQUM7U0FDWixDQUFDO0FBQ0YsWUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7QUFDakIsWUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7QUFDeEIsWUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUNoRSxZQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQzNELFlBQUksQ0FBQyxLQUFLO0FBQ1YsVUFBRSxDQUFDO0tBQ047O0FBcEJDLHNCQUFrQixXQXFCcEIsS0FBSyxHQUFBLGlCQUFHO0FBQ0osWUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO0FBQ3hCLFlBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO0FBQ3hCLFlBQUksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssS0FBSyxXQUFXLEVBQUU7QUFDMUMsZ0JBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzdFO0FBQ0QsZUFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztLQUM1Qjs7QUE1QkMsc0JBQWtCLFdBNkJwQixJQUFJLEdBQUEsZ0JBQUc7QUFDSCxZQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFBLEFBQUMsRUFBRTtBQUM1RSxnQkFBSSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLFdBQVcsRUFBRTtBQUMxQyxvQkFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ2hCO0FBQ0QsZ0JBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0FBQ2xFLGdCQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ3hELGdCQUFJLENBQUMsVUFBVTtBQUNmLGdCQUFJLENBQUM7QUFDTCxpQkFBSyxJQUFJLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUU7QUFDbkQsb0JBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7QUFDdEIscUJBQUssSUFBSSxJQUFJLEdBQUcsQ0FBQyxFQUFFLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsSUFBSSxHQUFHLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBRTtBQUMxRSx3QkFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDckMsd0JBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRTtBQUNoQyw0QkFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztxQkFDckM7aUJBQ0o7YUFDSjtTQUNKO0tBQ0o7O0FBaERDLHNCQUFrQixXQWlEcEIsc0JBQXNCLEdBQUEsZ0NBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRTtBQUN6QixZQUFJLElBQUksR0FBRyxJQUFJLENBQUM7QUFDaEIsa0JBQVUsQ0FBQyxZQUFZO0FBQ25CLGdCQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUM3QixFQUFFLEVBQUUsQ0FBQyxDQUFDO0tBQ1Y7O0FBdERDLHNCQUFrQixXQXVEcEIsY0FBYyxHQUFBLHdCQUFDLENBQUMsRUFBRSxDQUFDLEVBQUU7QUFDakIsWUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUNoQyxZQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQzlCLFlBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtZQUFFLElBQUksR0FBRztBQUMxQyxrQkFBTSxFQUFFLEtBQUssQ0FBQyxNQUFNLEVBQUU7QUFDdEIsaUJBQUssRUFBRSxLQUFLLENBQUMsS0FBSyxFQUFFO0FBQ3BCLGtCQUFNLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7U0FDbkM7WUFBRSxFQUFFLEdBQUc7QUFDSixrQkFBTSxFQUFFLEdBQUcsQ0FBQyxNQUFNLEVBQUU7QUFDcEIsaUJBQUssRUFBRSxHQUFHLENBQUMsS0FBSyxFQUFFO0FBQ2xCLGtCQUFNLEVBQUUsR0FBRyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7U0FDakMsQ0FBQztBQUNOLFlBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSTtZQUFFLEdBQUcsQ0FBQztBQUN2RDs7Ozs7QUFLQSxZQUFJLEdBQUc7QUFDSCxnQkFBSSxFQUFFO0FBQ0YsaUJBQUMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTztBQUM1RSxpQkFBQyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU07YUFDaEQ7QUFDRCxjQUFFLEVBQUU7QUFDQSxpQkFBQyxFQUFFLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNO0FBQ3BELGlCQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUMsTUFBTTthQUM1QztTQUNKLENBQUM7QUFDRixZQUFJLE1BQU0sR0FBRyxDQUNULE1BQU0sRUFDTixNQUFNLEVBQ04sTUFBTSxFQUNOLE1BQU0sRUFDTixNQUFNLEVBQ04sTUFBTSxFQUNOLE1BQU0sQ0FDVCxDQUFDO0FBQ0YsWUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDO0FBQ2xGLG9CQUFVLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO0FBQ3JELDBCQUFjLEVBQUUsQ0FBQztTQUNwQixDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7S0FDZjs7QUFoR0Msc0JBQWtCLFdBaUdwQixhQUFhLEdBQUEsdUJBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUU7QUFDMUMsWUFBSSxPQUFPLFNBQVMsS0FBSyxXQUFXLEVBQUU7QUFDbEMscUJBQVMsR0FBRyxDQUFDLENBQUM7U0FDakI7QUFDRCxZQUFJLE9BQU8sR0FBRyxLQUFLLFdBQVcsRUFBRTtBQUM1QixlQUFHLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztTQUNqQjtBQUNELFlBQUksU0FBUyxHQUFHLEVBQUU7WUFBRSxRQUFRLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDaEQsWUFBSSxFQUFFLEdBQUcsRUFBRSxFQUFFO0FBQ1QsZ0JBQUksRUFBRSxHQUFHLEVBQUU7Z0JBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQztBQUNyQixjQUFFLEdBQUcsRUFBRSxDQUFDO0FBQ1IsY0FBRSxHQUFHLEVBQUUsQ0FBQztBQUNSLGNBQUUsR0FBRyxFQUFFLENBQUM7QUFDUixjQUFFLEdBQUcsRUFBRSxDQUFDO1NBQ1g7QUFDRCxZQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztBQUMxQyxZQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7QUFDckQsWUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRTtZQUFFLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7QUFDaEQsWUFBSSxTQUFTLEVBQUU7QUFDWCxnQkFBSSxDQUFDLEdBQUcsRUFBRTtnQkFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDO0FBQ25CLGdCQUFJLFFBQVEsR0FBRyxDQUFDLEVBQUU7QUFDZCxpQkFBQyxHQUFHLEVBQUUsQ0FBQztBQUNQLGlCQUFDLEdBQUcsRUFBRSxDQUFDO2FBQ1Y7QUFDRCxxQkFBUyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsU0FBUyxDQUFBLEFBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQSxBQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsU0FBUyxDQUFBLEFBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQSxBQUFDLENBQUM7U0FDakk7QUFDRCxlQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRSxHQUFHLEdBQUcsR0FBRyxFQUFFLEdBQUcsR0FBRyxHQUFHLEVBQUUsR0FBRyxHQUFHLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQSxBQUFDLEdBQUcsR0FBRyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUEsQUFBQyxHQUFHLEdBQUcsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFBLEFBQUMsR0FBRyxHQUFHLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQSxBQUFDLEdBQUcsR0FBRyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUEsQUFBQyxHQUFHLEdBQUcsR0FBRyxFQUFFLEdBQUcsR0FBRyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUEsQUFBQyxHQUFHLEdBQUcsR0FBRyxFQUFFLEdBQUcsR0FBRyxHQUFHLEVBQUUsR0FBRyxTQUFTLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztLQUM3Tjs7Ozs7O0FBNUhDLHNCQUFrQixXQWdJcEIsaUJBQWlCLEdBQUEsMkJBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRTtBQUM1QixZQUFJLElBQUksR0FBRyxJQUFJO1lBQUUsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ3BELFlBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtBQUNaLGdCQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDbkM7QUFDRCxZQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQztBQUMxQixnQkFBUSxDQUFDLEVBQUUsQ0FBQyxVQUFVLEVBQUUsWUFBWTtBQUNoQyxtQkFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ3RCLGdCQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDZixDQUFDLENBQUM7QUFDSCxZQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7S0FDZjs7Ozs7O0FBM0lDLHNCQUFrQixXQStJcEIsU0FBUyxHQUFBLG1CQUFDLFFBQVEsRUFBRTtBQUNoQixZQUFJLElBQUksR0FBRyxJQUFJLENBQUM7QUFDaEIsWUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDO0FBQy9CLGdCQUFRLENBQUMsRUFBRSxDQUFDLFVBQVUsRUFBRSxZQUFZO0FBQ2hDLG1CQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO0FBQzNCLG1CQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1NBQzVCLENBQUMsQ0FBQztBQUNILFlBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztLQUNmOztXQXZKQyxrQkFBa0I7OztBQXlKeEIsa0JBQWtCLENBQUMsT0FBTyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDeEMsTUFBTSxDQUFDLE9BQU8sR0FBRyxZQUFZO0FBQ3pCLFdBQU87QUFDSCxrQkFBVSxFQUFFLGtCQUFrQjtBQUM5QixZQUFJLEVBQUUsY0FBVSxDQUFDLEVBQUU7QUFDZixBQUFDLGFBQUEsVUFBVSxRQUFRLEVBQUUsTUFBTSxFQUFFO0FBQ3pCLG9CQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFDbkIsT0FBTzthQUNkLENBQUEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBRTtTQUN6RDtLQUNKLENBQUM7Q0FDTCxDQUFDOzs7OztBQ3BLRixNQUFNLENBQUMsT0FBTyxHQUFHLFlBQVk7QUFDekIsV0FBTztBQUNILGVBQU8sRUFBRSxpQkFBaUI7QUFDMUIsWUFBSSxFQUFFLGNBQVUsQ0FBQyxFQUFFO0FBQ2YsQUFBQyxhQUFBLFVBQVUsUUFBUSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7QUFDL0Isb0JBQUksQ0FBQyxJQUFJLEVBQ0wsT0FBTztBQUNYLG9CQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLGtCQUFrQixFQUFFLFFBQVEsQ0FBQyxDQUFDO2FBQy9ELENBQUEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBRTtTQUN6RDtLQUNKLENBQUM7Q0FDTCxDQUFDOzs7OztBQ1hGLE1BQU0sQ0FBQyxPQUFPLEdBQUcsWUFBWTtBQUN6QixXQUFPO0FBQ0gsZUFBTyxFQUFFLGlCQUFpQjtBQUMxQixZQUFJLEVBQUUsY0FBVSxDQUFDLEVBQUU7QUFDZixBQUFDLGFBQUEsVUFBVSxRQUFRLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTtBQUMvQixvQkFBSSxDQUFDLElBQUksRUFDTCxPQUFPO0FBQ1gsd0JBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7QUFDNUMsb0JBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDNUIsQ0FBQSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFFO1NBQ3pEO0tBQ0osQ0FBQztDQUNMLENBQUM7Ozs7O0FDWkYsT0FBTyxDQUFDLHVCQUF1QixDQUFDLENBQUM7QUFDakMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUNoQyxXQUFXLEVBQ1gsVUFBVSxDQUNiLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLGdCQUFnQixFQUFFLE9BQU8sQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLG9CQUFvQixFQUFFLE9BQU8sQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLG1CQUFtQixFQUFFLE9BQU8sQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUM7Ozs7O0FDSmxRLElBQUksbUJBQW1CLEdBQUcsT0FBTyxDQUFDLDBCQUEwQixDQUFDLENBQUM7QUFDOUQsSUFBSSxhQUFhLEdBQUcsT0FBTyxDQUFDLG9CQUFvQixDQUFDLENBQUM7QUFDbEQsTUFBTSxDQUFDLE9BQU8sR0FBRyxDQUFBLFlBQVk7QUFDekIsUUFBSSxVQUFVLEdBQUcsb0JBQVUsY0FBYyxFQUFFO0FBQ3ZDLHNCQUFjLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRTtBQUNoQyxlQUFHLEVBQUUsY0FBYztBQUNuQixpQkFBSyxFQUFFO0FBQ0gsdUJBQU8sRUFBRTtBQUNMLDhCQUFVLEVBQUUsbUJBQW1CO0FBQy9CLDJCQUFPLEVBQUUsbUJBQW1CLENBQUMsUUFBUSxFQUFFO0FBQ3ZDLCtCQUFXLEVBQUUsb0NBQW9DO2lCQUNwRDthQUNKO1NBQ0osQ0FBQyxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsRUFBRTtBQUN6QixlQUFHLEVBQUUsV0FBVztBQUNoQixpQkFBSyxFQUFFO0FBQ0gsdUJBQU8sRUFBRTtBQUNMLDhCQUFVLEVBQUUsYUFBYTtBQUN6QiwyQkFBTyxFQUFFLGFBQWEsQ0FBQyxRQUFRLEVBQUU7QUFDakMsK0JBQVcsRUFBRSxrQ0FBa0M7aUJBQ2xEO2FBQ0o7U0FDSixDQUFDLENBQUM7S0FDTixDQUFDO0FBQ0YsY0FBVSxDQUFDLE9BQU8sR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7QUFDeEMsV0FBTyxVQUFVLENBQUM7Q0FDckIsQ0FBQSxFQUFFLENBQUM7Ozs7Ozs7QUMxQkosSUFBSSxHQUFHLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDOztJQUM1QixZQUFZO0FBQ0gsYUFEVCxZQUFZLENBQ0YsTUFBTSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFOzhCQURoRCxZQUFZOztBQUVWLFlBQUksSUFBSSxHQUFHLElBQUksQ0FBQztBQUNoQixZQUFJLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQztBQUNoQixZQUFJLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUM7QUFDN0IsWUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0FBQ25CLFlBQUksQ0FBQyxDQUFDLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztBQUMzQixZQUFJLGFBQWEsR0FBRyxXQUFXLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztBQUM5SCxZQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztBQUNqQixZQUFJLENBQUMsZ0JBQWdCLEdBQUcsYUFBYSxDQUFDLFdBQVcsQ0FBQztBQUNsRCxZQUFJLENBQUMsV0FBVyxHQUFHO0FBQ2YsdUJBQVcsRUFBRSxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUs7QUFDckQsd0JBQVksRUFBRSxFQUFFO0FBQ2hCLGdCQUFJLEVBQUUsRUFBRTtBQUNSLG9CQUFRLEVBQUUsRUFBRTtTQUNmLENBQUM7QUFDRixhQUFLLElBQUksRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUU7QUFDL0QsZ0JBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ2xDLGdCQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDNUQ7QUFDRCxhQUFLLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtBQUNqQyxnQkFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksR0FBRyxFQUFFO0FBQzNFLG9CQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO2FBQ25FO0FBQ0QsZ0JBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxHQUFHLEVBQUU7QUFDdEMsb0JBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUEsQUFBQyxFQUFFO0FBQ3hFLHdCQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3hELE1BQU07QUFDSCx3QkFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztpQkFDbkU7YUFDSjtTQUNKO0FBQ0QsWUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0tBQ3JCOztBQWpDQyxnQkFBWSxXQWtDZCxhQUFhLEdBQUEsdUJBQUMsUUFBUSxFQUFFO0FBQ3BCLFlBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDOUQsWUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtBQUN0RCxpQkFBSyxDQUFDLHdCQUF1SCxDQUFDLENBQUM7QUFDL0gsbUJBQU87U0FDVjtBQUNELFlBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUM7QUFDbEQsWUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0FBQzNDLFlBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztLQUNyQjs7QUEzQ0MsZ0JBQVksV0E0Q2QsZ0JBQWdCLEdBQUEsMEJBQUMsUUFBUSxFQUFFO0FBQ3ZCLFlBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDbEUsWUFBSSxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtBQUN2QixnQkFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQztBQUNsRCxnQkFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN6RDtBQUNELFlBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztBQUN2QyxZQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7S0FDckI7O0FBcERDLGdCQUFZLFdBcURkLFVBQVUsR0FBQSxzQkFBRztBQUNULFlBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztBQUMzQixhQUFLLElBQUksRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUU7QUFDbkUsZ0JBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ3RDLGdCQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3BDO0tBQ0o7O0FBM0RDLGdCQUFZLFdBNERkLE1BQU0sR0FBQSxrQkFBRztBQUNMLFlBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1lBQUUsRUFBRSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDcEUsVUFBRSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUN2QixZQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZO0FBQzFCLGNBQUUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDckIsaUJBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztTQUNqRCxDQUFDLENBQUM7S0FDTjs7QUFuRUMsZ0JBQVksV0FvRWQsTUFBTSxHQUFBLGtCQUFHO0FBQ0wsWUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFBRSxFQUFFLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUNwRSxVQUFFLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO0FBQ3ZCLFlBQUksQ0FBQyxJQUFJLFFBQVcsQ0FBQyxZQUFZO0FBQzdCLGNBQUUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDckIsaUJBQUssQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1NBQy9DLENBQUMsQ0FBQztLQUNOOztBQTNFQyxnQkFBWSxDQTRFUCxRQUFRLEdBQUEsb0JBQUc7QUFDZCxlQUFPOzs7O0FBSUgsZ0JBQUksRUFBRSxjQUFVLE1BQU0sRUFBRSxZQUFZLEVBQUUsR0FBRyxFQUFFO0FBQ3ZDLHVCQUFPLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDO2FBQzlEO1NBQ0osQ0FBQztLQUNMOztXQXJGQyxZQUFZOzs7QUF1RmxCLFlBQVksQ0FBQyxPQUFPLEdBQUcsQ0FDbkIsUUFBUSxFQUNSLE1BQU0sRUFDTixhQUFhLEVBQ2IsV0FBVyxDQUNkLENBQUM7QUFDRixNQUFNLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQzs7Ozs7OztJQzlGeEIsZ0JBQWdCO0FBQ1AsYUFEVCxnQkFBZ0IsQ0FDTixNQUFNLEVBQUUsSUFBSSxFQUFFOzhCQUR4QixnQkFBZ0I7O0FBRWQsY0FBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO0tBQy9COztBQUhDLG9CQUFnQixDQUlYLFFBQVEsR0FBQSxvQkFBRztBQUNkLGVBQU87QUFDSCxnQkFBSSxFQUFFLENBQUEsWUFBWTtBQUNkLG9CQUFJLFVBQVUsR0FBRyxvQkFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLEdBQUcsRUFBRTtBQUNsRCwyQkFBTyxHQUFHLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDLFFBQVEsQ0FBQztpQkFDaEUsQ0FBQztBQUNGLDBCQUFVLENBQUMsT0FBTyxHQUFHLENBQ2pCLFFBQVEsRUFDUixjQUFjLEVBQ2QsS0FBSyxDQUNSLENBQUM7QUFDRix1QkFBTyxVQUFVLENBQUM7YUFDckIsQ0FBQSxFQUFFO1NBQ04sQ0FBQztLQUNMOztXQWxCQyxnQkFBZ0I7OztBQW9CdEIsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLENBQ3ZCLFFBQVEsRUFDUixNQUFNLENBQ1QsQ0FBQztBQUNGLE1BQU0sQ0FBQyxPQUFPLEdBQUcsZ0JBQWdCLENBQUM7Ozs7Ozs7QUN4QmxDLElBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQzs7SUFDNUIsZUFBZTtBQUNOLGFBRFQsZUFBZSxDQUNMLE1BQU0sRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRTs4QkFEaEQsZUFBZTs7QUFFYixZQUFJLElBQUksR0FBRyxJQUFJLENBQUM7QUFDaEIsWUFBSSxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUM7QUFDaEIsWUFBSSxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDO0FBQzdCLFlBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztBQUNuQixZQUFJLENBQUMsQ0FBQyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7QUFDM0IsWUFBSSxhQUFhLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7QUFDOUgsWUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7QUFDakIsWUFBSSxDQUFDLGdCQUFnQixHQUFHLGFBQWEsQ0FBQyxXQUFXLENBQUM7QUFDbEQsWUFBSSxDQUFDLFdBQVcsR0FBRztBQUNmLHVCQUFXLEVBQUUsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLO0FBQ3JELHdCQUFZLEVBQUUsRUFBRTtBQUNoQixnQkFBSSxFQUFFLEVBQUU7QUFDUixvQkFBUSxFQUFFLEVBQUU7U0FDZixDQUFDO0FBQ0YsYUFBSyxJQUFJLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFO0FBQy9ELGdCQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUNsQyxnQkFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzVEO0FBQ0QsYUFBSyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7QUFDakMsZ0JBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEdBQUcsRUFBRTtBQUMzRSxvQkFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQzthQUNuRTtBQUNELGdCQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksR0FBRyxFQUFFO0FBQ3RDLG9CQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUEsQUFBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtBQUMxRyx3QkFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUN4RCxNQUFNLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtBQUMxQyx3QkFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ3pELHdCQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO2lCQUNuRSxNQUFNO0FBQ0gsd0JBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7aUJBQ25FO2FBQ0o7U0FDSjtBQUNELFlBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztLQUNyQjs7QUFwQ0MsbUJBQWUsV0FxQ2pCLGFBQWEsR0FBQSx1QkFBQyxRQUFRLEVBQUU7QUFDcEIsWUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUM5RCxZQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO0FBQ3RELGlCQUFLLENBQUMsMEJBQW1JLENBQUMsQ0FBQztBQUMzSSxtQkFBTztTQUNWO0FBQ0QsWUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQztBQUNsRCxZQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDM0MsWUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0tBQ3JCOztBQTlDQyxtQkFBZSxXQStDakIsZ0JBQWdCLEdBQUEsMEJBQUMsUUFBUSxFQUFFO0FBQ3ZCLFlBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDbEUsWUFBSSxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtBQUN2QixnQkFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQztBQUNsRCxnQkFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN6RDtBQUNELFlBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztBQUN2QyxZQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7S0FDckI7O0FBdkRDLG1CQUFlLFdBd0RqQixVQUFVLEdBQUEsc0JBQUc7QUFDVCxZQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7QUFDM0IsYUFBSyxJQUFJLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFO0FBQ25FLGdCQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUN0QyxnQkFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNwQztLQUNKOztBQTlEQyxtQkFBZSxXQStEakIsTUFBTSxHQUFBLGtCQUFHO0FBQ0wsWUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFBRSxFQUFFLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUNwRSxVQUFFLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO0FBQ3ZCLFlBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQzlCLGNBQUUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDckIsaUJBQUssQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7U0FDakUsQ0FBQyxDQUFDO0tBQ047O0FBdEVDLG1CQUFlLENBdUVWLFFBQVEsR0FBQSxvQkFBRztBQUNkLGVBQU87Ozs7QUFJSCwwQkFBYyxFQUFFLHdCQUFVLE1BQU0sRUFBRSxZQUFZLEVBQUUsR0FBRyxFQUFFO0FBQ2pELHVCQUFPLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLFFBQVEsQ0FBQzthQUN4RDtBQUNELGdCQUFJLEVBQUUsY0FBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLEdBQUcsRUFBRSxjQUFjLEVBQUUsVUFBVSxFQUFFO0FBQ25FLHVCQUFPLElBQUksR0FBRyxDQUFDLG9CQUFvQixDQUFDO0FBQ2hDLHdCQUFJLEVBQUUsRUFBRTtBQUNSLCtCQUFXLEVBQUUsRUFBRTtBQUNmLCtCQUFXLEVBQUUsRUFBRTtBQUNmLGtDQUFjLEVBQUUsY0FBYyxDQUFDLEVBQUU7QUFDakMsdUNBQW1CLEVBQUUsY0FBYztBQUNuQyx3QkFBSSxFQUFFLFVBQVUsQ0FBQyxXQUFXLENBQUMsRUFBRTtpQkFDbEMsQ0FBQyxDQUFDO2FBQ047U0FDSixDQUFDO0tBQ0w7O1dBMUZDLGVBQWU7OztBQTRGckIsZUFBZSxDQUFDLE9BQU8sR0FBRyxDQUN0QixRQUFRLEVBQ1IsTUFBTSxFQUNOLGFBQWEsRUFDYixXQUFXLENBQ2QsQ0FBQztBQUNGLE1BQU0sQ0FBQyxPQUFPLEdBQUcsZUFBZSxDQUFDOzs7Ozs7O0FDbkdqQyxJQUFJLEdBQUcsR0FBRyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7O0lBQzVCLGdCQUFnQjtBQUNQLGFBRFQsZ0JBQWdCLENBQ04sTUFBTSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFOzhCQUQvQyxnQkFBZ0I7O0FBRWQsWUFBSSxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUM7QUFDaEIsWUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0FBQ25CLFlBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztBQUN2QyxZQUFJLENBQUMsQ0FBQyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7QUFDNUIsWUFBSSxhQUFhO0FBQ2pCLG1CQUFXLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7QUFDaEYsWUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLENBQUMsV0FBVyxDQUFDO0FBQzdDLFlBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxDQUFDLFdBQVcsQ0FBQztBQUM3QyxhQUFLLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7QUFDNUIsZ0JBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksR0FBRyxFQUFFO0FBQ2pDLG9CQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7YUFDeEM7U0FDSjtBQUNELGFBQUssSUFBSSxFQUFFLEdBQUcsQ0FBQyxFQUFFLEVBQUUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFO0FBQzFELGdCQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQzdCLGdCQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7U0FDdkM7QUFDRCxZQUFJLGVBQWUsR0FBRyxjQUFjLENBQUM7QUFDckMsWUFBSSxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDO0FBQzNDLFlBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLEdBQUc7QUFDL0IsY0FBRSxFQUFFLGVBQWU7QUFDbkIsZ0JBQUksRUFBRSxnQkFBaUY7U0FDMUYsQ0FBQztBQUNGLGFBQUssSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtBQUMzQixnQkFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUNsQyxTQUFTO0FBQ2IsZ0JBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztTQUNoQztBQUNELFlBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLGFBQWEsRUFBRSxHQUFHLENBQUMsQ0FBQztBQUMzQyxhQUFLLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFO0FBQzdCLGdCQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUNwQyxTQUFTO0FBQ2IsZ0JBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsSUFBSSxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQzNHO0tBQ0o7O0FBcENDLG9CQUFnQixDQXFDWCxRQUFRLEdBQUEsb0JBQUc7QUFDZCxlQUFPOzs7O0FBSUgsZ0JBQUksRUFBRSxjQUFVLE1BQU0sRUFBRSxZQUFZLEVBQUUsR0FBRyxFQUFFO0FBQ3ZDLHVCQUFPLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDO2FBQzlEO1NBQ0osQ0FBQztLQUNMOztXQTlDQyxnQkFBZ0I7OztBQWdEdEIsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLENBQ3ZCLFFBQVEsRUFDUixNQUFNLEVBQ04sVUFBVSxFQUNWLGFBQWEsQ0FDaEIsQ0FBQztBQUNGLE1BQU0sQ0FBQyxPQUFPLEdBQUcsZ0JBQWdCLENBQUM7Ozs7Ozs7SUN2RDVCLFVBQVUsR0FDRCxTQURULFVBQVUsQ0FDQSxNQUFNLEVBQUU7MEJBRGxCLFVBQVU7Q0FFWDs7QUFFTCxVQUFVLENBQUMsT0FBTyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDaEMsTUFBTSxDQUFDLE9BQU8sR0FBRyxZQUFZO0FBQ3pCLFdBQU87QUFDSCxrQkFBVSxFQUFFLFVBQVU7QUFDdEIsWUFBSSxFQUFFLGNBQVUsQ0FBQyxFQUFFO0FBQ2YsQUFBQyxhQUFBLFVBQVUsUUFBUSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7QUFDL0Isb0JBQUksQ0FBQyxJQUFJLEVBQ0wsT0FBTzthQUNkLENBQUEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBRTtTQUN6RDtLQUNKLENBQUM7Q0FDTCxDQUFDOzs7OztBQ2ZGLE9BQU8sQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7Ozs7O0FDQTVJLElBQUksZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLHVCQUF1QixDQUFDLENBQUM7QUFDeEQsSUFBSSxZQUFZLEdBQUcsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUM7QUFDaEQsSUFBSSxlQUFlLEdBQUcsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7QUFDdEQsSUFBSSxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQztBQUN4RCxNQUFNLENBQUMsT0FBTyxHQUFHLENBQUEsWUFBWTtBQUN6QixRQUFJLFVBQVUsR0FBRyxvQkFBVSxjQUFjLEVBQUU7QUFDdkMsc0JBQWMsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFO0FBQzlCLGVBQUcsRUFBRSxZQUFZO0FBQ2pCLGlCQUFLLEVBQUU7QUFDSCx1QkFBTyxFQUFFO0FBQ0wsOEJBQVUsRUFBRSxnQkFBZ0I7QUFDNUIsMkJBQU8sRUFBRSxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7QUFDcEMsK0JBQVcsRUFBRSxrQ0FBa0M7aUJBQ2xEO2FBQ0o7U0FDSixDQUFDLENBQUMsS0FBSyxDQUFDLG9CQUFvQixFQUFFO0FBQzNCLGVBQUcsRUFBRSxlQUFlO0FBQ3BCLGlCQUFLLEVBQUU7QUFDSCx1QkFBTyxFQUFFO0FBQ0wsOEJBQVUsRUFBRSxlQUFlO0FBQzNCLDJCQUFPLEVBQUUsZUFBZSxDQUFDLFFBQVEsRUFBRTtBQUNuQywrQkFBVyxFQUFFLDJDQUEyQztpQkFDM0Q7YUFDSjtTQUNKLENBQUMsQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEVBQUU7QUFDdkIsZUFBRyxFQUFFLFdBQVc7QUFDaEIsaUJBQUssRUFBRTtBQUNILHVCQUFPLEVBQUU7QUFDTCw4QkFBVSxFQUFFLFlBQVk7QUFDeEIsMkJBQU8sRUFBRSxZQUFZLENBQUMsUUFBUSxFQUFFO0FBQ2hDLCtCQUFXLEVBQUUsdUNBQXVDO2lCQUN2RDthQUNKO1NBQ0osQ0FBQyxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsRUFBRTtBQUM1QixlQUFHLEVBQUUsZ0JBQWdCO0FBQ3JCLGlCQUFLLEVBQUU7QUFDSCx1QkFBTyxFQUFFO0FBQ0wsOEJBQVUsRUFBRSxnQkFBZ0I7QUFDNUIsMkJBQU8sRUFBRSxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUU7QUFDcEMsK0JBQVcsRUFBRSw0Q0FBNEM7aUJBQzVEO2FBQ0o7U0FDSixDQUFDLENBQUM7S0FDTixDQUFDO0FBQ0YsY0FBVSxDQUFDLE9BQU8sR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7QUFDeEMsV0FBTyxVQUFVLENBQUM7Q0FDckIsQ0FBQSxFQUFFLENBQUM7Ozs7O0FDOUNKLE1BQU0sQ0FBQyxPQUFPLEdBQUcsQ0FBQSxZQUFZO0FBQ3pCLFFBQUksVUFBVSxHQUFHLG9CQUFVLFVBQVUsRUFBRTtBQUNuQyxlQUFPO0FBQ0gsZ0JBQUksRUFBRSxjQUFVLENBQUMsRUFBRTtBQUNmLEFBQUMsaUJBQUEsVUFBVSxRQUFRLEVBQUUsTUFBTSxFQUFFO0FBQ3pCLDhCQUFVLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLEtBQUssQ0FBQyxDQUFDO0FBQzNDLDhCQUFVLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQztBQUNwQyw4QkFBVSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxHQUFHLENBQUMsQ0FBQztBQUMzQyw4QkFBVSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxHQUFHLENBQUMsQ0FBQztBQUN6Qyw4QkFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7QUFDbEMsNkJBQVMsS0FBSyxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUU7QUFDM0IsZ0NBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUM7cUJBQy9DO0FBQ0QsNkJBQVMsR0FBRyxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUU7QUFDekIsZ0NBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUM7cUJBQ2xEO2lCQUNKLENBQUEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBRTthQUN6RDtTQUNKLENBQUM7S0FDTCxDQUFDO0FBQ0YsY0FBVSxDQUFDLE9BQU8sR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO0FBQ3BDLFdBQU8sVUFBVSxDQUFDO0NBQ3JCLENBQUEsRUFBRSxDQUFDIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsInJlcXVpcmUoJy4vYXV0aC9tb2R1bGUnKTtcbnJlcXVpcmUoJy4vcGFnZXMvc3BlY2lhbHRpZXMvbW9kdWxlJyk7XG5yZXF1aXJlKCcuL3BhZ2VzL3VzZXItbGlzdC9tb2R1bGUnKTtcbnZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgnYXBwbGljYXRpb24nLCBbXG4gICAgJ2F1dGgnLFxuICAgICdwYWdlcy5zcGVjaWFsdGllcycsXG4gICAgJ3BhZ2VzLnVzZXItbGlzdCcsXG4gICAgJ25nU2FuaXRpemUnLFxuICAgICduZ1Jlc291cmNlJyxcbiAgICAnbmdDb29raWVzJ1xuXSkuY29uc3RhbnQoJ3BhdGhSZXNvdXJjZScsICcvYXBpL3YxLycpLmRpcmVjdGl2ZSgnaGlzdG9yeUJhY2snLCByZXF1aXJlKCcuL2hpc3RvcnlCYWNrLmpzJykpLmRpcmVjdGl2ZSgnc3RhdGVQZW5kaW5nQ2xhc3MnLCByZXF1aXJlKCcuL3N0YXRlUGVuZGluZ0NsYXNzLmpzJykpLmZhY3RvcnkoJ0RhdGEnLCByZXF1aXJlKCcuL0RhdGEuanMnKSkuZmFjdG9yeSgnRGF0YVNvcnQnLCByZXF1aXJlKCcuL0RhdGFTb3J0LmpzJykpLmZhY3RvcnkoJ0RhdGFDb252ZXJ0JywgcmVxdWlyZSgnLi9EYXRhQ29udmVydC5qcycpKS5mYWN0b3J5KCdIVFRQRXJyb3JIYW5kbGVyJywgcmVxdWlyZSgnLi9IVFRQRXJyb3JIYW5kbGVyLmpzJykpLnNlcnZpY2UoJ0FwaScsIHJlcXVpcmUoJy4vQXBpLmpzJykpLmZpbHRlcignZGVmYXVsdCcsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGlucHV0LCBhcmcpIHtcbiAgICAgICAgcmV0dXJuICEhaW5wdXQudHJpbSgpID8gaW5wdXQgOiBhcmc7XG4gICAgfTtcbn0pLmNvbmZpZyhmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGRpRnVuY3Rpb24gPSBmdW5jdGlvbiAoJHVybFJvdXRlclByb3ZpZGVyLCAkaHR0cFByb3ZpZGVyKSB7XG4gICAgICAgICR1cmxSb3V0ZXJQcm92aWRlci5vdGhlcndpc2UoJy9zcGVjaWFsdGllcycpO1xuICAgICAgICAkaHR0cFByb3ZpZGVyLmludGVyY2VwdG9ycy5wdXNoKCdIVFRQRXJyb3JIYW5kbGVyJyk7XG4gICAgfTtcbiAgICBkaUZ1bmN0aW9uLiRpbmplY3QgPSBbXG4gICAgICAgICckdXJsUm91dGVyUHJvdmlkZXInLFxuICAgICAgICAnJGh0dHBQcm92aWRlcidcbiAgICBdO1xuICAgIHJldHVybiBkaUZ1bmN0aW9uO1xufSgpKTsiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgZGlGdW5jdGlvbiA9IGZ1bmN0aW9uIEFwaSgkcmVzb3VyY2UsICRjb29raWVzLCBwYXRoUmVzb3VyY2UpIHtcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgICB2YXIgYXBpUmVzb3VyY2UgPSBmdW5jdGlvbiAodXJsLCBwYXJhbURlZmF1bHRzLCBhY3Rpb25zLCBvcHRpb25zKSB7XG4gICAgICAgICAgICB2YXIgbmcgPSBhbmd1bGFyLCBtZXRob2RzID0ge1xuICAgICAgICAgICAgICAgICAgICBxdWVyeToge1xuICAgICAgICAgICAgICAgICAgICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnWC1DU1JGVG9rZW4nOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAkY29va2llc1snY3NyZnRva2VuJ107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBhbGw6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHsgcGFnZV9zaXplOiA5OTk5OTk5IH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ1gtQ1NSRlRva2VuJzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJGNvb2tpZXNbJ2NzcmZ0b2tlbiddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgJ2RlbGV0ZSc6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGhvZDogJ0RFTEVURScsXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ1gtQ1NSRlRva2VuJzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJGNvb2tpZXNbJ2NzcmZ0b2tlbiddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgY3JlYXRlOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnWC1DU1JGVG9rZW4nOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAkY29va2llc1snY3NyZnRva2VuJ107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB1cGRhdGU6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BVVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ1gtQ1NSRlRva2VuJzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJGNvb2tpZXNbJ2NzcmZ0b2tlbiddO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgcGF0Y2g6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BBVENIJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnWC1DU1JGVG9rZW4nOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAkY29va2llc1snY3NyZnRva2VuJ107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIHJldHVybiAkcmVzb3VyY2UocGF0aFJlc291cmNlICsgdXJsLCBwYXJhbURlZmF1bHRzIHx8IHt9LCBuZy5leHRlbmQobmcuY29weShtZXRob2RzKSwgYWN0aW9ucyB8fCB7fSksIG9wdGlvbnMpO1xuICAgICAgICB9O1xuICAgICAgICB2YXIgYXBpUmVzb3VyY2VEZWZhdWx0ID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgICAgIHJldHVybiBhcGlSZXNvdXJjZShuYW1lICsgJy86aWQvOmNvbnRyb2xsZXIvPycsIHtcbiAgICAgICAgICAgICAgICBpZDogJ0BpZCcsXG4gICAgICAgICAgICAgICAgY29udHJvbGxlcjogJ0Bjb250cm9sbGVyJ1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVzb3VyY2UgPSBhcGlSZXNvdXJjZTtcbiAgICAgICAgdGhpcy5yZXNvdXJjZURlZmF1bHQgPSBhcGlSZXNvdXJjZURlZmF1bHQ7XG4gICAgICAgIHRoaXMuc3BlY2lhbGl6YXRpb24gPSB0aGlzLnJlc291cmNlRGVmYXVsdCgnc3BlY2lhbGl6YXRpb24nKTtcbiAgICAgICAgdGhpcy5kaXNjaXBsaW5lID0gdGhpcy5yZXNvdXJjZURlZmF1bHQoJ2Rpc2NpcGxpbmUnKTtcbiAgICAgICAgdGhpcy51c2VyX2Rpc2NpcGxpbmVfbGlzdCA9IHRoaXMucmVzb3VyY2VEZWZhdWx0KCd1c2VyX2Rpc2NpcGxpbmVfbGlzdCcpO1xuICAgIH07XG4gICAgZGlGdW5jdGlvbi4kaW5qZWN0ID0gW1xuICAgICAgICAnJHJlc291cmNlJyxcbiAgICAgICAgJyRjb29raWVzJyxcbiAgICAgICAgJ3BhdGhSZXNvdXJjZSdcbiAgICBdO1xuICAgIHJldHVybiBkaUZ1bmN0aW9uO1xufSgpOyIsInZhciBjZmcgPSByZXF1aXJlKCcuL2NvbmZpZycpO1xuY2xhc3MgU3BlY2lhbHR5IHtcbiAgICBjb25zdHJ1Y3RvcihpZCwgbmFtZSkge1xuICAgICAgICB0aGlzLmlkID0gaWQ7XG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSAnJyArICc8cD5cXHUwNDEyXFx1MDQzOFxcdTA0MzdcXHUwNDQzXFx1MDQzMFxcdTA0M0JcXHUwNDM4XFx1MDQzN1xcdTA0MzBcXHUwNDQ2XFx1MDQzOFxcdTA0NEYgXFx1MDQzM1xcdTA0NDBcXHUwNDMwXFx1MDQ0NFxcdTA0MzAgXFx1MDQzMlxcdTA0M0RcXHUwNDQzXFx1MDQ0MlxcdTA0NDBcXHUwNDM1XFx1MDQzRFxcdTA0M0RcXHUwNDM4XFx1MDQ0NSBcXHUwNDM4IFxcdTA0MzJcXHUwNDNEXFx1MDQzNVxcdTA0NDhcXHUwNDNEXFx1MDQzOFxcdTA0NDUgXFx1MDQ0MVxcdTA0MzJcXHUwNDRGXFx1MDQzN1xcdTA0MzVcXHUwNDM5IFxcdTA0MzRcXHUwNDM4XFx1MDQ0MVxcdTA0NDZcXHUwNDM4XFx1MDQzRlxcdTA0M0JcXHUwNDM4XFx1MDQzRCBcXHUwNDMyIFxcdTA0NDBcXHUwNDMwXFx1MDQzQ1xcdTA0M0FcXHUwNDMwXFx1MDQ0NSBcXHUwNDQzXFx1MDQ0N1xcdTA0MzVcXHUwNDMxXFx1MDQzRFxcdTA0M0VcXHUwNDMzXFx1MDQzRSBcXHUwNDNGXFx1MDQzQlxcdTA0MzBcXHUwNDNEXFx1MDQzMC48L3A+JztcbiAgICB9XG59XG5jbGFzcyBEaXNjaXBsaW5lIHtcbiAgICBjb25zdHJ1Y3RvcihpZCwgbmFtZSkge1xuICAgICAgICB0aGlzLmlkID0gaWQ7XG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSAnJyArICc8cD5cXHUwNDEyXFx1MDQzOFxcdTA0MzdcXHUwNDQzXFx1MDQzMFxcdTA0M0JcXHUwNDM4XFx1MDQzN1xcdTA0MzBcXHUwNDQ2XFx1MDQzOFxcdTA0NEYgXFx1MDQzM1xcdTA0NDBcXHUwNDMwXFx1MDQ0NFxcdTA0MzAgXFx1MDQzMlxcdTA0M0RcXHUwNDQzXFx1MDQ0MlxcdTA0NDBcXHUwNDM1XFx1MDQzRFxcdTA0M0RcXHUwNDM4XFx1MDQ0NSBcXHUwNDM4IFxcdTA0MzJcXHUwNDNEXFx1MDQzNVxcdTA0NDhcXHUwNDNEXFx1MDQzOFxcdTA0NDUgXFx1MDQ0MVxcdTA0MzJcXHUwNDRGXFx1MDQzN1xcdTA0MzVcXHUwNDM5IFxcdTA0MzRcXHUwNDM4XFx1MDQ0MVxcdTA0NDZcXHUwNDM4XFx1MDQzRlxcdTA0M0JcXHUwNDM4XFx1MDQzRCBcXHUwNDMyIFxcdTA0NDBcXHUwNDMwXFx1MDQzQ1xcdTA0M0FcXHUwNDMwXFx1MDQ0NSBcXHUwNDQzXFx1MDQ0N1xcdTA0MzVcXHUwNDMxXFx1MDQzRFxcdTA0M0VcXHUwNDMzXFx1MDQzRSBcXHUwNDNGXFx1MDQzQlxcdTA0MzBcXHUwNDNEXFx1MDQzMC48L3A+JztcbiAgICAgICAgdGhpcy53ZWlnaHQgPSBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiAoTWF0aC5yYW5kb20oKSAqIDIpICsgMSk7XG4gICAgICAgIHRoaXMucG9zID0gMDtcbiAgICAgICAgdGhpcy5zdGFydCA9IDA7XG4gICAgICAgIHRoaXMuZW5kID0gMDtcbiAgICB9XG59XG52YXIgaWRfX3ByZWZpeCA9ICdJRCcsIGlkX19jb3VudGVyID0gMDtcbnZhciBuZXdJRCA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZF9fY291bnRlcisrO1xuICAgIHJldHVybiBpZF9fcHJlZml4ICsgaWRfX2NvdW50ZXIudG9TdHJpbmcoKTtcbn07XG52YXIgRGF0YSA9IG1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBzcGVjSWRzID0gW10sIGRpc2NJZHMgPSBbXTtcbiAgICB2YXIgZGF0YSA9IHtcbiAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgc3BlY3M6IHt9LFxuICAgICAgICAgICAgZGlzY3M6IHt9XG4gICAgICAgIH0sXG4gICAgICAgIGNvbm5lY3Rpb25zOiB7XG4gICAgICAgICAgICBmcm9tOiB7fSxcbiAgICAgICAgICAgIHRvOiB7fVxuICAgICAgICB9LFxuICAgICAgICBnZXRTcGVjaWFsdHk6IGZ1bmN0aW9uIChpZCQyKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5kYXRhLnNwZWNzW2lkJDJdO1xuICAgICAgICB9LFxuICAgICAgICBnZXRTcGVjaWFsdHlEaXNjaXBsaW5lTGlzdDogZnVuY3Rpb24gKGlkJDIpIHtcbiAgICAgICAgICAgIHZhciBkaXNjcyA9IHt9O1xuICAgICAgICAgICAgdmFyIGNvbm5lY3Rpb25zID0ge1xuICAgICAgICAgICAgICAgIGZyb206IHt9LFxuICAgICAgICAgICAgICAgIHRvOiB7fVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGZvciAodmFyIF9pID0gMCwgX2wgPSB0aGlzLmNvbm5lY3Rpb25zLmZyb21baWQkMl0ubGVuZ3RoOyBfaSA8IF9sOyBfaSsrKSB7XG4gICAgICAgICAgICAgICAgdmFyIGRpc2MgPSB0aGlzLmNvbm5lY3Rpb25zLmZyb21baWQkMl1bX2ldO1xuICAgICAgICAgICAgICAgIGRpc2NzW2Rpc2NdID0gYW5ndWxhci5jb3B5KHRoaXMuZGF0YS5kaXNjc1tkaXNjXSk7XG4gICAgICAgICAgICAgICAgZGlzY3NbZGlzY10uY1R5cGUgPSAnbWFpbic7XG4gICAgICAgICAgICAgICAgZGlzY3NbZGlzY10uc2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgY29ubmVjdGlvbnMuZnJvbVtkaXNjXSA9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICBjb25uZWN0aW9ucy5mcm9tW2Rpc2NdID0gW107XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgY29ubmVjdGlvbnMudG9bZGlzY10gPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgY29ubmVjdGlvbnMudG9bZGlzY10gPSBbXTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZm9yICh2YXIgX2kkMiA9IDAsIF9sJDIgPSB0aGlzLmNvbm5lY3Rpb25zLmZyb21bZGlzY10ubGVuZ3RoOyBfaSQyIDwgX2wkMjsgX2kkMisrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBzZW1EaXNjID0gdGhpcy5jb25uZWN0aW9ucy5mcm9tW2Rpc2NdW19pJDJdO1xuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGRpc2NzW3NlbURpc2NdID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGlzY3Nbc2VtRGlzY10gPSBhbmd1bGFyLmNvcHkodGhpcy5kYXRhLmRpc2NzW3NlbURpc2NdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2NzW3NlbURpc2NdLmNUeXBlID0gJ3NlY29uZGFyeSc7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNjc1tzZW1EaXNjXS5zZWxlY3RlZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBjb25uZWN0aW9ucy50b1tzZW1EaXNjXSA9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbm5lY3Rpb25zLnRvW3NlbURpc2NdID0gW107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29ubmVjdGlvbnMuZnJvbVtzZW1EaXNjXSA9IFtdO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgY29ubmVjdGlvbnMudG9bc2VtRGlzY10ucHVzaChkaXNjKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbm5lY3Rpb25zLmZyb21bZGlzY10ucHVzaChzZW1EaXNjKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBmb3IgKHZhciBfaSQzID0gMCwgX2wkMyA9IHRoaXMuY29ubmVjdGlvbnMudG9bZGlzY10ubGVuZ3RoOyBfaSQzIDwgX2wkMzsgX2kkMysrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBzZW1EaXNjID0gdGhpcy5jb25uZWN0aW9ucy50b1tkaXNjXVtfaSQzXTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBkaXNjc1tzZW1EaXNjXSA9PT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIHRoaXMuZGF0YS5kaXNjc1tzZW1EaXNjXSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2NzW3NlbURpc2NdID0gYW5ndWxhci5jb3B5KHRoaXMuZGF0YS5kaXNjc1tzZW1EaXNjXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNjc1tzZW1EaXNjXS5jVHlwZSA9ICdzZWNvbmRhcnknO1xuICAgICAgICAgICAgICAgICAgICAgICAgZGlzY3Nbc2VtRGlzY10uc2VsZWN0ZWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgY29ubmVjdGlvbnMuZnJvbVtzZW1EaXNjXSA9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbm5lY3Rpb25zLnRvW3NlbURpc2NdID0gW107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29ubmVjdGlvbnMuZnJvbVtzZW1EaXNjXSA9IFtdO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgY29ubmVjdGlvbnMuZnJvbVtzZW1EaXNjXS5wdXNoKGRpc2MpO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29ubmVjdGlvbnMudG9bZGlzY10ucHVzaChzZW1EaXNjKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgZGlzY3M6IGRpc2NzLFxuICAgICAgICAgICAgICAgIGNvbm5lY3Rpb25zOiBjb25uZWN0aW9uc1xuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgdmFyIG5hbWVzID0gW1xuICAgICAgICAnXFx1MDQyRFxcdTA0M0ZcXHUwNDM4XFx1MDQzNFxcdTA0MzVcXHUwNDNDXFx1MDQzOFxcdTA0M0VcXHUwNDNCXFx1MDQzRVxcdTA0MzNcXHUwNDM4XFx1MDQ0RiBcXHUwNDM4IFxcdTA0MzNcXHUwNDNFXFx1MDQ0MVxcdTA0NDNcXHUwNDM0XFx1MDQzMFxcdTA0NDBcXHUwNDQxXFx1MDQ0MlxcdTA0MzJcXHUwNDM1XFx1MDQzRFxcdTA0M0RcXHUwNDRCXFx1MDQzNSBcXHUwNDQwXFx1MDQzNVxcdTA0MzNcXHUwNDM4XFx1MDQ0MVxcdTA0NDJcXHUwNDQwXFx1MDQ0QicsXG4gICAgICAgICdcXHUwNDFDXFx1MDQzRVxcdTA0M0JcXHUwNDM1XFx1MDQzQVxcdTA0NDNcXHUwNDNCXFx1MDQ0RlxcdTA0NDBcXHUwNDNEXFx1MDQzRS1cXHUwNDMzXFx1MDQzNVxcdTA0M0RcXHUwNDM1XFx1MDQ0MlxcdTA0MzhcXHUwNDQ3XFx1MDQzNVxcdTA0NDFcXHUwNDNBXFx1MDQzOFxcdTA0MzUgXFx1MDQ0MlxcdTA0MzVcXHUwNDQ1XFx1MDQzRFxcdTA0M0VcXHUwNDNCXFx1MDQzRVxcdTA0MzNcXHUwNDM4XFx1MDQzOCcsXG4gICAgICAgICdcXHUwNDI0XFx1MDQ0M1xcdTA0M0RcXHUwNDM0XFx1MDQzMFxcdTA0M0NcXHUwNDM1XFx1MDQzRFxcdTA0NDJcXHUwNDMwXFx1MDQzQlxcdTA0NENcXHUwNDNEXFx1MDQzMFxcdTA0NEYgXFx1MDQzNFxcdTA0MzhcXHUwNDMwXFx1MDQzMVxcdTA0MzVcXHUwNDQyXFx1MDQzRVxcdTA0M0JcXHUwNDNFXFx1MDQzM1xcdTA0MzhcXHUwNDRGJyxcbiAgICAgICAgJ1xcdTA0MjFcXHUwNDNFXFx1MDQzMlxcdTA0NDBcXHUwNDM1XFx1MDQzQ1xcdTA0MzVcXHUwNDNEXFx1MDQzRFxcdTA0NEJcXHUwNDM1IFxcdTA0MzBcXHUwNDNCXFx1MDQzM1xcdTA0M0VcXHUwNDQwXFx1MDQzOFxcdTA0NDJcXHUwNDNDXFx1MDQ0QiBcXHUwNDNCXFx1MDQzNVxcdTA0NDdcXHUwNDM1XFx1MDQzRFxcdTA0MzhcXHUwNDRGJyxcbiAgICAgICAgJ1xcdTA0MTJcXHUwNDNFXFx1MDQzRlxcdTA0NDBcXHUwNDNFXFx1MDQ0MVxcdTA0NEIgXFx1MDQzNFxcdTA0MzhcXHUwNDMwXFx1MDQzM1xcdTA0M0RcXHUwNDNFXFx1MDQ0MVxcdTA0NDJcXHUwNDM4XFx1MDQzQVxcdTA0MzgnLFxuICAgICAgICAnXFx1MDQxNFxcdTA0MzhcXHUwNDMwXFx1MDQzMVxcdTA0MzVcXHUwNDQyXFx1MDQzOFxcdTA0NDdcXHUwNDM1XFx1MDQ0MVxcdTA0M0FcXHUwNDMwXFx1MDQ0RiBcXHUwNDQwXFx1MDQzNVxcdTA0NDJcXHUwNDM4XFx1MDQzRFxcdTA0M0VcXHUwNDNGXFx1MDQzMFxcdTA0NDJcXHUwNDM4XFx1MDQ0RicsXG4gICAgICAgICdcXHUwNDE0XFx1MDQzOFxcdTA0MzBcXHUwNDMxXFx1MDQzNVxcdTA0NDJcXHUwNDM4XFx1MDQ0N1xcdTA0MzVcXHUwNDQxXFx1MDQzQVxcdTA0MzBcXHUwNDRGIFxcdTA0M0RcXHUwNDM1XFx1MDQ0NFxcdTA0NDBcXHUwNDNFXFx1MDQzRlxcdTA0MzBcXHUwNDQyXFx1MDQzOFxcdTA0NEYnLFxuICAgICAgICAnXFx1MDQxMFxcdTA0NDBcXHUwNDQyXFx1MDQzNVxcdTA0NDBcXHUwNDM4XFx1MDQzMFxcdTA0M0JcXHUwNDRDXFx1MDQzRFxcdTA0MzBcXHUwNDRGIFxcdTA0MzNcXHUwNDM4XFx1MDQzRlxcdTA0MzVcXHUwNDQwXFx1MDQ0MlxcdTA0MzVcXHUwNDNEXFx1MDQzN1xcdTA0MzhcXHUwNDRGIFxcdTA0MzggXFx1MDQ0MVxcdTA0MzVcXHUwNDQwXFx1MDQzNFxcdTA0MzVcXHUwNDQ3XFx1MDQzRFxcdTA0M0UtXFx1MDQ0MVxcdTA0M0VcXHUwNDQxXFx1MDQ0M1xcdTA0MzRcXHUwNDM4XFx1MDQ0MVxcdTA0NDJcXHUwNDRCXFx1MDQzNSBcXHUwNDNFXFx1MDQ0MVxcdTA0M0JcXHUwNDNFXFx1MDQzNlxcdTA0M0RcXHUwNDM1XFx1MDQzRFxcdTA0MzhcXHUwNDRGJyxcbiAgICAgICAgJ1xcdTA0MTRcXHUwNDM4XFx1MDQzMFxcdTA0MzFcXHUwNDM1XFx1MDQ0MlxcdTA0MzhcXHUwNDQ3XFx1MDQzNVxcdTA0NDFcXHUwNDNBXFx1MDQzMFxcdTA0NEYgXFx1MDQzRFxcdTA0MzVcXHUwNDM5XFx1MDQ0MFxcdTA0M0VcXHUwNDNGXFx1MDQzMFxcdTA0NDJcXHUwNDM4XFx1MDQ0RicsXG4gICAgICAgICdcXHUwNDIxXFx1MDQzOFxcdTA0M0RcXHUwNDM0XFx1MDQ0MFxcdTA0M0VcXHUwNDNDIFxcdTA0MzRcXHUwNDM4XFx1MDQzMFxcdTA0MzFcXHUwNDM1XFx1MDQ0MlxcdTA0MzhcXHUwNDQ3XFx1MDQzNVxcdTA0NDFcXHUwNDNBXFx1MDQzRVxcdTA0MzkgXFx1MDQ0MVxcdTA0NDJcXHUwNDNFXFx1MDQzRlxcdTA0NEInLFxuICAgICAgICAnXFx1MDQxRVxcdTA0NDFcXHUwNDQyXFx1MDQ0MFxcdTA0NEJcXHUwNDM1IFxcdTA0M0VcXHUwNDQxXFx1MDQzQlxcdTA0M0VcXHUwNDM2XFx1MDQzRFxcdTA0MzVcXHUwNDNEXFx1MDQzOFxcdTA0NEYgXFx1MDQ0MVxcdTA0MzBcXHUwNDQ1XFx1MDQzMFxcdTA0NDBcXHUwNDNEXFx1MDQzRVxcdTA0MzNcXHUwNDNFIFxcdTA0MzRcXHUwNDM4XFx1MDQzMFxcdTA0MzFcXHUwNDM1XFx1MDQ0MlxcdTA0MzAnLFxuICAgICAgICAnXFx1MDQyMVxcdTA0MzBcXHUwNDQ1XFx1MDQzMFxcdTA0NDBcXHUwNDNEXFx1MDQ0QlxcdTA0MzkgXFx1MDQzNFxcdTA0MzhcXHUwNDMwXFx1MDQzMVxcdTA0MzVcXHUwNDQyIFxcdTA0NDMgXFx1MDQzNFxcdTA0MzVcXHUwNDQyXFx1MDQzNVxcdTA0MzkgXFx1MDQzOCBcXHUwNDNGXFx1MDQzRVxcdTA0MzRcXHUwNDQwXFx1MDQzRVxcdTA0NDFcXHUwNDQyXFx1MDQzQVxcdTA0M0VcXHUwNDMyJyxcbiAgICAgICAgJ1xcdTA0MUZcXHUwNDNFXFx1MDQzQ1xcdTA0M0ZcXHUwNDNFXFx1MDQzMlxcdTA0MzBcXHUwNDRGIFxcdTA0MzhcXHUwNDNEXFx1MDQ0MVxcdTA0NDNcXHUwNDNCXFx1MDQzOFxcdTA0M0RcXHUwNDNFXFx1MDQ0MlxcdTA0MzVcXHUwNDQwXFx1MDQzMFxcdTA0M0ZcXHUwNDM4XFx1MDQ0RicsXG4gICAgICAgICdcXHUwNDFEXFx1MDQzNVxcdTA0M0ZcXHUwNDQwXFx1MDQzNVxcdTA0NDBcXHUwNDRCXFx1MDQzMlxcdTA0M0RcXHUwNDNFXFx1MDQzNSBcXHUwNDNDXFx1MDQzRVxcdTA0M0RcXHUwNDM4XFx1MDQ0MlxcdTA0M0VcXHUwNDQwXFx1MDQzOFxcdTA0NDBcXHUwNDNFXFx1MDQzMlxcdTA0MzBcXHUwNDNEXFx1MDQzOFxcdTA0MzUgXFx1MDQzM1xcdTA0M0JcXHUwNDM4XFx1MDQzQVxcdTA0MzVcXHUwNDNDXFx1MDQzOFxcdTA0MzgnLFxuICAgICAgICAnXFx1MDQxRVxcdTA0MzZcXHUwNDM4XFx1MDQ0MFxcdTA0MzVcXHUwNDNEXFx1MDQzOFxcdTA0MzUgXFx1MDQzOCBcXHUwNDNDXFx1MDQzNVxcdTA0NDJcXHUwNDMwXFx1MDQzMVxcdTA0M0VcXHUwNDNCXFx1MDQzOFxcdTA0NDdcXHUwNDM1XFx1MDQ0MVxcdTA0M0FcXHUwNDM4XFx1MDQzOSBcXHUwNDQxXFx1MDQzOFxcdTA0M0RcXHUwNDM0XFx1MDQ0MFxcdTA0M0VcXHUwNDNDJyxcbiAgICAgICAgJ1xcdTA0MUZcXHUwNDQwXFx1MDQzRVxcdTA0NDRcXHUwNDM4XFx1MDQzQlxcdTA0MzBcXHUwNDNBXFx1MDQ0MlxcdTA0MzhcXHUwNDNBXFx1MDQzMCBcXHUwNDQxXFx1MDQzMFxcdTA0NDVcXHUwNDMwXFx1MDQ0MFxcdTA0M0RcXHUwNDNFXFx1MDQzM1xcdTA0M0UgXFx1MDQzNFxcdTA0MzhcXHUwNDMwXFx1MDQzMVxcdTA0MzVcXHUwNDQyXFx1MDQzMCBcXHUwNDM4IFxcdTA0MzVcXHUwNDMzXFx1MDQzRSBcXHUwNDNFXFx1MDQ0MVxcdTA0M0JcXHUwNDNFXFx1MDQzNlxcdTA0M0RcXHUwNDM1XFx1MDQzRFxcdTA0MzhcXHUwNDM5JyxcbiAgICAgICAgJ1xcdTA0MjBcXHUwNDM1XFx1MDQzRlxcdTA0NDBcXHUwNDNFXFx1MDQzNFxcdTA0NDNcXHUwNDNBXFx1MDQ0MlxcdTA0MzhcXHUwNDMyXFx1MDQzRFxcdTA0M0VcXHUwNDM1IFxcdTA0MzdcXHUwNDM0XFx1MDQzRVxcdTA0NDBcXHUwNDNFXFx1MDQzMlxcdTA0NENcXHUwNDM1IFxcdTA0MzggXFx1MDQ0MVxcdTA0MzBcXHUwNDQ1XFx1MDQzMFxcdTA0NDBcXHUwNDNEXFx1MDQ0QlxcdTA0MzkgXFx1MDQzNFxcdTA0MzhcXHUwNDMwXFx1MDQzMVxcdTA0MzVcXHUwNDQyJyxcbiAgICAgICAgJ1xcdTA0MjFcXHUwNDMwXFx1MDQ0NVxcdTA0MzBcXHUwNDQwXFx1MDQzRFxcdTA0NEJcXHUwNDM5IFxcdTA0MzRcXHUwNDM4XFx1MDQzMFxcdTA0MzFcXHUwNDM1XFx1MDQ0MiBcXHUwNDM4IFxcdTA0MzFcXHUwNDM1XFx1MDQ0MFxcdTA0MzVcXHUwNDNDXFx1MDQzNVxcdTA0M0RcXHUwNDNEXFx1MDQzRVxcdTA0NDFcXHUwNDQyXFx1MDQ0QycsXG4gICAgICAgICdcXHUwNDEzXFx1MDQzNVxcdTA0NDFcXHUwNDQyXFx1MDQzMFxcdTA0NDZcXHUwNDM4XFx1MDQzRVxcdTA0M0RcXHUwNDNEXFx1MDQ0QlxcdTA0MzkgXFx1MDQ0MVxcdTA0MzBcXHUwNDQ1XFx1MDQzMFxcdTA0NDBcXHUwNDNEXFx1MDQ0QlxcdTA0MzkgXFx1MDQzNFxcdTA0MzhcXHUwNDMwXFx1MDQzMVxcdTA0MzVcXHUwNDQyJyxcbiAgICAgICAgJ1xcdTA0MUZcXHUwNDQwXFx1MDQzRVxcdTA0MzNcXHUwNDQwXFx1MDQzMFxcdTA0M0NcXHUwNDNDXFx1MDQ0QiBcXHUwNDNFXFx1MDQzMVxcdTA0NDNcXHUwNDQ3XFx1MDQzNVxcdTA0M0RcXHUwNDM4XFx1MDQ0RiBcXHUwNDMxXFx1MDQzRVxcdTA0M0JcXHUwNDRDXFx1MDQzRFxcdTA0NEJcXHUwNDQ1IFxcdTA0NDFcXHUwNDMwXFx1MDQ0NVxcdTA0MzBcXHUwNDQwXFx1MDQzRFxcdTA0NEJcXHUwNDNDIFxcdTA0MzRcXHUwNDM4XFx1MDQzMFxcdTA0MzFcXHUwNDM1XFx1MDQ0MlxcdTA0M0VcXHUwNDNDJyxcbiAgICAgICAgJ1xcdTA0MUZcXHUwNDQwXFx1MDQzRVxcdTA0MzFcXHUwNDNCXFx1MDQzNVxcdTA0M0NcXHUwNDRCIFxcdTA0MzNcXHUwNDNCXFx1MDQzOFxcdTA0M0FcXHUwNDM1XFx1MDQzQ1xcdTA0MzhcXHUwNDQ3XFx1MDQzNVxcdTA0NDFcXHUwNDNBXFx1MDQzRVxcdTA0MzNcXHUwNDNFIFxcdTA0M0FcXHUwNDNFXFx1MDQzRFxcdTA0NDJcXHUwNDQwXFx1MDQzRVxcdTA0M0JcXHUwNDRGJyxcbiAgICAgICAgJ1xcdTA0MjFcXHUwNDNFXFx1MDQ0N1xcdTA0MzVcXHUwNDQyXFx1MDQzMFxcdTA0M0RcXHUwNDNEXFx1MDQzMFxcdTA0NEYgXFx1MDQ0RFxcdTA0M0RcXHUwNDM0XFx1MDQzRVxcdTA0M0FcXHUwNDQwXFx1MDQzOFxcdTA0M0RcXHUwNDNEXFx1MDQzMFxcdTA0NEYgXFx1MDQzRlxcdTA0MzBcXHUwNDQyXFx1MDQzRVxcdTA0M0JcXHUwNDNFXFx1MDQzM1xcdTA0MzhcXHUwNDRGJyxcbiAgICAgICAgJ1xcdTA0MUVcXHUwNDQxXFx1MDQzRVxcdTA0MzFcXHUwNDM1XFx1MDQzRFxcdTA0M0RcXHUwNDNFXFx1MDQ0MVxcdTA0NDJcXHUwNDM4IFxcdTA0NDFcXHUwNDNFXFx1MDQzQ1xcdTA0MzBcXHUwNDQyXFx1MDQzOFxcdTA0NDdcXHUwNDM1XFx1MDQ0MVxcdTA0M0FcXHUwNDM4XFx1MDQ0NSBcXHUwNDM3XFx1MDQzMFxcdTA0MzFcXHUwNDNFXFx1MDQzQlxcdTA0MzVcXHUwNDMyXFx1MDQzMFxcdTA0M0RcXHUwNDM4XFx1MDQzOScsXG4gICAgICAgICdcXHUwNDFEXFx1MDQzMFxcdTA0NDBcXHUwNDQzXFx1MDQ0OFxcdTA0MzVcXHUwNDNEXFx1MDQzOFxcdTA0NEYgXFx1MDQzQVxcdTA0MzBcXHUwNDNCXFx1MDQ0Q1xcdTA0NDZcXHUwNDM4XFx1MDQzOS1cXHUwNDQ0XFx1MDQzRVxcdTA0NDFcXHUwNDQ0XFx1MDQzRVxcdTA0NDBcXHUwNDNEXFx1MDQzRVxcdTA0MzNcXHUwNDNFIFxcdTA0M0VcXHUwNDMxXFx1MDQzQ1xcdTA0MzVcXHUwNDNEXFx1MDQzMCcsXG4gICAgICAgICdcXHUwNDIxXFx1MDQzMFxcdTA0NDVcXHUwNDMwXFx1MDQ0MFxcdTA0M0RcXHUwNDRCXFx1MDQzOSBcXHUwNDM0XFx1MDQzOFxcdTA0MzBcXHUwNDMxXFx1MDQzNVxcdTA0NDIgXFx1MDQzOCBcXHUwNDNFXFx1MDQzRFxcdTA0M0FcXHUwNDNFXFx1MDQzQlxcdTA0M0VcXHUwNDMzXFx1MDQzOFxcdTA0NEYnLFxuICAgICAgICAnXFx1MDQyMFxcdTA0MzVcXHUwNDM0XFx1MDQzQVxcdTA0MzhcXHUwNDM1IFxcdTA0NDRcXHUwNDNFXFx1MDQ0MFxcdTA0M0NcXHUwNDRCIFxcdTA0NDFcXHUwNDMwXFx1MDQ0NVxcdTA0MzBcXHUwNDQwXFx1MDQzRFxcdTA0M0VcXHUwNDMzXFx1MDQzRSBcXHUwNDM0XFx1MDQzOFxcdTA0MzBcXHUwNDMxXFx1MDQzNVxcdTA0NDJcXHUwNDMwJyxcbiAgICAgICAgJ1xcdTA0MTJcXHUwNDM4XFx1MDQzN1xcdTA0NDNcXHUwNDMwXFx1MDQzQlxcdTA0MzhcXHUwNDM3XFx1MDQzMFxcdTA0NDZcXHUwNDM4XFx1MDQ0RiBcXHUwNDMzXFx1MDQ0MFxcdTA0MzBcXHUwNDQ0XFx1MDQzMCBcXHUwNDMyXFx1MDQzRFxcdTA0NDNcXHUwNDQyXFx1MDQ0MFxcdTA0MzVcXHUwNDNEXFx1MDQzRFxcdTA0MzhcXHUwNDQ1IFxcdTA0MzggXFx1MDQzMlxcdTA0M0RcXHUwNDM1XFx1MDQ0OFxcdTA0M0RcXHUwNDM4XFx1MDQ0NSBcXHUwNDQxXFx1MDQzMlxcdTA0NEZcXHUwNDM3XFx1MDQzNVxcdTA0MzknXG4gICAgXTtcbiAgICB2YXIgbmwgPSBuYW1lcy5sZW5ndGgsIG5pID0gMDtcbiAgICB2YXIgc2MgPSBjZmcuc3BlY2lhbHR5LmNvdW50LCBkYyA9IGNmZy5kaXNjaXBsaW5lLmNvdW50LCBzMmRjID0gY2ZnLmNvbm5lY3Rpb25zO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc2M7IGkrKykge1xuICAgICAgICB2YXIgaWQgPSBuZXdJRCgpO1xuICAgICAgICBuaSsrO1xuICAgICAgICBkYXRhLmRhdGEuc3BlY3NbaWRdID0gbmV3IFNwZWNpYWx0eShpZCwgbmFtZXNbbmkgJSBubF0pO1xuICAgICAgICBkYXRhLmNvbm5lY3Rpb25zLmZyb21baWRdID0gW107XG4gICAgICAgIGRhdGEuY29ubmVjdGlvbnMudG9baWRdID0gW107XG4gICAgICAgIHNwZWNJZHMucHVzaChpZCk7XG4gICAgfVxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZGM7IGkrKykge1xuICAgICAgICB2YXIgaWQgPSBuZXdJRCgpO1xuICAgICAgICBuaSsrO1xuICAgICAgICBkYXRhLmRhdGEuZGlzY3NbaWRdID0gbmV3IERpc2NpcGxpbmUoaWQsIG5hbWVzW25pICUgbmxdKTtcbiAgICAgICAgZGF0YS5jb25uZWN0aW9ucy5mcm9tW2lkXSA9IFtdO1xuICAgICAgICBkYXRhLmNvbm5lY3Rpb25zLnRvW2lkXSA9IFtdO1xuICAgICAgICBkaXNjSWRzLnB1c2goaWQpO1xuICAgIH1cbiAgICBmb3IgKHZhciBpIGluIGRhdGEuZGF0YS5zcGVjcykge1xuICAgICAgICBmb3IgKHZhciBqID0gMDsgaiA8IHMyZGM7IGorKykge1xuICAgICAgICAgICAgdmFyIHJhbmQgPSBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiAoZGMgLSAxKSk7XG4gICAgICAgICAgICB3aGlsZSAoZGF0YS5jb25uZWN0aW9ucy5mcm9tW2ldLmluZGV4T2YoZGlzY0lkc1tyYW5kXSkgPj0gMCkge1xuICAgICAgICAgICAgICAgIHJhbmQgPSBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiAoZGMgLSAxKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBTZXR0aW5nIHVwIHRoZSBzcGVjaWFsdHktPmRpc2NpcGxpbmUgY29ubmVjdGlvbnNcbiAgICAgICAgICAgIGRhdGEuY29ubmVjdGlvbnMuZnJvbVtpXS5wdXNoKGRpc2NJZHNbcmFuZF0pO1xuICAgICAgICAgICAgZGF0YS5jb25uZWN0aW9ucy50b1tkaXNjSWRzW3JhbmRdXS5wdXNoKGkpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGZvciAodmFyIGogPSAwOyBqIDwgZGMgLyAxLjc7IGorKykge1xuICAgICAgICB2YXIgcmFuZCA9IE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqIChkYyAtIDEpKSwgcmFuZDIgPSBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiAoZGMgLSAxKSk7XG4gICAgICAgIGlmICghKGRhdGEuY29ubmVjdGlvbnMuZnJvbVtkaXNjSWRzW3JhbmRdXS5pbmRleE9mKGRpc2NJZHNbcmFuZDJdKSA+PSAwICYmIGRhdGEuY29ubmVjdGlvbnMudG9bZGlzY0lkc1tyYW5kMl1dLmluZGV4T2YoZGlzY0lkc1tyYW5kXSkgPj0gMCkpIHtcbiAgICAgICAgICAgIGRhdGEuY29ubmVjdGlvbnMuZnJvbVtkaXNjSWRzW3JhbmRdXS5wdXNoKGRpc2NJZHNbcmFuZDJdKTtcbiAgICAgICAgICAgIGRhdGEuY29ubmVjdGlvbnMudG9bZGlzY0lkc1tyYW5kMl1dLnB1c2goZGlzY0lkc1tyYW5kXSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGRhdGE7XG59OyIsInZhciBjZmcgPSByZXF1aXJlKCcuL2NvbmZpZycpO1xuZnVuY3Rpb24gcHVzaFNpbmdsZShhcnJheSwgaXRlbSkge1xuICAgIGlmIChhcnJheS5pbmRleE9mKGl0ZW0pID09PSAtMSkge1xuICAgICAgICBhcnJheS5wdXNoKGl0ZW0pO1xuICAgIH1cbn1cbmNsYXNzIERpc2NpcGxpbmUge1xuICAgIGNvbnN0cnVjdG9yKGRhdGEsIHNlbGVjdGVkKSB7XG4gICAgICAgIHRoaXMuaWQgPSAnJyArIGRhdGEuaWQ7XG4gICAgICAgIHRoaXMubmFtZSA9IGRhdGEubmFtZTtcbiAgICAgICAgdGhpcy5oaW50ID0gZGF0YS5oaW50O1xuICAgICAgICB0aGlzLmRlc2NyaXB0aW9uID0gZGF0YS5kZXNjcmlwdGlvbjtcbiAgICAgICAgdGhpcy5zZWxlY3RlZCA9IHNlbGVjdGVkO1xuICAgICAgICB2YXIgY28gPSBkYXRhLmMgPyBkYXRhLmNbMF0gPyBkYXRhLmNbMF0gOiB7fSA6IHt9O1xuICAgICAgICB0aGlzLnR5cGUgPSBjby50eXBlIHx8ICdvJztcbiAgICAgICAgdGhpcy53ZWlnaHQgPSBNYXRoLmNlaWwoKGNvLndlaWdodCB8fCAzKSAvIDMpO1xuICAgICAgICB0aGlzLnNlbWVzdGVyX2Zyb20gPSAoY28uc2VtZXN0ZXJfZnJvbSB8fCAwKSAtIDE7XG4gICAgICAgIHRoaXMuc2VtZXN0ZXJfdG8gPSAoY28uc2VtZXN0ZXJfdG8gfHwgMCkgLSAxO1xuICAgICAgICB0aGlzLmNhdGVnb3J5ID0gY28uY2F0ZWdvcnkgfHwge307XG4gICAgICAgIHRoaXMucG9zID0gMDtcbiAgICAgICAgdGhpcy5zdGFydCA9IDA7XG4gICAgICAgIHRoaXMuZW5kID0gMDtcbiAgICB9XG59XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIERhdGFDb252ZXJ0KCkge1xuICAgIHJldHVybiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICB2YXIgcmVzdWx0ID0ge1xuICAgICAgICAgICAgZGlzY2lwbGluZXM6IHt9LFxuICAgICAgICAgICAgY2F0ZWdvcmllczoge30sXG4gICAgICAgICAgICBjb25uZWN0aW9uczoge1xuICAgICAgICAgICAgICAgIGZyb206IHt9LFxuICAgICAgICAgICAgICAgIHRvOiB7fSxcbiAgICAgICAgICAgICAgICB0eXBlczoge31cbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdmFyIC8vIGNvbnNvbGUubG9nKGRhdGEpO1xuICAgICAgICBjb252ZXJ0RGlzY2lwbGluZSA9IGZ1bmN0aW9uIChkaXNjLCBzZWxlY3RlZCkge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQuZGlzY2lwbGluZXNbZGlzYy5pZF0gPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICB2YXIgY0Rpc2MgPSByZXN1bHQuZGlzY2lwbGluZXNbZGlzYy5pZF0gPSBuZXcgRGlzY2lwbGluZShkaXNjLCBzZWxlY3RlZCk7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQuY2F0ZWdvcmllc1tjRGlzYy5jYXRlZ29yeS5pZF0gPT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIGNEaXNjLmNhdGVnb3J5LmlkICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQuY2F0ZWdvcmllc1tjRGlzYy5jYXRlZ29yeS5pZF0gPSBjRGlzYy5jYXRlZ29yeTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQuY29ubmVjdGlvbnMudHlwZXNbY0Rpc2MudHlwZV0gPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbm5lY3Rpb25zLnR5cGVzW2NEaXNjLnR5cGVdID0gW107XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJlc3VsdC5jb25uZWN0aW9ucy50eXBlc1tjRGlzYy50eXBlXS5wdXNoKGNEaXNjLmlkKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdC5kaXNjaXBsaW5lc1tkaXNjLmlkXS5zZWxlY3RlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmICgoKGRpc2MuYyB8fCBbXSlbMF0gfHwge30pLnR5cGUgPT0gJ20nKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdC5kaXNjaXBsaW5lc1tkaXNjLmlkXS50eXBlID0gJ3AnO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdmFyIGFkZE5leHQgPSBbXTtcbiAgICAgICAgZm9yICh2YXIgX2kgPSAwLCBfbCA9IGRhdGEubGVuZ3RoOyBfaSA8IF9sOyBfaSsrKSB7XG4gICAgICAgICAgICB2YXIgaXRlbSA9IGRhdGFbX2ldO1xuICAgICAgICAgICAgY29udmVydERpc2NpcGxpbmUoaXRlbSwgdHJ1ZSk7XG4gICAgICAgICAgICBpZiAodHlwZW9mIHJlc3VsdC5jb25uZWN0aW9ucy5mcm9tW2l0ZW0uaWRdID09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0LmNvbm5lY3Rpb25zLmZyb21baXRlbS5pZF0gPSBbXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh0eXBlb2YgcmVzdWx0LmNvbm5lY3Rpb25zLnRvW2l0ZW1dID09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0LmNvbm5lY3Rpb25zLnRvW2l0ZW0uaWRdID0gW107XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgZm9yICh2YXIgX2kkMiA9IDAsIF9sJDIgPSBkYXRhLmxlbmd0aDsgX2kkMiA8IF9sJDI7IF9pJDIrKykge1xuICAgICAgICAgICAgdmFyIGl0ZW0gPSBkYXRhW19pJDJdO1xuICAgICAgICAgICAgZm9yICh2YXIgX2kkMyA9IDAsIF9sJDMgPSBpdGVtLmdvYWwubGVuZ3RoOyBfaSQzIDwgX2wkMzsgX2kkMysrKSB7XG4gICAgICAgICAgICAgICAgdmFyIHBOb3Rpb24gPSBpdGVtLmdvYWxbX2kkM107XG4gICAgICAgICAgICAgICAgZm9yICh2YXIgX2kkNCA9IDAsIF9sJDQgPSBwTm90aW9uLmRpc2NpcGxpbmVvYmplY3QubGVuZ3RoOyBfaSQ0IDwgX2wkNDsgX2kkNCsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBwSXRlbSA9IHBOb3Rpb24uZGlzY2lwbGluZW9iamVjdFtfaSQ0XTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBJdGVtLmlkID09IGl0ZW0uaWQpXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICAgICAgY29udmVydERpc2NpcGxpbmUocEl0ZW0sIGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQuY29ubmVjdGlvbnMuZnJvbVtwSXRlbS5pZF0gPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5jb25uZWN0aW9ucy5mcm9tW3BJdGVtLmlkXSA9IFtdO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgcmVzdWx0LmNvbm5lY3Rpb25zLnRvW3BJdGVtLmlkXSA9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbm5lY3Rpb25zLnRvW3BJdGVtLmlkXSA9IFtdO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHB1c2hTaW5nbGUocmVzdWx0LmNvbm5lY3Rpb25zLmZyb21baXRlbS5pZF0sIHBJdGVtLmlkKTtcbiAgICAgICAgICAgICAgICAgICAgcHVzaFNpbmdsZShyZXN1bHQuY29ubmVjdGlvbnMudG9bcEl0ZW0uaWRdLCBpdGVtLmlkKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZm9yICh2YXIgX2kkNSA9IDAsIF9sJDUgPSBwTm90aW9uLmRpc2NpcGxpbmVnb2FsLmxlbmd0aDsgX2kkNSA8IF9sJDU7IF9pJDUrKykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgcEl0ZW0gPSBwTm90aW9uLmRpc2NpcGxpbmVnb2FsW19pJDVdO1xuICAgICAgICAgICAgICAgICAgICBpZiAocEl0ZW0uaWQgPT0gaXRlbS5pZClcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgICAgICBjb252ZXJ0RGlzY2lwbGluZShwSXRlbSwgZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHJlc3VsdC5jb25uZWN0aW9ucy5mcm9tW3BJdGVtLmlkXSA9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbm5lY3Rpb25zLmZyb21bcEl0ZW0uaWRdID0gW107XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQuY29ubmVjdGlvbnMudG9bcEl0ZW0uaWRdID09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuY29ubmVjdGlvbnMudG9bcEl0ZW0uaWRdID0gW107XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBmb3IgKHZhciBfaSQ2ID0gMCwgX2wkNiA9IGl0ZW0ub2JqZWN0Lmxlbmd0aDsgX2kkNiA8IF9sJDY7IF9pJDYrKykge1xuICAgICAgICAgICAgICAgIHZhciBjTm90aW9uID0gaXRlbS5vYmplY3RbX2kkNl07XG4gICAgICAgICAgICAgICAgZm9yICh2YXIgX2kkNyA9IDAsIF9sJDcgPSBjTm90aW9uLmRpc2NpcGxpbmVnb2FsLmxlbmd0aDsgX2kkNyA8IF9sJDc7IF9pJDcrKykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgY0l0ZW0gPSBjTm90aW9uLmRpc2NpcGxpbmVnb2FsW19pJDddO1xuICAgICAgICAgICAgICAgICAgICBpZiAoY0l0ZW0uaWQgPT0gaXRlbS5pZClcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgICAgICBjb252ZXJ0RGlzY2lwbGluZShjSXRlbSwgZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHJlc3VsdC5jb25uZWN0aW9ucy5mcm9tW2NJdGVtLmlkXSA9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbm5lY3Rpb25zLmZyb21bY0l0ZW0uaWRdID0gW107XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQuY29ubmVjdGlvbnMudG9bY0l0ZW0uaWRdID09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuY29ubmVjdGlvbnMudG9bY0l0ZW0uaWRdID0gW107XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgcHVzaFNpbmdsZShyZXN1bHQuY29ubmVjdGlvbnMudG9baXRlbS5pZF0sIGNJdGVtLmlkKTtcbiAgICAgICAgICAgICAgICAgICAgcHVzaFNpbmdsZShyZXN1bHQuY29ubmVjdGlvbnMuZnJvbVtjSXRlbS5pZF0sIGl0ZW0uaWQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBmb3IgKHZhciBfaSQ4ID0gMCwgX2wkOCA9IGNOb3Rpb24uZGlzY2lwbGluZW9iamVjdC5sZW5ndGg7IF9pJDggPCBfbCQ4OyBfaSQ4KyspIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGNJdGVtID0gY05vdGlvbi5kaXNjaXBsaW5lb2JqZWN0W19pJDhdO1xuICAgICAgICAgICAgICAgICAgICBpZiAoY0l0ZW0uaWQgPT0gaXRlbS5pZClcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgICAgICBjb252ZXJ0RGlzY2lwbGluZShjSXRlbSwgZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHJlc3VsdC5jb25uZWN0aW9ucy5mcm9tW2NJdGVtLmlkXSA9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmNvbm5lY3Rpb25zLmZyb21bY0l0ZW0uaWRdID0gW107XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQuY29ubmVjdGlvbnMudG9bY0l0ZW0uaWRdID09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuY29ubmVjdGlvbnMudG9bY0l0ZW0uaWRdID0gW107XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xufTsiLCJPYmplY3QuZmlsdGVyID0gZnVuY3Rpb24gKG9iaiwgZnVuYykge1xuICAgIG9iaiA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkob2JqKSk7XG4gICAgZm9yICh2YXIga2V5IGluIG9iaikge1xuICAgICAgICBpZiAob2JqLmhhc093blByb3BlcnR5KGtleSkgJiYgIWZ1bmMob2JqW2tleV0sIGtleSkpIHtcbiAgICAgICAgICAgIGRlbGV0ZSBvYmpba2V5XTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gb2JqO1xufTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gRGF0YVNvcnQoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChkYXRhLCBjZmcpIHtcbiAgICAgICAgdmFyIHRhYmxlID0ge307XG4gICAgICAgIHZhciBzZW1lc3RlcldlaWdodHMgPSBuZXdBcnJheShjZmcuc2VtZXN0ZXIuY291bnQgKyAyLCAwKTtcbiAgICAgICAgdmFyIHNlbWVzdGVyRGlzY3MgPSBuZXdBcnJheShjZmcuc2VtZXN0ZXIuY291bnQgKyAyLCAwKTtcbiAgICAgICAgdmFyIGNkYXRhID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShkYXRhKSk7XG4gICAgICAgIGNkYXRhLmRpc2NpcGxpbmVzID0gT2JqZWN0LmZpbHRlcihjZGF0YS5kaXNjaXBsaW5lcywgZnVuY3Rpb24gKGVsZW0pIHtcbiAgICAgICAgICAgIHJldHVybiBlbGVtLnNlbGVjdGVkO1xuICAgICAgICB9KTtcbiAgICAgICAgdmFyIGRpc2NzID0gY2RhdGEuZGlzY2lwbGluZXM7XG4gICAgICAgIHZhciBpblNlbWVzdGVycyA9IG5ld0FycmF5KGNmZy5zZW1lc3Rlci5jb3VudCArIDIsIFtdKTtcbiAgICAgICAgdmFyIHF1ZXVlID0gT2JqZWN0LmtleXMoY2RhdGEuZGlzY2lwbGluZXMpO1xuICAgICAgICB2YXIgZGF0YVdlaWdodCA9IHF1ZXVlLnJlZHVjZShmdW5jdGlvbiAobywgbikge1xuICAgICAgICAgICAgcmV0dXJuIG8gKyBjZGF0YS5kaXNjaXBsaW5lc1tuXS53ZWlnaHQ7XG4gICAgICAgIH0sIDApO1xuICAgICAgICB2YXIgY3VycmVudFNlbWVzdGVyID0gMCwgc2VtZXN0ZXJXaWR0aCA9IDA7XG4gICAgICAgIHZhciBzdGFydGVkID0gW107XG4gICAgICAgIHZhciBjb25uZWN0aW9uc0ZpbHRlciA9IGZ1bmN0aW9uIChsaXN0KSB7XG4gICAgICAgICAgICBmb3IgKHZhciBpJDIgaW4gbGlzdCkge1xuICAgICAgICAgICAgICAgIGlmICghbGlzdC5oYXNPd25Qcm9wZXJ0eShpJDIpKVxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGNkYXRhLmRpc2NpcGxpbmVzW2kkMl0gPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBsaXN0W2kkMl07XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaiQyID0gMDsgaiQyIDwgbGlzdFtpJDJdLmxlbmd0aDsgaiQyKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgY2RhdGEuZGlzY2lwbGluZXNbbGlzdFtpJDJdW2okMl1dID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpc3RbaSQyXS5zcGxpY2UoaiQyLCAxKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBqJDItLTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBsaXN0O1xuICAgICAgICB9O1xuICAgICAgICB2YXIgLy8gY29uc29sZS5sb2coY2RhdGEpO1xuICAgICAgICBjb25uZWN0aW9ucyA9IHtcbiAgICAgICAgICAgIGZyb206IGNvbm5lY3Rpb25zRmlsdGVyKGNkYXRhLmNvbm5lY3Rpb25zLmZyb20pLFxuICAgICAgICAgICAgdG86IGNvbm5lY3Rpb25zRmlsdGVyKGNkYXRhLmNvbm5lY3Rpb25zLnRvKVxuICAgICAgICB9O1xuICAgICAgICBzb3J0LmNhbGwocXVldWUsIGNvbXBhcmF0b3IpO1xuICAgICAgICBmb3IgKC8vIHF1ZXVlLnNvcnQoY29tcGFyYXRvcik7XG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhxdWV1ZSk7XG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhkYXRhLmNvbm5lY3Rpb25zKTtcbiAgICAgICAgICAgIHZhciBpID0gMDsgaSA8IHF1ZXVlLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAvLyBGaWxsaW5nIHRoZSB0YWJsZSByb3cgd2l0aCBudWxsJ3MuXG4gICAgICAgICAgICB0YWJsZVtxdWV1ZVtpXV0gPSBuZXdBcnJheShjZmcuc2VtZXN0ZXIuY291bnQgKiBjZmcuc2VtZXN0ZXIud2VpZ2h0LCAwKTtcbiAgICAgICAgICAgIGlmICgvLyBGaWxsaW5nIHVwIGEgaGFyZCBwb3NpdGlvbmVkIHN0dWZmLlxuICAgICAgICAgICAgICAgIGRpc2NzW3F1ZXVlW2ldXS5zZW1lc3Rlcl9mcm9tID49IDApIHtcbiAgICAgICAgICAgICAgICBpZiAoZGlzY3NbcXVldWVbaV1dLnNlbWVzdGVyX3RvID09PSAtMSkge1xuICAgICAgICAgICAgICAgICAgICBkaXNjc1txdWV1ZVtpXV0uc2VtZXN0ZXJfdG8gPSBkaXNjc1txdWV1ZVtpXV0uc2VtZXN0ZXJfZnJvbTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdmFyIC8vIFNldHRpbmcgdGhlIHNlbWVzdGVycyB3ZWlnaHRzLlxuICAgICAgICAgICAgICAgIGRTZW1lc3RlcldlaWdodCA9IGRpc2NzW3F1ZXVlW2ldXS53ZWlnaHQgLyAoZGlzY3NbcXVldWVbaV1dLnNlbWVzdGVyX3RvIC0gZGlzY3NbcXVldWVbaV1dLnNlbWVzdGVyX2Zyb20gKyAxKSB8IDA7XG4gICAgICAgICAgICAgICAgZFNlbWVzdGVyV2VpZ2h0ID0gTWF0aC5jZWlsKGRTZW1lc3RlcldlaWdodCAvIDMpICogMztcbiAgICAgICAgICAgICAgICBkaXNjc1txdWV1ZVtpXV0uc3RhcnQgPSBkaXNjc1txdWV1ZVtpXV0uc2VtZXN0ZXJfZnJvbSAqIGNmZy5zZW1lc3Rlci53ZWlnaHQ7XG4gICAgICAgICAgICAgICAgZGlzY3NbcXVldWVbaV1dLmVuZCA9IChkaXNjc1txdWV1ZVtpXV0uc2VtZXN0ZXJfdG8gKyAxKSAqIGNmZy5zZW1lc3Rlci53ZWlnaHQ7XG4gICAgICAgICAgICAgICAgZm9yICh2YXIgayA9IGRpc2NzW3F1ZXVlW2ldXS5zZW1lc3Rlcl9mcm9tOyBrIDw9IGRpc2NzW3F1ZXVlW2ldXS5zZW1lc3Rlcl90bzsgaysrKSB7XG4gICAgICAgICAgICAgICAgICAgIHNlbWVzdGVyRGlzY3Nba10rKztcbiAgICAgICAgICAgICAgICAgICAgc2VtZXN0ZXJXZWlnaHRzW2tdICs9IGRTZW1lc3RlcldlaWdodDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZGF0YVdlaWdodCAtPSBkaXNjc1txdWV1ZVtpXV0ud2VpZ2h0O1xuICAgICAgICAgICAgICAgIGRpc2NzW3F1ZXVlW2ldXS53ZWlnaHQgPSAwO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHdoaWxlIChkYXRhV2VpZ2h0ID4gMCkge1xuICAgICAgICAgICAgdmFyIGNoYW5nZSA9IGZhbHNlO1xuICAgICAgICAgICAgZm9yICgvLyBFYWNoIGVsZW1lbnRcbiAgICAgICAgICAgICAgICB2YXIgaSA9IDA7IGkgPCBxdWV1ZS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGlmIChkaXNjc1txdWV1ZVtpXV0ud2VpZ2h0ID4gMCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoLy8gQ2hlY2tpbmcgaWYgdGhlcmUgYXJlIHNvbWUgc3BhY2UgaW4gY3VycmVudCBzZW1lc3Rlci5cbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbWVzdGVyV2VpZ2h0c1tjdXJyZW50U2VtZXN0ZXJdID49IGNmZy5zZW1lc3Rlci53ZWlnaHQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1cnJlbnRTZW1lc3RlcisrO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc2VtZXN0ZXJXZWlnaHRzW2N1cnJlbnRTZW1lc3Rlcl0gPCBjZmcuc2VtZXN0ZXIud2VpZ2h0ICYmIHNlbWVzdGVyRGlzY3NbY3VycmVudFNlbWVzdGVyXSA+PSBjZmcuc2VtZXN0ZXIubGltaXQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpblNlbWVzdGVyc1tjdXJyZW50U2VtZXN0ZXJdLmluZGV4T2YocXVldWVbaV0pIDwgMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmICgvLyBDaGVja2luZyBpZiB0aGVyZSBhcmUgYW55IHVuZmluaXNoZWQgcGFyZW50cy5cbiAgICAgICAgICAgICAgICAgICAgICAgIGFycmF5SW5BcnJheShjb25uZWN0aW9ucy50b1txdWV1ZVtpXV0sIHN0YXJ0ZWQpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB2YXIgcG9zID0gMCwgc3RhcnRlZE5vdyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICBpZiAoLy8gQ2hlY2tpbmcgaWYgdGhpcyBpcyBhIG5ldyBkaXNjaXBsaW5lIGFuZCBzdGFydGluZyBpdCBvciBpdCBoYXNcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGJlZW4gYWxyZWFkeSBzdGFydGVkLlxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQW5kIHNldHRpbmcgdXAgY3VycmVudCBwb3NpdGlvbiByZWxhdGl2ZWx5IHRvIHRoYXQuXG4gICAgICAgICAgICAgICAgICAgICAgICBzdGFydGVkLmluZGV4T2YocXVldWVbaV0pIDwgMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhcnRlZE5vdyA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICBwb3MgPSBwbWF4KHF1ZXVlW2ldKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0ZWQucHVzaChxdWV1ZVtpXSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwb3MgPSBkaXNjc1txdWV1ZVtpXV0uZW5kO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmICgvLyBDaGVja2luZyBpZiBjdXJyZW50IGRpc2NpcGxpbmUgaXMgbmV3IGluIHRob2lzIHNlbWVzdGVyIG9yIG5vdC5cbiAgICAgICAgICAgICAgICAgICAgICAgIGluU2VtZXN0ZXJzW2N1cnJlbnRTZW1lc3Rlcl0uaW5kZXhPZihxdWV1ZVtpXSkgPCAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZW1lc3RlckRpc2NzW2N1cnJlbnRTZW1lc3Rlcl0rKztcbiAgICAgICAgICAgICAgICAgICAgICAgIGluU2VtZXN0ZXJzW2N1cnJlbnRTZW1lc3Rlcl0ucHVzaChxdWV1ZVtpXSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHBvcyA8IGN1cnJlbnRTZW1lc3RlciAqIGNmZy5zZW1lc3Rlci53ZWlnaHQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvcyA9IGN1cnJlbnRTZW1lc3RlciAqIGNmZy5zZW1lc3Rlci53ZWlnaHQ7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHN0YXJ0ZWROb3cpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2NzW3F1ZXVlW2ldXS5zdGFydCA9IHBvcztcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2NzW3F1ZXVlW2ldXS5zZW1lc3RlcldlaWdodCA9IHt9O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGRpc2NzW3F1ZXVlW2ldXS5wb3MgPSBwb3M7XG4gICAgICAgICAgICAgICAgICAgIGRpc2NzW3F1ZXVlW2ldXS53ZWlnaHQgLT0gMTtcbiAgICAgICAgICAgICAgICAgICAgdGFibGVbcXVldWVbaV1dW3Bvc10gPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICBkaXNjc1txdWV1ZVtpXV0uZW5kID0gcG9zICsgMTtcbiAgICAgICAgICAgICAgICAgICAgZGlzY3NbcXVldWVbaV1dLnNlbWVzdGVyV2VpZ2h0W2N1cnJlbnRTZW1lc3Rlcl0gPSAoZGlzY3NbcXVldWVbaV1dLnNlbWVzdGVyV2VpZ2h0W2N1cnJlbnRTZW1lc3Rlcl0gfHwgMCkgKyAxO1xuICAgICAgICAgICAgICAgICAgICBzZW1lc3RlcldlaWdodHNbY3VycmVudFNlbWVzdGVyXSsrO1xuICAgICAgICAgICAgICAgICAgICBjaGFuZ2UgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICBpZiAoc2VtZXN0ZXJXaWR0aCA8IGRpc2NzW3F1ZXVlW2ldXS5lbmQgLSBjdXJyZW50U2VtZXN0ZXIgKiBjZmcuc2VtZXN0ZXIud2VpZ2h0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZW1lc3RlcldpZHRoID0gZGlzY3NbcXVldWVbaV1dLmVuZCAtIGN1cnJlbnRTZW1lc3RlciAqIGNmZy5zZW1lc3Rlci53ZWlnaHQ7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKGRpc2NzW3F1ZXVlW2ldXS53ZWlnaHQgPT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhcnRlZC5zcGxpY2Uoc3RhcnRlZC5pbmRleE9mKHF1ZXVlW2ldKSwgMSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZGF0YVdlaWdodC0tO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICghY2hhbmdlKSB7XG4gICAgICAgICAgICAgICAgY3VycmVudFNlbWVzdGVyKys7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgdmFyIGRpZmYgPSBjZmcuc2VtZXN0ZXIud2VpZ2h0IC0gc2VtZXN0ZXJXaWR0aDtcbiAgICAgICAgZm9yICgvLyBjb25zb2xlLmxvZyhkaWZmKTtcbiAgICAgICAgICAgIHZhciBpID0gMDsgaSA8IHF1ZXVlLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGNmZy5zZW1lc3Rlci5jb3VudDsgaisrKSB7XG4gICAgICAgICAgICAgICAgdGFibGVbcXVldWVbaV1dLnNwbGljZSgoaiArIDEpICogc2VtZXN0ZXJXaWR0aCwgZGlmZik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBkaXNjc1txdWV1ZVtpXV0uc3RhcnQgLT0gKGRpc2NzW3F1ZXVlW2ldXS5zdGFydCAvIGNmZy5zZW1lc3Rlci53ZWlnaHQgfCAwKSAqIGRpZmY7XG4gICAgICAgICAgICBkaXNjc1txdWV1ZVtpXV0uc3RhcnQgLT0gZGlzY3NbcXVldWVbaV1dLnN0YXJ0ICUgc2VtZXN0ZXJXaWR0aDtcbiAgICAgICAgICAgIGRpc2NzW3F1ZXVlW2ldXS5lbmQgLT0gKGRpc2NzW3F1ZXVlW2ldXS5lbmQgLyBjZmcuc2VtZXN0ZXIud2VpZ2h0IHwgMCkgKiBkaWZmO1xuICAgICAgICAgICAgaWYgKGRpc2NzW3F1ZXVlW2ldXS5lbmQgJSBzZW1lc3RlcldpZHRoID4gMCkge1xuICAgICAgICAgICAgICAgIGRpc2NzW3F1ZXVlW2ldXS5lbmQgKz0gc2VtZXN0ZXJXaWR0aCAtIGRpc2NzW3F1ZXVlW2ldXS5lbmQgJSBzZW1lc3RlcldpZHRoO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZGlzY3NbcXVldWVbaV1dLndlaWdodCA9IGRhdGEuZGlzY2lwbGluZXNbcXVldWVbaV1dLndlaWdodCAqIDM7XG4gICAgICAgIH1cbiAgICAgICAgc2VtZXN0ZXJXZWlnaHRzLnNwbGljZShjZmcuc2VtZXN0ZXIuY291bnQsIGNmZy5zZW1lc3Rlci5jb3VudCk7XG4gICAgICAgIHNlbWVzdGVyRGlzY3Muc3BsaWNlKGNmZy5zZW1lc3Rlci5jb3VudCwgY2ZnLnNlbWVzdGVyLmNvdW50KTtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHRhYmxlOiB0YWJsZSxcbiAgICAgICAgICAgIGRpc2NzOiBkaXNjcyxcbiAgICAgICAgICAgIHdlaWdodHM6IHNlbWVzdGVyV2VpZ2h0cyxcbiAgICAgICAgICAgIGRpc2NDb3VudDogc2VtZXN0ZXJEaXNjcyxcbiAgICAgICAgICAgIHdpZHRoOiBzZW1lc3RlcldpZHRoXG4gICAgICAgIH07XG4gICAgICAgIGZ1bmN0aW9uIHBtYXgoaWQpIHtcbiAgICAgICAgICAgIHZhciBtYXhJRCA9IGNvbm5lY3Rpb25zLnRvW2lkXVswXTtcbiAgICAgICAgICAgIGZvciAodmFyIGkkMiA9IDE7IGkkMiA8IGNvbm5lY3Rpb25zLnRvW2lkXS5sZW5ndGg7IGkkMisrKSB7XG4gICAgICAgICAgICAgICAgdmFyIG5pZCA9IGNvbm5lY3Rpb25zLnRvW2lkXVtpJDJdO1xuICAgICAgICAgICAgICAgIGlmIChkaXNjc1ttYXhJRF0uZW5kIDwgZGlzY3NbbmlkXS5lbmQpIHtcbiAgICAgICAgICAgICAgICAgICAgbWF4SUQgPSBuaWQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHR5cGVvZiBtYXhJRCA9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgIHJldHVybiAwO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZGlzY3NbbWF4SURdLmVuZDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBmdW5jdGlvbiBjb21wYXJhdG9yKGEsIGIpIHtcbiAgICAgICAgICAgIHZhciB0byA9IGNvbm5lY3Rpb25zLnRvLCBmcm9tID0gY29ubmVjdGlvbnMuZnJvbTtcbiAgICAgICAgICAgIHZhciByID0gZnJvbVtiXS5pbmRleE9mKGEpID49IDAgPyAxIDogZnJvbVthXS5pbmRleE9mKGIpID49IDAgPyAtMSA6IHRvW2FdLmxlbmd0aCA+IHRvW2JdLmxlbmd0aCA/IDEgOiBmcm9tW2FdLmxlbmd0aCA+IGZyb21bYl0ubGVuZ3RoID8gLTEgOiAwO1xuICAgICAgICAgICAgcmV0dXJuIHI7XG4gICAgICAgIH1cbiAgICAgICAgZnVuY3Rpb24gc29ydChjbXApIHtcbiAgICAgICAgICAgIGZvciAodmFyIGkkMiA9IDAsIGwgPSB0aGlzLmxlbmd0aDsgaSQyIDwgbDsgaSQyKyspIHtcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBqJDIgPSAwOyBqJDIgPCBsOyBqJDIrKykge1xuICAgICAgICAgICAgICAgICAgICBpZiAoaSQyICE9PSBqJDIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjbXAodGhpc1tpJDJdLCB0aGlzW2okMl0pID09PSAtMSAmJiBpJDIgPiBqJDIgfHwgY21wKHRoaXNbaSQyXSwgdGhpc1tqJDJdKSA9PT0gMSAmJiBpJDIgPCBqJDIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdG1wID0gdGhpc1tpJDJdO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXNbaSQyXSA9IHRoaXNbaiQyXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzW2okMl0gPSB0bXA7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9O1xufTtcbmZ1bmN0aW9uIG5ld0FycmF5KGNvdW50LCBkZWYpIHtcbiAgICB2YXIgYXJyID0gbmV3IEFycmF5KGNvdW50KTtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNvdW50OyBpKyspIHtcbiAgICAgICAgYXJyW2ldID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShkZWYpKTtcbiAgICB9XG4gICAgcmV0dXJuIGFycjtcbn1cbmZ1bmN0aW9uIGFycmF5SW5BcnJheShvbmUsIHR3bykge1xuICAgIGlmIChvbmUubGVuZ3RoID09PSAwIHx8IHR3by5sZW5ndGggPT09IDApIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBpZiAob25lLmxlbmd0aCA8IHR3by5sZW5ndGgpIHtcbiAgICAgICAgdmFyIHRtcCA9IHR3bztcbiAgICAgICAgdHdvID0gb25lO1xuICAgICAgICBvbmUgPSB0bXA7XG4gICAgfVxuICAgIGZvciAodmFyIGkgPSAwLCBsID0gb25lLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgICAgICBpZiAodHdvLmluZGV4T2Yob25lW2ldKSA+PSAwKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG59IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGRpRnVuY3Rpb24gPSBmdW5jdGlvbiBIVFRQRXJyb3JIYW5kbGVyKCRxLCAkbG9nLCAkaW5qZWN0b3IpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICdyZXNwb25zZUVycm9yJzogZnVuY3Rpb24gKHJlamVjdGlvbikge1xuICAgICAgICAgICAgICAgIGlmICgvLyBub3RpZmljYXRpb25zLmFkZCh7XG4gICAgICAgICAgICAgICAgICAgIC8vICAgJ3RpdGxlJzogJ0hUVFAgJyArIHJlamVjdGlvbi5jb25maWcubWV0aG9kICsgJyBFcnJvcjonLFxuICAgICAgICAgICAgICAgICAgICAvLyAgICd0eXBlJzogJ2Vycm9yJyxcbiAgICAgICAgICAgICAgICAgICAgLy8gICAnY29udGVudCc6IHJlamVjdGlvbi5jb25maWcudXJsICsgJ1xcbidcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICsgcmVqZWN0aW9uLnN0YXR1cyArICc6ICcgKyByZWplY3Rpb24uc3RhdHVzVGV4dCxcbiAgICAgICAgICAgICAgICAgICAgLy8gfSk7XG4gICAgICAgICAgICAgICAgICAgIHJlamVjdGlvbi5zdGF0dXMgPT0gNDAzKSB7XG4gICAgICAgICAgICAgICAgICAgICRpbmplY3Rvci5nZXQoJyRzdGF0ZScpLmdvKCdhdXRoJywge30sIHsgbG9jYXRpb246ICdyZXBsYWNlJyB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuICRxLnJlamVjdChyZWplY3Rpb24pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH07XG4gICAgZGlGdW5jdGlvbi4kaW5qZWN0ID0gW1xuICAgICAgICAnJHEnLFxuICAgICAgICAnJGxvZycsXG4gICAgICAgICckaW5qZWN0b3InXG4gICAgXTtcbiAgICByZXR1cm4gZGlGdW5jdGlvbjtcbn0oKTsiLCJjbGFzcyBBdXRoQ3RybCB7XG4gICAgY29uc3RydWN0b3IoJHJvb3RTY29wZSwgJHNjb3BlLCAkaHR0cCwgJGxvY2F0aW9uLCAkc3RhdGUsICRjb29raWVzKSB7XG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgICAgdGhpcy5zdWJtaXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkaHR0cCh7XG4gICAgICAgICAgICAgICAgbWV0aG9kOiAncG9zdCcsXG4gICAgICAgICAgICAgICAgdXJsOiAnL3Byb2ZpbGUvYXV0aC9sb2dpbi8nLFxuICAgICAgICAgICAgICAgIGRhdGE6IHNlbGYubG9naW5EYXRhLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgJ1gtQ1NSRlRva2VuJzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRjb29raWVzWydjc3JmdG9rZW4nXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEsIHN0YXR1cywgaGVhZGVycywgY29uZmlnKSB7XG4gICAgICAgICAgICAgICAgJHJvb3RTY29wZS5jdXJyZW50VXNlciA9IGRhdGE7XG4gICAgICAgICAgICAgICAgJHN0YXRlLmdvKCdzcGVjaWFsdGllcycsIHt9LCB7IGxvY2F0aW9uOiAncmVwbGFjZScgfSk7XG4gICAgICAgICAgICB9KS5lcnJvcihmdW5jdGlvbiAoZGF0YSwgc3RhdHVzLCBoZWFkZXJzLCBjb25maWcpIHtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmxvZ2luRGF0YSA9IHtcbiAgICAgICAgICAgIHVzZXJuYW1lOiAnJyxcbiAgICAgICAgICAgIHBhc3N3b3JkOiAnJ1xuICAgICAgICB9O1xuICAgICAgICAkc2NvcGUuQXV0aEN0cmwgPSB0aGlzO1xuICAgIH1cbiAgICBzdGF0aWMgcmVzb2x2ZXIoKSB7XG4gICAgICAgIHJldHVybiB7fTtcbiAgICB9XG59XG5BdXRoQ3RybC4kaW5qZWN0ID0gW1xuICAgICckcm9vdFNjb3BlJyxcbiAgICAnJHNjb3BlJyxcbiAgICAnJGh0dHAnLFxuICAgICckbG9jYXRpb24nLFxuICAgICckc3RhdGUnLFxuICAgICckY29va2llcydcbl07XG5tb2R1bGUuZXhwb3J0cyA9IEF1dGhDdHJsOyIsImFuZ3VsYXIubW9kdWxlKCdhdXRoJywgWyd1aS5yb3V0ZXInXSkuY29uZmlnKHJlcXVpcmUoJy4vcm91dGVzJykpLnJ1bihmdW5jdGlvbiAoJHJvb3RTY29wZSwgJGluamVjdG9yKSB7XG4gICAgLy8gY29uc29sZS5sb2cod2luZG93LmxvY2F0aW9uLm9yaWdpbiArICcvYXBpL3YxL3VzZXInKTtcbiAgICAkaW5qZWN0b3IuZ2V0KCckaHR0cCcpKHtcbiAgICAgICAgbWV0aG9kOiAnZ2V0JyxcbiAgICAgICAgdXJsOiAnL2FwaS92MS91c2VyJyxcbiAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgJ1gtQ1NSRlRva2VuJzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiAkaW5qZWN0b3IuZ2V0KCckY29va2llcycpWydjc3JmdG9rZW4nXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEsIHN0YXR1cywgaGVhZGVycywgY29uZmlnKSB7XG4gICAgICAgICRyb290U2NvcGUuY3VycmVudFVzZXIgPSBkYXRhO1xuICAgIH0pO1xuICAgICRyb290U2NvcGUuJG9uKCckc3RhdGVDaGFuZ2VTdGFydCcsIGZ1bmN0aW9uIChldmVudCwgdG9TdGF0ZSwgdG9QYXJhbXMpIHtcbiAgICAgICAgdmFyIHJlcXVpcmVMb2dpbiA9ICh0b1N0YXRlLmRhdGEgfHwge30pLnJlcXVpcmVMb2dpbjtcbiAgICAgICAgaWYgKHJlcXVpcmVMb2dpbiAmJiB0eXBlb2YgJHJvb3RTY29wZS5jdXJyZW50VXNlciA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAkaW5qZWN0b3IuZ2V0KCckc3RhdGUnKS5nbygnYXV0aCcsIHt9LCB7IGxvY2F0aW9uOiAncmVwbGFjZScgfSk7XG4gICAgICAgIH1cbiAgICB9KTtcbn0pOyIsInZhciBBdXRoQ3RybCA9IHJlcXVpcmUoJy4vQXV0aEN0cmwuanMnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBkaUZ1bmN0aW9uID0gZnVuY3Rpb24gKCRzdGF0ZVByb3ZpZGVyKSB7XG4gICAgICAgICRzdGF0ZVByb3ZpZGVyLnN0YXRlKCdhdXRoJywge1xuICAgICAgICAgICAgdXJsOiAnL2F1dGgvbG9naW4nLFxuICAgICAgICAgICAgdmlld3M6IHtcbiAgICAgICAgICAgICAgICAnYm9keUAnOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6IEF1dGhDdHJsLFxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlOiBBdXRoQ3RybC5yZXNvbHZlcigpLFxuICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9zL2h0bWwvdGVtcGxhdGVzL2xvZ2luLmh0bWwnXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9O1xuICAgIGRpRnVuY3Rpb24uJGluamVjdCA9IFsnJHN0YXRlUHJvdmlkZXInXTtcbiAgICByZXR1cm4gZGlGdW5jdGlvbjtcbn0oKTsiLCJ2YXIgZGF0YSA9IG1vZHVsZS5leHBvcnRzID0ge1xuICAgIHNlbWVzdGVyOiB7XG4gICAgICAgIGNvdW50OiA4LFxuICAgICAgICB3ZWlnaHQ6IDEwLFxuICAgICAgICBsaW1pdDogOFxuICAgIH0sXG4gICAgZGlzY2lwbGluZTogeyBjb3VudDogNTIgfSxcbiAgICBjb25uZWN0aW9uczogNDAsXG4gICAgc3BlY2lhbHR5OiB7IGNvdW50OiA1IH1cbn07IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGRpRnVuY3Rpb24gPSBmdW5jdGlvbiAoJHdpbmRvdykge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHMpIHtcbiAgICAgICAgICAgICAgICAoZnVuY3Rpb24gKCRlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICRlbGVtZW50Lm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICR3aW5kb3cuaGlzdG9yeS5iYWNrKCk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0uYXBwbHkocywgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH07XG4gICAgZGlGdW5jdGlvbi4kaW5qZWN0ID0gWyckd2luZG93J107XG4gICAgcmV0dXJuIGRpRnVuY3Rpb247XG59KCk7IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGRpRnVuY3Rpb24gPSBmdW5jdGlvbiAoJGRvY3VtZW50KSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXF1aXJlOiAnXmludGVyYWN0UGFyZW50JyxcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzKSB7XG4gICAgICAgICAgICAgICAgKGZ1bmN0aW9uICgkZWxlbWVudCwgJGF0dHJzLCBjdHJsKSB7XG4gICAgICAgICAgICAgICAgICAgIGN0cmwub2JqZWN0ID0gJGVsZW1lbnQ7XG4gICAgICAgICAgICAgICAgfS5hcHBseShzLCBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfTtcbiAgICBkaUZ1bmN0aW9uLiRpbmplY3QgPSBbJyRkb2N1bWVudCddO1xuICAgIHJldHVybiBkaUZ1bmN0aW9uO1xufSgpOyIsImZ1bmN0aW9uIHRyYW5zZm9ybU1hdHJpeCh0cmFuc2Zvcm1TdHJpbmcpIHtcbiAgICBpZiAodHJhbnNmb3JtU3RyaW5nICE9PSAnbm9uZScpIHtcbiAgICAgICAgdmFyIGFyciA9IHRyYW5zZm9ybVN0cmluZy5yZXBsYWNlKCdtYXRyaXgoJywgJycpLnJlcGxhY2UoJyknLCAnJykuc3BsaXQoJywgJykubWFwKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICByZXR1cm4gcGFyc2VGbG9hdChlKTtcbiAgICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIGFyciA9IFtcbiAgICAgICAgICAgIDEsXG4gICAgICAgICAgICAwLFxuICAgICAgICAgICAgMCxcbiAgICAgICAgICAgIDEsXG4gICAgICAgICAgICAwLFxuICAgICAgICAgICAgMFxuICAgICAgICBdO1xuICAgIH1cbiAgICByZXR1cm4gYXJyO1xufVxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGRpRnVuY3Rpb24gPSBmdW5jdGlvbiAoJGRvY3VtZW50KSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjb250cm9sbGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgICAgICAgICAgIHRoaXMuem9vbSA9IHpvb207XG4gICAgICAgICAgICAgICAgdGhpcy5tb3VzZWRvd24gPSBtb3VzZWRvd247XG4gICAgICAgICAgICAgICAgdGhpcy5tb3VzZW1vdmUgPSBtb3VzZW1vdmU7XG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gem9vbShldmVudCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWV2ZW50LmN0cmxLZXkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzY2FsZUZhY3RvciA9IDAuMDUsIGQgPSBldmVudC5kZWx0YVk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgbWF0cml4ID0gdHJhbnNmb3JtTWF0cml4KHNlbGYub2JqZWN0LmNzcygndHJhbnNmb3JtJykpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHNjYWxlID0gbWF0cml4WzBdO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9mZnNldCA9IHNlbGYub2JqZWN0Lm9mZnNldCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZCA9IGQgPiAwICYmIHNjYWxlIDwgMiA/IGQgOiBkIDwgMCAmJiBzY2FsZSA+IDAuNCA/IGQgOiAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWF0cml4WzBdID0gc2NhbGUgKyBzY2FsZUZhY3RvciAqIGQ7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXRyaXhbM10gPSBtYXRyaXhbMF07XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgb3JpZ2luID0gKGV2ZW50LmNsaWVudFggLSBvZmZzZXQubGVmdCB8IDApLnRvU3RyaW5nKCkgKyAncHggJyArIChldmVudC5jbGllbnRZIC0gb2Zmc2V0LnRvcCB8IDApLnRvU3RyaW5nKCkgKyAncHgnO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGQgIT09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLm9iamVjdC5jc3Moe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAndHJhbnNmb3JtLW9yaWdpbic6IG9yaWdpbixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJy13ZWJraXQtdHJhbnNmb3JtLW9yaWdpbic6IG9yaWdpblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYub2JqZWN0LmNzcygndHJhbnNmb3JtJywgJ21hdHJpeCgnICsgbWF0cml4LmpvaW4oJywnKSArICcpJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gbW91c2Vkb3duKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYubGFzdCA9IGV2ZW50O1xuICAgICAgICAgICAgICAgICAgICAkZG9jdW1lbnQub24oJ21vdXNlbW92ZScsIG1vdXNlbW92ZSk7XG4gICAgICAgICAgICAgICAgICAgICRkb2N1bWVudC5vbignbW91c2V1cCcsIG1vdXNldXApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBtb3VzZW1vdmUoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIG1hdHJpeCA9IHRyYW5zZm9ybU1hdHJpeChzZWxmLm9iamVjdC5jc3MoJ3RyYW5zZm9ybScpKTtcbiAgICAgICAgICAgICAgICAgICAgbWF0cml4WzRdIC09IHNlbGYubGFzdC5jbGllbnRYIC0gZXZlbnQuY2xpZW50WDtcbiAgICAgICAgICAgICAgICAgICAgbWF0cml4WzVdIC09IHNlbGYubGFzdC5jbGllbnRZIC0gZXZlbnQuY2xpZW50WTtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5vYmplY3QuY3NzKCd0cmFuc2Zvcm0nLCAnbWF0cml4KCcgKyBtYXRyaXguam9pbignLCcpICsgJyknKTtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5sYXN0ID0gZXZlbnQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIG1vdXNldXAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgJGRvY3VtZW50Lm9mZignbW91c2Vtb3ZlJywgbW91c2Vtb3ZlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHMpIHtcbiAgICAgICAgICAgICAgICAoZnVuY3Rpb24gKCRlbGVtZW50LCAkYXR0cnMsIGN0cmwpIHtcbiAgICAgICAgICAgICAgICAgICAgY3RybC5wYXJlbnQgPSAkZWxlbWVudDtcbiAgICAgICAgICAgICAgICAgICAgJGVsZW1lbnQubW91c2V3aGVlbChjdHJsLnpvb20pO1xuICAgICAgICAgICAgICAgICAgICAkZWxlbWVudC5vbignbW91c2Vkb3duJywgY3RybC5tb3VzZWRvd24pO1xuICAgICAgICAgICAgICAgIH0uYXBwbHkocywgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH07XG4gICAgZGlGdW5jdGlvbi4kaW5qZWN0ID0gWyckZG9jdW1lbnQnXTtcbiAgICByZXR1cm4gZGlGdW5jdGlvbjtcbn0oKTsiLCJ2YXIgYXBwID0gbW9kdWxlLmV4cG9ydHMgPSBhbmd1bGFyLm1vZHVsZSgnaW50ZXJhY3QnLCBbXSkuZGlyZWN0aXZlKCdpbnRlcmFjdFBhcmVudCcsIHJlcXVpcmUoJy4vZGlyZWN0aXZlcy9pbnRlcmFjdFBhcmVudC5qcycpKS5kaXJlY3RpdmUoJ2ludGVyYWN0T2JqZWN0JywgcmVxdWlyZSgnLi9kaXJlY3RpdmVzL2ludGVyYWN0T2JqZWN0LmpzJykpOyIsImNsYXNzIFNwZWNpYWx0aWVzTGlzdEN0cmwge1xuICAgIGNvbnN0cnVjdG9yKCRzY29wZSwgZGF0YSkge1xuICAgICAgICAkc2NvcGUuc3BlY3MgPSBkYXRhLnJlc3VsdHM7XG4gICAgfVxuICAgIHN0YXRpYyByZXNvbHZlcigpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGRhdGE6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB2YXIgZGlGdW5jdGlvbiA9IGZ1bmN0aW9uICgkc3RhdGUsICRzdGF0ZVBhcmFtcywgQXBpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBBcGkuc3BlY2lhbGl6YXRpb24ucXVlcnkoJHN0YXRlUGFyYW1zKS4kcHJvbWlzZTtcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIGRpRnVuY3Rpb24uJGluamVjdCA9IFtcbiAgICAgICAgICAgICAgICAgICAgJyRzdGF0ZScsXG4gICAgICAgICAgICAgICAgICAgICckc3RhdGVQYXJhbXMnLFxuICAgICAgICAgICAgICAgICAgICAnQXBpJ1xuICAgICAgICAgICAgICAgIF07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGRpRnVuY3Rpb247XG4gICAgICAgICAgICB9KClcbiAgICAgICAgfTtcbiAgICB9XG59XG5TcGVjaWFsdGllc0xpc3RDdHJsLiRpbmplY3QgPSBbXG4gICAgJyRzY29wZScsXG4gICAgJ2RhdGEnXG5dO1xubW9kdWxlLmV4cG9ydHMgPSBTcGVjaWFsdGllc0xpc3RDdHJsOyIsInZhciBjZmcgPSByZXF1aXJlKCcuLi8uLi9jb25maWcnKTtcbmNsYXNzIFNwZWNpYWx0eUN0cmwge1xuICAgIGNvbnN0cnVjdG9yKCRzY29wZSwgZGF0YSwgRGF0YVNvcnQsIERhdGFDb252ZXJ0KSB7XG4gICAgICAgIHRoaXMucyA9ICRzY29wZTtcbiAgICAgICAgdGhpcy5zLnNwZWMgPSBkYXRhO1xuICAgICAgICB0aGlzLnMuc3BlY2lhbHR5Q3RybCA9IHRoaXM7XG4gICAgICAgIHZhciBjb252ZXJ0ZWREYXRhID0gLy8gdmFyIGNvbnZlcnRlZERhdGEgPSBEYXRhQ29udmVydChkYXRhLnJlbGF0aW9ucyk7XG4gICAgICAgIERhdGFDb252ZXJ0KFtdLmNvbmNhdChkYXRhLnNwZWNpYWxpdHkucmVsYXRpb25zLCBkYXRhLnJlbGF0aW9ucykpO1xuICAgICAgICB0aGlzLmNvbm5lY3Rpb25zID0gY29udmVydGVkRGF0YS5jb25uZWN0aW9ucztcbiAgICAgICAgdGhpcy5kaXNjaXBsaW5lcyA9IGNvbnZlcnRlZERhdGEuZGlzY2lwbGluZXM7XG4gICAgICAgIHRoaXMuY2F0ZWdvcmllcyA9IGNvbnZlcnRlZERhdGEuY2F0ZWdvcmllcztcbiAgICAgICAgZm9yICh2YXIgaSBpbiB0aGlzLmNhdGVnb3JpZXMpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5jYXRlZ29yaWVzLmhhc093blByb3BlcnR5KGkpKVxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgdGhpcy5jYXRlZ29yaWVzW2ldLmxpc3QgPSBbXTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnNvcnRlZCA9IERhdGFTb3J0KGNvbnZlcnRlZERhdGEsIGNmZyk7XG4gICAgICAgIHZhciBkZWZhdWx0Q2F0ZWdvcnkgPSAodGhpcy5jYXRlZ29yaWVzW09iamVjdC5rZXlzKHRoaXMuY2F0ZWdvcmllcylbMF1dIHx8IHt9KS5pZDtcbiAgICAgICAgZm9yICh2YXIgaSBpbiB0aGlzLnNvcnRlZC5kaXNjcykge1xuICAgICAgICAgICAgaWYgKCF0aGlzLnNvcnRlZC5kaXNjcy5oYXNPd25Qcm9wZXJ0eShpKSlcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgIHRoaXMuY2F0ZWdvcmllc1t0aGlzLnNvcnRlZC5kaXNjc1tpXS5jYXRlZ29yeS5pZCB8fCBkZWZhdWx0Q2F0ZWdvcnldLmxpc3QucHVzaCh0aGlzLnNvcnRlZC5kaXNjc1tpXS5pZCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgc3RhdGljIHJlc29sdmVyKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgLy8gc3BlYzogZnVuY3Rpb24oJHN0YXRlLCAkc3RhdGVQYXJhbXMsIERhdGEpIHtcbiAgICAgICAgICAgIC8vICAgcmV0dXJuIERhdGEuZ2V0U3BlY2lhbHR5KCRzdGF0ZVBhcmFtcy5pZCk7XG4gICAgICAgICAgICAvLyB9LFxuICAgICAgICAgICAgZGF0YTogZnVuY3Rpb24gKCRzdGF0ZSwgJHN0YXRlUGFyYW1zLCBBcGkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gQXBpLnNwZWNpYWxpemF0aW9uLmdldCgkc3RhdGVQYXJhbXMpLiRwcm9taXNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH1cbn1cblNwZWNpYWx0eUN0cmwuJGluamVjdCA9IFtcbiAgICAnJHNjb3BlJyxcbiAgICAnZGF0YScsXG4gICAgJ0RhdGFTb3J0JyxcbiAgICAnRGF0YUNvbnZlcnQnXG5dO1xubW9kdWxlLmV4cG9ydHMgPSBTcGVjaWFsdHlDdHJsOyIsImNsYXNzIFNwZWNpYWx0eURMaXN0Q3RybCB7XG4gICAgY29uc3RydWN0b3IoJHNjb3BlKSB7XG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgICAgdGhpcy5zID0gJHNjb3BlO1xuICAgICAgICBpZiAoIXRoaXMucy5zcGVjaWFsdHlDdHJsKVxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB0aGlzLmNvbm5lY3Rpb25zID0gdGhpcy5zLnNwZWNpYWx0eUN0cmwuY29ubmVjdGlvbnM7XG4gICAgICAgIHRoaXMucy5zcGVjaWFsdHlDdHJsLmRyYXdlciA9IHRoaXM7XG4gICAgICAgIHRoaXMuY29uZmlnID0ge1xuICAgICAgICAgICAgb2Zmc2V0OiAxNSxcbiAgICAgICAgICAgIHZvZmZzZXQ6IDYsXG4gICAgICAgICAgICBjdXJyZW50OiAwLFxuICAgICAgICAgICAgYXBwZW5kOiA2XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuY2FudmFzID0ge307XG4gICAgICAgIHRoaXMuaXRlbXNEcmF3biA9IGZhbHNlO1xuICAgICAgICB0aGlzLml0ZW1zTGVmdCA9IE9iamVjdC5rZXlzKHRoaXMucy5zcGVjaWFsdHlDdHJsLnNvcnRlZC5kaXNjcyk7XG4gICAgICAgIHRoaXMuZmxvdyA9IE9iamVjdC5rZXlzKHRoaXMucy5zcGVjaWFsdHlDdHJsLnNvcnRlZC5kaXNjcyk7XG4gICAgICAgIHRoaXMuaXRlbXMgPSAvLyBjb25zb2xlLmxvZyhAZmxvdyk7XG4gICAgICAgIHt9O1xuICAgIH1cbiAgICBjbGVhcigpIHtcbiAgICAgICAgdGhpcy5jb25maWcuY3VycmVudCA9IDA7XG4gICAgICAgIHRoaXMuaXRlbXNEcmF3biA9IGZhbHNlO1xuICAgICAgICBpZiAodHlwZW9mIHRoaXMuY2FudmFzLnBhcGVyICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgdGhpcy5jYW52YXMucGFwZXIuY2FudmFzLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5jYW52YXMucGFwZXIuY2FudmFzKTtcbiAgICAgICAgfVxuICAgICAgICBkZWxldGUgdGhpcy5jYW52YXMucGFwZXI7XG4gICAgfVxuICAgIGRyYXcoKSB7XG4gICAgICAgIGlmICghdGhpcy5pdGVtc0RyYXduICYmICh0aGlzLml0ZW1zTGVmdC5sZW5ndGggPT09IDAgJiYgISF0aGlzLmNhbnZhcy53cmFwcGVyKSkge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiB0aGlzLmNhbnZhcy5wYXBlciAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNsZWFyKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLmNhbnZhcy53cmFwcGVyLmhlaWdodCh0aGlzLmNhbnZhcy53cmFwcGVyLnBhcmVudCgpLmhlaWdodCgpKTtcbiAgICAgICAgICAgIHRoaXMuY2FudmFzLnBhcGVyID0gbmV3IFJhcGhhZWwodGhpcy5jYW52YXMud3JhcHBlclswXSk7XG4gICAgICAgICAgICB0aGlzLml0ZW1zRHJhd24gPSAvLyB2YXIgZmxvdyA9IEBmbG93O1xuICAgICAgICAgICAgdHJ1ZTtcbiAgICAgICAgICAgIGZvciAodmFyIF9pID0gMCwgX2wgPSB0aGlzLmZsb3cubGVuZ3RoOyBfaSA8IF9sOyBfaSsrKSB7XG4gICAgICAgICAgICAgICAgdmFyIGkgPSB0aGlzLmZsb3dbX2ldO1xuICAgICAgICAgICAgICAgIGZvciAodmFyIF9pJDIgPSAwLCBfbCQyID0gdGhpcy5jb25uZWN0aW9ucy50b1tpXS5sZW5ndGg7IF9pJDIgPCBfbCQyOyBfaSQyKyspIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGogPSB0aGlzLmNvbm5lY3Rpb25zLnRvW2ldW19pJDJdO1xuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5mbG93LmluZGV4T2YoJycgKyBqKSA+PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRyYXdDb25uZWN0aW9uRGVmZmVyZWQoaiwgaSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgZHJhd0Nvbm5lY3Rpb25EZWZmZXJlZChpLCBqKSB7XG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBzZWxmLmRyYXdDb25uZWN0aW9uKGksIGopO1xuICAgICAgICB9LCAxMCk7XG4gICAgfVxuICAgIGRyYXdDb25uZWN0aW9uKGEsIGIpIHtcbiAgICAgICAgdmFyICRmcm9tID0gJCh0aGlzLml0ZW1zW2FdWzBdKTtcbiAgICAgICAgdmFyICR0byA9ICQodGhpcy5pdGVtc1tiXVswXSk7XG4gICAgICAgIHZhciBvZmZzZXQgPSB0aGlzLmNhbnZhcy53cmFwcGVyLm9mZnNldCgpLCBmcm9tID0ge1xuICAgICAgICAgICAgICAgIG9mZnNldDogJGZyb20ub2Zmc2V0KCksXG4gICAgICAgICAgICAgICAgd2lkdGg6ICRmcm9tLndpZHRoKCksXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAkZnJvbS5vdXRlckhlaWdodChmYWxzZSlcbiAgICAgICAgICAgIH0sIHRvID0ge1xuICAgICAgICAgICAgICAgIG9mZnNldDogJHRvLm9mZnNldCgpLFxuICAgICAgICAgICAgICAgIHdpZHRoOiAkdG8ud2lkdGgoKSxcbiAgICAgICAgICAgICAgICBoZWlnaHQ6ICR0by5vdXRlckhlaWdodChmYWxzZSlcbiAgICAgICAgICAgIH07XG4gICAgICAgIHZhciBkaXJlY3Rpb24gPSBmcm9tLm9mZnNldC5sZWZ0IDwgdG8ub2Zmc2V0LmxlZnQsIHRtcDtcbiAgICAgICAgdmFyIC8vIGlmKGRpcmVjdGlvbikge1xuICAgICAgICAvLyAgIHRtcCA9IGZyb207XG4gICAgICAgIC8vICAgZnJvbSA9IHRvO1xuICAgICAgICAvLyAgIHRvID0gdG1wO1xuICAgICAgICAvLyB9XG4gICAgICAgIGRyYXcgPSB7XG4gICAgICAgICAgICBmcm9tOiB7XG4gICAgICAgICAgICAgICAgeDogZnJvbS5vZmZzZXQubGVmdCAtIG9mZnNldC5sZWZ0ICsgdGhpcy5jb25maWcub2Zmc2V0ICsgdGhpcy5jb25maWcudm9mZnNldCxcbiAgICAgICAgICAgICAgICB5OiBmcm9tLm9mZnNldC50b3AgLSBvZmZzZXQudG9wICsgZnJvbS5oZWlnaHRcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0bzoge1xuICAgICAgICAgICAgICAgIHg6IHRvLm9mZnNldC5sZWZ0IC0gb2Zmc2V0LmxlZnQgKyB0aGlzLmNvbmZpZy5vZmZzZXQsXG4gICAgICAgICAgICAgICAgeTogdG8ub2Zmc2V0LnRvcCAtIG9mZnNldC50b3AgKyB0by5oZWlnaHRcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdmFyIGNvbG9ycyA9IFtcbiAgICAgICAgICAgICcjYmJiJyxcbiAgICAgICAgICAgICcjY2M3JyxcbiAgICAgICAgICAgICcjYzc3JyxcbiAgICAgICAgICAgICcjN2M3JyxcbiAgICAgICAgICAgICcjN2NjJyxcbiAgICAgICAgICAgICcjYzdjJyxcbiAgICAgICAgICAgICcjN2NjJ1xuICAgICAgICBdO1xuICAgICAgICB0aGlzLmRyYXdBcnJvd1BhdGgoZHJhdy5mcm9tLngsIGRyYXcuZnJvbS55LCBkcmF3LnRvLngsIGRyYXcudG8ueSwgNSwgZGlyZWN0aW9uKS5hdHRyKHtcbiAgICAgICAgICAgICdzdHJva2UnOiBjb2xvcnNbdGhpcy5jb25maWcuY3VycmVudCAlIGNvbG9ycy5sZW5ndGhdLFxuICAgICAgICAgICAgJ3N0cm9rZS13aWR0aCc6IDFcbiAgICAgICAgfSkudG9CYWNrKCk7XG4gICAgfVxuICAgIGRyYXdBcnJvd1BhdGgoeDEsIHkxLCB4MiwgeTIsIGFycm93U2l6ZSwgZGlyKSB7XG4gICAgICAgIGlmICh0eXBlb2YgYXJyb3dTaXplID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgYXJyb3dTaXplID0gMDtcbiAgICAgICAgfVxuICAgICAgICBpZiAodHlwZW9mIGRpciA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIGRpciA9IHgxIDwgeDI7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIGFycm93UGF0aCA9ICcnLCBhcnJvd0RpciA9IGRpciA+IDAgPyAtMSA6IDE7XG4gICAgICAgIGlmICh4MSA8IHgyKSB7XG4gICAgICAgICAgICB2YXIgdHggPSB4MSwgdHkgPSB5MTtcbiAgICAgICAgICAgIHgxID0geDI7XG4gICAgICAgICAgICB4MiA9IHR4O1xuICAgICAgICAgICAgeTEgPSB5MjtcbiAgICAgICAgICAgIHkyID0gdHk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5jb25maWcuY3VycmVudCArPSB0aGlzLmNvbmZpZy5hcHBlbmQ7XG4gICAgICAgIHZhciBvID0gMCAtIHRoaXMuY29uZmlnLmN1cnJlbnQgLSB0aGlzLmNvbmZpZy5vZmZzZXQ7XG4gICAgICAgIHZhciBmID0gZGlyID4gMCA/IDggOiAxMSwgdCA9IGRpciA8PSAwID8gOCA6IDExO1xuICAgICAgICBpZiAoYXJyb3dTaXplKSB7XG4gICAgICAgICAgICB2YXIgeCA9IHgyLCB5ID0geTI7XG4gICAgICAgICAgICBpZiAoYXJyb3dEaXIgPCAwKSB7XG4gICAgICAgICAgICAgICAgeCA9IHgxO1xuICAgICAgICAgICAgICAgIHkgPSB5MTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGFycm93UGF0aCA9ICdNJyArICh4IC0gYXJyb3dTaXplKSArICcgJyArICh5ICsgYXJyb3dTaXplKSArICdMJyArIHggKyAnICcgKyB5ICsgJ0wnICsgKHggKyBhcnJvd1NpemUpICsgJyAnICsgKHkgKyBhcnJvd1NpemUpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLmNhbnZhcy5wYXBlci5wYXRoKCdNJyArIHgxICsgJyAnICsgeTEgKyAnTCcgKyB4MSArICcgJyArICh5MSArIGYpICsgJ0wnICsgKHgxICsgbykgKyAnICcgKyAoeTEgKyBmKSArICdMJyArICh4MSArIG8pICsgJyAnICsgKHkyICsgdCkgKyAnTCcgKyB4MiArICcgJyArICh5MiArIHQpICsgJ0wnICsgeDIgKyAnICcgKyB5MiArIGFycm93UGF0aCkudG9CYWNrKCk7XG4gICAgfVxuICAgIC8qKlxyXG4gICAqIExpbmtpbmcgc29tZSBlbGVtZW50IHRvIHRoZSBkaXNjaXBsaW5lIGlkLlxyXG4gICAqL1xuICAgIHNldERpc2NpcGxpbmVJdGVtKGlkLCAkZWxlbWVudCkge1xuICAgICAgICB2YXIgc2VsZiA9IHRoaXMsIGluZGV4ID0gdGhpcy5pdGVtc0xlZnQuaW5kZXhPZihpZCk7XG4gICAgICAgIGlmIChpbmRleCA+PSAwKSB7XG4gICAgICAgICAgICB0aGlzLml0ZW1zTGVmdC5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuaXRlbXNbaWRdID0gJGVsZW1lbnQ7XG4gICAgICAgICRlbGVtZW50Lm9uKCckZGVzdHJveScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGRlbGV0ZSBzZWxmLml0ZW1zW2lkXTtcbiAgICAgICAgICAgIHNlbGYuZHJhdygpO1xuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5kcmF3KCk7XG4gICAgfVxuICAgIC8qKlxyXG4gICAqIExpbmtpbmcgdGhlIGNhbnZhcy5cclxuICAgKi9cbiAgICBzZXRDYW52YXMoJGVsZW1lbnQpIHtcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgICB0aGlzLmNhbnZhcy53cmFwcGVyID0gJGVsZW1lbnQ7XG4gICAgICAgICRlbGVtZW50Lm9uKCckZGVzdHJveScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGRlbGV0ZSBzZWxmLmNhbnZhcy53cmFwcGVyO1xuICAgICAgICAgICAgZGVsZXRlIHNlbGYuY2FudmFzLnBhcGVyO1xuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5kcmF3KCk7XG4gICAgfVxufVxuU3BlY2lhbHR5RExpc3RDdHJsLiRpbmplY3QgPSBbJyRzY29wZSddO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgY29udHJvbGxlcjogU3BlY2lhbHR5RExpc3RDdHJsLFxuICAgICAgICBsaW5rOiBmdW5jdGlvbiAocykge1xuICAgICAgICAgICAgKGZ1bmN0aW9uICgkZWxlbWVudCwgJGF0dHJzKSB7XG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLnNwZWNpYWx0eUN0cmwpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH0uYXBwbHkocywgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSkpO1xuICAgICAgICB9XG4gICAgfTtcbn07IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgcmVxdWlyZTogJ15zcGVjaWFsdHlETGlzdCcsXG4gICAgICAgIGxpbms6IGZ1bmN0aW9uIChzKSB7XG4gICAgICAgICAgICAoZnVuY3Rpb24gKCRlbGVtZW50LCAkYXR0cnMsIGN0cmwpIHtcbiAgICAgICAgICAgICAgICBpZiAoIWN0cmwpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICBjdHJsLnNldERpc2NpcGxpbmVJdGVtKCRhdHRycy5zcGVjaWFsdHlETGlzdEl0ZW0sICRlbGVtZW50KTtcbiAgICAgICAgICAgIH0uYXBwbHkocywgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSkpO1xuICAgICAgICB9XG4gICAgfTtcbn07IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgcmVxdWlyZTogJ15zcGVjaWFsdHlETGlzdCcsXG4gICAgICAgIGxpbms6IGZ1bmN0aW9uIChzKSB7XG4gICAgICAgICAgICAoZnVuY3Rpb24gKCRlbGVtZW50LCAkYXR0cnMsIGN0cmwpIHtcbiAgICAgICAgICAgICAgICBpZiAoIWN0cmwpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAkZWxlbWVudC5oZWlnaHQoJGVsZW1lbnQucGFyZW50KCkuaGVpZ2h0KCkpO1xuICAgICAgICAgICAgICAgIGN0cmwuc2V0Q2FudmFzKCRlbGVtZW50KTtcbiAgICAgICAgICAgIH0uYXBwbHkocywgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSkpO1xuICAgICAgICB9XG4gICAgfTtcbn07IiwicmVxdWlyZSgnLi4vLi4vaW50ZXJhY3QvbW9kdWxlJyk7XG5hbmd1bGFyLm1vZHVsZSgncGFnZXMuc3BlY2lhbHRpZXMnLCBbXG4gICAgJ3VpLnJvdXRlcicsXG4gICAgJ2ludGVyYWN0J1xuXSkuY29uZmlnKHJlcXVpcmUoJy4vcm91dGVzJykpLmRpcmVjdGl2ZSgnc3BlY2lhbHR5RExpc3QnLCByZXF1aXJlKCcuL2RpcmVjdGl2ZXMvc3BlY2lhbHR5RExpc3QuanMnKSkuZGlyZWN0aXZlKCdzcGVjaWFsdHlETGlzdEl0ZW0nLCByZXF1aXJlKCcuL2RpcmVjdGl2ZXMvc3BlY2lhbHR5RExpc3RJdGVtLmpzJykpLmRpcmVjdGl2ZSgnc3BlY2lhbHR5RExpc3RNYXAnLCByZXF1aXJlKCcuL2RpcmVjdGl2ZXMvc3BlY2lhbHR5RExpc3RNYXAuanMnKSk7IiwidmFyIFNwZWNpYWx0aWVzTGlzdEN0cmwgPSByZXF1aXJlKCcuL1NwZWNpYWx0aWVzTGlzdEN0cmwuanMnKTtcbnZhciBTcGVjaWFsdHlDdHJsID0gcmVxdWlyZSgnLi9TcGVjaWFsdHlDdHJsLmpzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgZGlGdW5jdGlvbiA9IGZ1bmN0aW9uICgkc3RhdGVQcm92aWRlcikge1xuICAgICAgICAkc3RhdGVQcm92aWRlci5zdGF0ZSgnc3BlY2lhbHRpZXMnLCB7XG4gICAgICAgICAgICB1cmw6ICcvc3BlY2lhbHRpZXMnLFxuICAgICAgICAgICAgdmlld3M6IHtcbiAgICAgICAgICAgICAgICAnYm9keUAnOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6IFNwZWNpYWx0aWVzTGlzdEN0cmwsXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmU6IFNwZWNpYWx0aWVzTGlzdEN0cmwucmVzb2x2ZXIoKSxcbiAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvcy9odG1sL3RlbXBsYXRlcy9zcGVjaWFsdGllcy5odG1sJ1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSkuc3RhdGUoJ3NwZWNpYWx0aWVzLml0ZW0nLCB7XG4gICAgICAgICAgICB1cmw6ICcvaXRlbS86aWQnLFxuICAgICAgICAgICAgdmlld3M6IHtcbiAgICAgICAgICAgICAgICAnYm9keUAnOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6IFNwZWNpYWx0eUN0cmwsXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmU6IFNwZWNpYWx0eUN0cmwucmVzb2x2ZXIoKSxcbiAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvcy9odG1sL3RlbXBsYXRlcy9zcGVjaWFsdHkuaHRtbCdcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgZGlGdW5jdGlvbi4kaW5qZWN0ID0gWyckc3RhdGVQcm92aWRlciddO1xuICAgIHJldHVybiBkaUZ1bmN0aW9uO1xufSgpOyIsInZhciBjZmcgPSByZXF1aXJlKCcuLi8uLi9jb25maWcnKTtcbmNsYXNzIFVzZXJMaXN0Q3RybCB7XG4gICAgY29uc3RydWN0b3IoJHNjb3BlLCBkYXRhLCBEYXRhQ29udmVydCwgJGluamVjdG9yKSB7XG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgICAgdGhpcy5zID0gJHNjb3BlO1xuICAgICAgICB0aGlzLnJlcXVpcmUgPSAkaW5qZWN0b3IuZ2V0O1xuICAgICAgICB0aGlzLnMuaXRlbSA9IGRhdGE7XG4gICAgICAgIHRoaXMucy5Vc2VyTGlzdEN0cmwgPSB0aGlzO1xuICAgICAgICB2YXIgY29udmVydGVkRGF0YSA9IERhdGFDb252ZXJ0KFtdLmNvbmNhdChkYXRhLnNwZWNpYWxpemF0aW9uX2luZm8uc3BlY2lhbGl0eS5yZWxhdGlvbnMsIGRhdGEuc3BlY2lhbGl6YXRpb25faW5mby5yZWxhdGlvbnMpKTtcbiAgICAgICAgdGhpcy5kYXRhID0gZGF0YTtcbiAgICAgICAgdGhpcy5kaXNjaXBsaW5lc19saXN0ID0gY29udmVydGVkRGF0YS5kaXNjaXBsaW5lcztcbiAgICAgICAgdGhpcy5kaXNjaXBsaW5lcyA9IHtcbiAgICAgICAgICAgIGNyZWRpdHNMZWZ0OiBjZmcuc2VtZXN0ZXIud2VpZ2h0ICogY2ZnLnNlbWVzdGVyLmNvdW50LFxuICAgICAgICAgICAgY3JlZGl0c1N0YWNrOiBbXSxcbiAgICAgICAgICAgIGxpc3Q6IFtdLFxuICAgICAgICAgICAgc2VsZWN0ZWQ6IFtdXG4gICAgICAgIH07XG4gICAgICAgIGZvciAodmFyIF9pID0gMCwgX2wgPSB0aGlzLmRhdGEuZGlzY2lwbGluZXMubGVuZ3RoOyBfaSA8IF9sOyBfaSsrKSB7XG4gICAgICAgICAgICB2YXIgaSA9IHRoaXMuZGF0YS5kaXNjaXBsaW5lc1tfaV07XG4gICAgICAgICAgICB0aGlzLmRpc2NpcGxpbmVzLnNlbGVjdGVkLnB1c2godGhpcy5kaXNjaXBsaW5lc19saXN0W2ldKTtcbiAgICAgICAgfVxuICAgICAgICBmb3IgKHZhciBpIGluIHRoaXMuZGlzY2lwbGluZXNfbGlzdCkge1xuICAgICAgICAgICAgaWYgKHRoaXMuZGlzY2lwbGluZXNfbGlzdFtpXS5zZWxlY3RlZCAmJiB0aGlzLmRpc2NpcGxpbmVzX2xpc3RbaV0udHlwZSA9PSAnbScpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2NpcGxpbmVzLmNyZWRpdHNMZWZ0IC09IHRoaXMuZGlzY2lwbGluZXNfbGlzdFtpXS53ZWlnaHQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodGhpcy5kaXNjaXBsaW5lc19saXN0W2ldLnR5cGUgPT0gJ28nKSB7XG4gICAgICAgICAgICAgICAgaWYgKCEodGhpcy5kYXRhLmRpc2NpcGxpbmVzLmluZGV4T2YodGhpcy5kaXNjaXBsaW5lc19saXN0W2ldLmlkIHwgMCkgPj0gMCkpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNjaXBsaW5lcy5saXN0LnB1c2godGhpcy5kaXNjaXBsaW5lc19saXN0W2ldKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2NpcGxpbmVzLmNyZWRpdHNMZWZ0IC09IHRoaXMuZGlzY2lwbGluZXNfbGlzdFtpXS53ZWlnaHQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMudXBkYXRlRGF0YSgpO1xuICAgIH1cbiAgICBhZGREaXNjaXBsaW5lKHBvc2l0aW9uKSB7XG4gICAgICAgIHZhciBkaXNjaXBsaW5lID0gdGhpcy5kaXNjaXBsaW5lcy5saXN0LnNwbGljZShwb3NpdGlvbiwgMSlbMF07XG4gICAgICAgIGlmICh0aGlzLmRpc2NpcGxpbmVzLmNyZWRpdHNMZWZ0IC0gZGlzY2lwbGluZS53ZWlnaHQgPCAwKSB7XG4gICAgICAgICAgICBhbGVydCgnXFx1MDQxNFxcdTA0NDNcXHUwNDM2XFx1MDQzNSBcXHUwNDMxXFx1MDQzMFxcdTA0MzNcXHUwNDMwXFx1MDQ0MlxcdTA0M0UgXFx1MDQzNFxcdTA0MzhcXHUwNDQxXFx1MDQ0NlxcdTA0MzhcXHUwNDNGXFx1MDQzQlxcdTA0NTZcXHUwNDNELicpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuZGlzY2lwbGluZXMuY3JlZGl0c0xlZnQgLT0gZGlzY2lwbGluZS53ZWlnaHQ7XG4gICAgICAgIHRoaXMuZGlzY2lwbGluZXMuc2VsZWN0ZWQucHVzaChkaXNjaXBsaW5lKTtcbiAgICAgICAgdGhpcy51cGRhdGVEYXRhKCk7XG4gICAgfVxuICAgIHJlbW92ZURpc2NpcGxpbmUocG9zaXRpb24pIHtcbiAgICAgICAgdmFyIGRpc2NpcGxpbmUgPSB0aGlzLmRpc2NpcGxpbmVzLnNlbGVjdGVkLnNwbGljZShwb3NpdGlvbiwgMSlbMF07XG4gICAgICAgIGlmIChkaXNjaXBsaW5lLndlaWdodCA+IDApIHtcbiAgICAgICAgICAgIHRoaXMuZGlzY2lwbGluZXMuY3JlZGl0c0xlZnQgKz0gZGlzY2lwbGluZS53ZWlnaHQ7XG4gICAgICAgICAgICB0aGlzLmRpc2NpcGxpbmVzLmNyZWRpdHNTdGFjay5wdXNoKGRpc2NpcGxpbmUud2VpZ2h0KTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmRpc2NpcGxpbmVzLmxpc3QucHVzaChkaXNjaXBsaW5lKTtcbiAgICAgICAgdGhpcy51cGRhdGVEYXRhKCk7XG4gICAgfVxuICAgIHVwZGF0ZURhdGEoKSB7XG4gICAgICAgIHRoaXMuZGF0YS5kaXNjaXBsaW5lcyA9IFtdO1xuICAgICAgICBmb3IgKHZhciBfaSA9IDAsIF9sID0gdGhpcy5kaXNjaXBsaW5lcy5zZWxlY3RlZC5sZW5ndGg7IF9pIDwgX2w7IF9pKyspIHtcbiAgICAgICAgICAgIHZhciBpID0gdGhpcy5kaXNjaXBsaW5lcy5zZWxlY3RlZFtfaV07XG4gICAgICAgICAgICB0aGlzLmRhdGEuZGlzY2lwbGluZXMucHVzaChpLmlkKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBzdWJtaXQoKSB7XG4gICAgICAgIHZhciBzdGF0ZSA9IHRoaXMucmVxdWlyZSgnJHN0YXRlJyksIHJ0ID0gdGhpcy5yZXF1aXJlKCckcm9vdFNjb3BlJyk7XG4gICAgICAgIHJ0LiRlbWl0KCdmb3JtOnN0YXJ0Jyk7XG4gICAgICAgIHRoaXMuZGF0YS4kdXBkYXRlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJ0LiRlbWl0KCdmb3JtOmVuZCcpO1xuICAgICAgICAgICAgc3RhdGUuZ28oc3RhdGUuY3VycmVudCwge30sIHsgcmVsb2FkOiB0cnVlIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgcmVtb3ZlKCkge1xuICAgICAgICB2YXIgc3RhdGUgPSB0aGlzLnJlcXVpcmUoJyRzdGF0ZScpLCBydCA9IHRoaXMucmVxdWlyZSgnJHJvb3RTY29wZScpO1xuICAgICAgICBydC4kZW1pdCgnZm9ybTpzdGFydCcpO1xuICAgICAgICB0aGlzLmRhdGFbJyRkZWxldGUnXShmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBydC4kZW1pdCgnZm9ybTplbmQnKTtcbiAgICAgICAgICAgIHN0YXRlLmdvKCd1c2VyLWxpc3QnLCB7fSwgeyByZWxvYWQ6IHRydWUgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBzdGF0aWMgcmVzb2x2ZXIoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAvLyBzcGVjOiBmdW5jdGlvbigkc3RhdGUsICRzdGF0ZVBhcmFtcywgRGF0YSkge1xuICAgICAgICAgICAgLy8gICByZXR1cm4gRGF0YS5nZXRTcGVjaWFsdHkoJHN0YXRlUGFyYW1zLmlkKTtcbiAgICAgICAgICAgIC8vIH0sXG4gICAgICAgICAgICBkYXRhOiBmdW5jdGlvbiAoJHN0YXRlLCAkc3RhdGVQYXJhbXMsIEFwaSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBBcGkudXNlcl9kaXNjaXBsaW5lX2xpc3QuZ2V0KCRzdGF0ZVBhcmFtcykuJHByb21pc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfVxufVxuVXNlckxpc3RDdHJsLiRpbmplY3QgPSBbXG4gICAgJyRzY29wZScsXG4gICAgJ2RhdGEnLFxuICAgICdEYXRhQ29udmVydCcsXG4gICAgJyRpbmplY3Rvcidcbl07XG5tb2R1bGUuZXhwb3J0cyA9IFVzZXJMaXN0Q3RybDsiLCJjbGFzcyBVc2VyTGlzdExpc3RDdHJsIHtcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsIGRhdGEpIHtcbiAgICAgICAgJHNjb3BlLmxpc3RzID0gZGF0YS5yZXN1bHRzO1xuICAgIH1cbiAgICBzdGF0aWMgcmVzb2x2ZXIoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBkYXRhOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIGRpRnVuY3Rpb24gPSBmdW5jdGlvbiAoJHN0YXRlLCAkc3RhdGVQYXJhbXMsIEFwaSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gQXBpLnVzZXJfZGlzY2lwbGluZV9saXN0LnF1ZXJ5KCRzdGF0ZVBhcmFtcykuJHByb21pc2U7XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBkaUZ1bmN0aW9uLiRpbmplY3QgPSBbXG4gICAgICAgICAgICAgICAgICAgICckc3RhdGUnLFxuICAgICAgICAgICAgICAgICAgICAnJHN0YXRlUGFyYW1zJyxcbiAgICAgICAgICAgICAgICAgICAgJ0FwaSdcbiAgICAgICAgICAgICAgICBdO1xuICAgICAgICAgICAgICAgIHJldHVybiBkaUZ1bmN0aW9uO1xuICAgICAgICAgICAgfSgpXG4gICAgICAgIH07XG4gICAgfVxufVxuVXNlckxpc3RMaXN0Q3RybC4kaW5qZWN0ID0gW1xuICAgICckc2NvcGUnLFxuICAgICdkYXRhJ1xuXTtcbm1vZHVsZS5leHBvcnRzID0gVXNlckxpc3RMaXN0Q3RybDsiLCJ2YXIgY2ZnID0gcmVxdWlyZSgnLi4vLi4vY29uZmlnJyk7XG5jbGFzcyBVc2VyTGlzdE5ld0N0cmwge1xuICAgIGNvbnN0cnVjdG9yKCRzY29wZSwgZGF0YSwgRGF0YUNvbnZlcnQsICRpbmplY3Rvcikge1xuICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgICAgIHRoaXMucyA9ICRzY29wZTtcbiAgICAgICAgdGhpcy5yZXF1aXJlID0gJGluamVjdG9yLmdldDtcbiAgICAgICAgdGhpcy5zLml0ZW0gPSBkYXRhO1xuICAgICAgICB0aGlzLnMuVXNlckxpc3RDdHJsID0gdGhpcztcbiAgICAgICAgdmFyIGNvbnZlcnRlZERhdGEgPSBEYXRhQ29udmVydChbXS5jb25jYXQoZGF0YS5zcGVjaWFsaXphdGlvbl9pbmZvLnNwZWNpYWxpdHkucmVsYXRpb25zLCBkYXRhLnNwZWNpYWxpemF0aW9uX2luZm8ucmVsYXRpb25zKSk7XG4gICAgICAgIHRoaXMuZGF0YSA9IGRhdGE7XG4gICAgICAgIHRoaXMuZGlzY2lwbGluZXNfbGlzdCA9IGNvbnZlcnRlZERhdGEuZGlzY2lwbGluZXM7XG4gICAgICAgIHRoaXMuZGlzY2lwbGluZXMgPSB7XG4gICAgICAgICAgICBjcmVkaXRzTGVmdDogY2ZnLnNlbWVzdGVyLndlaWdodCAqIGNmZy5zZW1lc3Rlci5jb3VudCxcbiAgICAgICAgICAgIGNyZWRpdHNTdGFjazogW10sXG4gICAgICAgICAgICBsaXN0OiBbXSxcbiAgICAgICAgICAgIHNlbGVjdGVkOiBbXVxuICAgICAgICB9O1xuICAgICAgICBmb3IgKHZhciBfaSA9IDAsIF9sID0gdGhpcy5kYXRhLmRpc2NpcGxpbmVzLmxlbmd0aDsgX2kgPCBfbDsgX2krKykge1xuICAgICAgICAgICAgdmFyIGkgPSB0aGlzLmRhdGEuZGlzY2lwbGluZXNbX2ldO1xuICAgICAgICAgICAgdGhpcy5kaXNjaXBsaW5lcy5zZWxlY3RlZC5wdXNoKHRoaXMuZGlzY2lwbGluZXNfbGlzdFtpXSk7XG4gICAgICAgIH1cbiAgICAgICAgZm9yICh2YXIgaSBpbiB0aGlzLmRpc2NpcGxpbmVzX2xpc3QpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmRpc2NpcGxpbmVzX2xpc3RbaV0uc2VsZWN0ZWQgJiYgdGhpcy5kaXNjaXBsaW5lc19saXN0W2ldLnR5cGUgPT0gJ20nKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kaXNjaXBsaW5lcy5jcmVkaXRzTGVmdCAtPSB0aGlzLmRpc2NpcGxpbmVzX2xpc3RbaV0ud2VpZ2h0O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRoaXMuZGlzY2lwbGluZXNfbGlzdFtpXS50eXBlID09ICdvJykge1xuICAgICAgICAgICAgICAgIGlmICghKHRoaXMuZGF0YS5kaXNjaXBsaW5lcy5pbmRleE9mKHRoaXMuZGlzY2lwbGluZXNfbGlzdFtpXS5pZCkgPj0gMCkgJiYgIXRoaXMuZGlzY2lwbGluZXNfbGlzdFtpXS5zZWxlY3RlZCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2NpcGxpbmVzLmxpc3QucHVzaCh0aGlzLmRpc2NpcGxpbmVzX2xpc3RbaV0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5kaXNjaXBsaW5lc19saXN0W2ldLnNlbGVjdGVkKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzY2lwbGluZXMuc2VsZWN0ZWQucHVzaCh0aGlzLmRpc2NpcGxpbmVzX2xpc3RbaV0pO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2NpcGxpbmVzLmNyZWRpdHNMZWZ0IC09IHRoaXMuZGlzY2lwbGluZXNfbGlzdFtpXS53ZWlnaHQ7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNjaXBsaW5lcy5jcmVkaXRzTGVmdCAtPSB0aGlzLmRpc2NpcGxpbmVzX2xpc3RbaV0ud2VpZ2h0O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLnVwZGF0ZURhdGEoKTtcbiAgICB9XG4gICAgYWRkRGlzY2lwbGluZShwb3NpdGlvbikge1xuICAgICAgICB2YXIgZGlzY2lwbGluZSA9IHRoaXMuZGlzY2lwbGluZXMubGlzdC5zcGxpY2UocG9zaXRpb24sIDEpWzBdO1xuICAgICAgICBpZiAodGhpcy5kaXNjaXBsaW5lcy5jcmVkaXRzTGVmdCAtIGRpc2NpcGxpbmUud2VpZ2h0IDwgMCkge1xuICAgICAgICAgICAgYWxlcnQoJ1xcdTA0MjFcXHUwNDNCXFx1MDQzOFxcdTA0NDhcXHUwNDNBXFx1MDQzRVxcdTA0M0MgXFx1MDQzQ1xcdTA0M0RcXHUwNDNFXFx1MDQzM1xcdTA0M0UgXFx1MDQzNFxcdTA0MzhcXHUwNDQxXFx1MDQ0NlxcdTA0MzhcXHUwNDNGXFx1MDQzQlxcdTA0MzhcXHUwNDNELicpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuZGlzY2lwbGluZXMuY3JlZGl0c0xlZnQgLT0gZGlzY2lwbGluZS53ZWlnaHQ7XG4gICAgICAgIHRoaXMuZGlzY2lwbGluZXMuc2VsZWN0ZWQucHVzaChkaXNjaXBsaW5lKTtcbiAgICAgICAgdGhpcy51cGRhdGVEYXRhKCk7XG4gICAgfVxuICAgIHJlbW92ZURpc2NpcGxpbmUocG9zaXRpb24pIHtcbiAgICAgICAgdmFyIGRpc2NpcGxpbmUgPSB0aGlzLmRpc2NpcGxpbmVzLnNlbGVjdGVkLnNwbGljZShwb3NpdGlvbiwgMSlbMF07XG4gICAgICAgIGlmIChkaXNjaXBsaW5lLndlaWdodCA+IDApIHtcbiAgICAgICAgICAgIHRoaXMuZGlzY2lwbGluZXMuY3JlZGl0c0xlZnQgKz0gZGlzY2lwbGluZS53ZWlnaHQ7XG4gICAgICAgICAgICB0aGlzLmRpc2NpcGxpbmVzLmNyZWRpdHNTdGFjay5wdXNoKGRpc2NpcGxpbmUud2VpZ2h0KTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmRpc2NpcGxpbmVzLmxpc3QucHVzaChkaXNjaXBsaW5lKTtcbiAgICAgICAgdGhpcy51cGRhdGVEYXRhKCk7XG4gICAgfVxuICAgIHVwZGF0ZURhdGEoKSB7XG4gICAgICAgIHRoaXMuZGF0YS5kaXNjaXBsaW5lcyA9IFtdO1xuICAgICAgICBmb3IgKHZhciBfaSA9IDAsIF9sID0gdGhpcy5kaXNjaXBsaW5lcy5zZWxlY3RlZC5sZW5ndGg7IF9pIDwgX2w7IF9pKyspIHtcbiAgICAgICAgICAgIHZhciBpID0gdGhpcy5kaXNjaXBsaW5lcy5zZWxlY3RlZFtfaV07XG4gICAgICAgICAgICB0aGlzLmRhdGEuZGlzY2lwbGluZXMucHVzaChpLmlkKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBzdWJtaXQoKSB7XG4gICAgICAgIHZhciBzdGF0ZSA9IHRoaXMucmVxdWlyZSgnJHN0YXRlJyksIHJ0ID0gdGhpcy5yZXF1aXJlKCckcm9vdFNjb3BlJyk7XG4gICAgICAgIHJ0LiRlbWl0KCdmb3JtOnN0YXJ0Jyk7XG4gICAgICAgIHRoaXMuZGF0YS4kY3JlYXRlKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICBydC4kZW1pdCgnZm9ybTplbmQnKTtcbiAgICAgICAgICAgIHN0YXRlLmdvKCd1c2VyLWxpc3QuaXRlbScsIHsgaWQ6IGRhdGEuaWQgfSwgeyByZWxvYWQ6IHRydWUgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBzdGF0aWMgcmVzb2x2ZXIoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAvLyBzcGVjOiBmdW5jdGlvbigkc3RhdGUsICRzdGF0ZVBhcmFtcywgRGF0YSkge1xuICAgICAgICAgICAgLy8gICByZXR1cm4gRGF0YS5nZXRTcGVjaWFsdHkoJHN0YXRlUGFyYW1zLmlkKTtcbiAgICAgICAgICAgIC8vIH0sXG4gICAgICAgICAgICBzcGVjaWFsaXphdGlvbjogZnVuY3Rpb24gKCRzdGF0ZSwgJHN0YXRlUGFyYW1zLCBBcGkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gQXBpLnNwZWNpYWxpemF0aW9uLmdldCgkc3RhdGVQYXJhbXMpLiRwcm9taXNlO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGRhdGE6IGZ1bmN0aW9uICgkc3RhdGUsICRzdGF0ZVBhcmFtcywgQXBpLCBzcGVjaWFsaXphdGlvbiwgJHJvb3RTY29wZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgQXBpLnVzZXJfZGlzY2lwbGluZV9saXN0KHtcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJycsXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnJyxcbiAgICAgICAgICAgICAgICAgICAgZGlzY2lwbGluZXM6IFtdLFxuICAgICAgICAgICAgICAgICAgICBzcGVjaWFsaXphdGlvbjogc3BlY2lhbGl6YXRpb24uaWQsXG4gICAgICAgICAgICAgICAgICAgIHNwZWNpYWxpemF0aW9uX2luZm86IHNwZWNpYWxpemF0aW9uLFxuICAgICAgICAgICAgICAgICAgICB1c2VyOiAkcm9vdFNjb3BlLmN1cnJlbnRVc2VyLmlkXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfVxufVxuVXNlckxpc3ROZXdDdHJsLiRpbmplY3QgPSBbXG4gICAgJyRzY29wZScsXG4gICAgJ2RhdGEnLFxuICAgICdEYXRhQ29udmVydCcsXG4gICAgJyRpbmplY3Rvcidcbl07XG5tb2R1bGUuZXhwb3J0cyA9IFVzZXJMaXN0TmV3Q3RybDsiLCJ2YXIgY2ZnID0gcmVxdWlyZSgnLi4vLi4vY29uZmlnJyk7XG5jbGFzcyBVc2VyTGlzdFZpZXdDdHJsIHtcbiAgICBjb25zdHJ1Y3Rvcigkc2NvcGUsIGRhdGEsIERhdGFTb3J0LCBEYXRhQ29udmVydCkge1xuICAgICAgICB0aGlzLnMgPSAkc2NvcGU7XG4gICAgICAgIHRoaXMucy5kYXRhID0gZGF0YTtcbiAgICAgICAgdGhpcy5zLnNwZWMgPSBkYXRhLnNwZWNpYWxpemF0aW9uX2luZm87XG4gICAgICAgIHRoaXMucy5zcGVjaWFsdHlDdHJsID0gdGhpcztcbiAgICAgICAgdmFyIGNvbnZlcnRlZERhdGEgPSAvLyB2YXIgY29udmVydGVkRGF0YSA9IERhdGFDb252ZXJ0KEBzLnNwZWMucmVsYXRpb25zKTtcbiAgICAgICAgRGF0YUNvbnZlcnQoW10uY29uY2F0KHRoaXMucy5zcGVjLnNwZWNpYWxpdHkucmVsYXRpb25zLCB0aGlzLnMuc3BlYy5yZWxhdGlvbnMpKTtcbiAgICAgICAgdGhpcy5jb25uZWN0aW9ucyA9IGNvbnZlcnRlZERhdGEuY29ubmVjdGlvbnM7XG4gICAgICAgIHRoaXMuZGlzY2lwbGluZXMgPSBjb252ZXJ0ZWREYXRhLmRpc2NpcGxpbmVzO1xuICAgICAgICBmb3IgKHZhciBpIGluIHRoaXMuZGlzY2lwbGluZXMpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmRpc2NpcGxpbmVzW2ldLnR5cGUgPT0gJ28nKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kaXNjaXBsaW5lc1tpXS5zZWxlY3RlZCA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGZvciAodmFyIF9pID0gMCwgX2wgPSBkYXRhLmRpc2NpcGxpbmVzLmxlbmd0aDsgX2kgPCBfbDsgX2krKykge1xuICAgICAgICAgICAgdmFyIGkgPSBkYXRhLmRpc2NpcGxpbmVzW19pXTtcbiAgICAgICAgICAgIHRoaXMuZGlzY2lwbGluZXNbaV0uc2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIHZhciBkZWZhdWx0Q2F0ZWdvcnkgPSAndXNlci0tY2hvaWNlJztcbiAgICAgICAgdGhpcy5jYXRlZ29yaWVzID0gY29udmVydGVkRGF0YS5jYXRlZ29yaWVzO1xuICAgICAgICB0aGlzLmNhdGVnb3JpZXNbZGVmYXVsdENhdGVnb3J5XSA9IHtcbiAgICAgICAgICAgIGlkOiBkZWZhdWx0Q2F0ZWdvcnksXG4gICAgICAgICAgICBuYW1lOiAnXFx1MDQxMlxcdTA0MzhcXHUwNDMxXFx1MDQ1NlxcdTA0NDAgXFx1MDQ0MVxcdTA0NDJcXHUwNDQzXFx1MDQzNFxcdTA0MzVcXHUwNDNEXFx1MDQ0MlxcdTA0MzAnXG4gICAgICAgIH07XG4gICAgICAgIGZvciAodmFyIGkgaW4gdGhpcy5jYXRlZ29yaWVzKSB7XG4gICAgICAgICAgICBpZiAoIXRoaXMuY2F0ZWdvcmllcy5oYXNPd25Qcm9wZXJ0eShpKSlcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgIHRoaXMuY2F0ZWdvcmllc1tpXS5saXN0ID0gW107XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5zb3J0ZWQgPSBEYXRhU29ydChjb252ZXJ0ZWREYXRhLCBjZmcpO1xuICAgICAgICBmb3IgKHZhciBpIGluIHRoaXMuc29ydGVkLmRpc2NzKSB7XG4gICAgICAgICAgICBpZiAoIXRoaXMuc29ydGVkLmRpc2NzLmhhc093blByb3BlcnR5KGkpKVxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgdGhpcy5jYXRlZ29yaWVzW3RoaXMuc29ydGVkLmRpc2NzW2ldLmNhdGVnb3J5LmlkIHx8IGRlZmF1bHRDYXRlZ29yeV0ubGlzdC5wdXNoKHRoaXMuc29ydGVkLmRpc2NzW2ldLmlkKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBzdGF0aWMgcmVzb2x2ZXIoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAvLyBzcGVjOiBmdW5jdGlvbigkc3RhdGUsICRzdGF0ZVBhcmFtcywgRGF0YSkge1xuICAgICAgICAgICAgLy8gICByZXR1cm4gRGF0YS5nZXRTcGVjaWFsdHkoJHN0YXRlUGFyYW1zLmlkKTtcbiAgICAgICAgICAgIC8vIH0sXG4gICAgICAgICAgICBkYXRhOiBmdW5jdGlvbiAoJHN0YXRlLCAkc3RhdGVQYXJhbXMsIEFwaSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBBcGkudXNlcl9kaXNjaXBsaW5lX2xpc3QuZ2V0KCRzdGF0ZVBhcmFtcykuJHByb21pc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfVxufVxuVXNlckxpc3RWaWV3Q3RybC4kaW5qZWN0ID0gW1xuICAgICckc2NvcGUnLFxuICAgICdkYXRhJyxcbiAgICAnRGF0YVNvcnQnLFxuICAgICdEYXRhQ29udmVydCdcbl07XG5tb2R1bGUuZXhwb3J0cyA9IFVzZXJMaXN0Vmlld0N0cmw7IiwiY2xhc3MgU2VsZWN0Q3RybCB7XG4gICAgY29uc3RydWN0b3IoJHNjb3BlKSB7XG4gICAgfVxufVxuU2VsZWN0Q3RybC4kaW5qZWN0ID0gWyckc2NvcGUnXTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB7XG4gICAgICAgIGNvbnRyb2xsZXI6IFNlbGVjdEN0cmwsXG4gICAgICAgIGxpbms6IGZ1bmN0aW9uIChzKSB7XG4gICAgICAgICAgICAoZnVuY3Rpb24gKCRlbGVtZW50LCAkYXR0cnMsIGN0cmwpIHtcbiAgICAgICAgICAgICAgICBpZiAoIWN0cmwpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH0uYXBwbHkocywgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSkpO1xuICAgICAgICB9XG4gICAgfTtcbn07IiwiYW5ndWxhci5tb2R1bGUoJ3BhZ2VzLnVzZXItbGlzdCcsIFsndWkucm91dGVyJ10pLmNvbmZpZyhyZXF1aXJlKCcuL3JvdXRlcycpKS5kaXJlY3RpdmUoJ3NlbGVjdExpc3QnLCByZXF1aXJlKCcuL2RpcmVjdGl2ZXMvc2VsZWN0TGlzdC5qcycpKTsiLCJ2YXIgVXNlckxpc3RMaXN0Q3RybCA9IHJlcXVpcmUoJy4vVXNlckxpc3RMaXN0Q3RybC5qcycpO1xudmFyIFVzZXJMaXN0Q3RybCA9IHJlcXVpcmUoJy4vVXNlckxpc3RDdHJsLmpzJyk7XG52YXIgVXNlckxpc3ROZXdDdHJsID0gcmVxdWlyZSgnLi9Vc2VyTGlzdE5ld0N0cmwuanMnKTtcbnZhciBVc2VyTGlzdFZpZXdDdHJsID0gcmVxdWlyZSgnLi9Vc2VyTGlzdFZpZXdDdHJsLmpzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgZGlGdW5jdGlvbiA9IGZ1bmN0aW9uICgkc3RhdGVQcm92aWRlcikge1xuICAgICAgICAkc3RhdGVQcm92aWRlci5zdGF0ZSgndXNlci1saXN0Jywge1xuICAgICAgICAgICAgdXJsOiAnL3VzZXItbGlzdCcsXG4gICAgICAgICAgICB2aWV3czoge1xuICAgICAgICAgICAgICAgICdib2R5QCc6IHtcbiAgICAgICAgICAgICAgICAgICAgY29udHJvbGxlcjogVXNlckxpc3RMaXN0Q3RybCxcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZTogVXNlckxpc3RMaXN0Q3RybC5yZXNvbHZlcigpLFxuICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9zL2h0bWwvdGVtcGxhdGVzL3VzZXItbGlzdC5odG1sJ1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSkuc3RhdGUoJ3VzZXItbGlzdC5pdGVtLW5ldycsIHtcbiAgICAgICAgICAgIHVybDogJy9pdGVtL25ldy86aWQnLFxuICAgICAgICAgICAgdmlld3M6IHtcbiAgICAgICAgICAgICAgICAnYm9keUAnOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6IFVzZXJMaXN0TmV3Q3RybCxcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZTogVXNlckxpc3ROZXdDdHJsLnJlc29sdmVyKCksXG4gICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL3MvaHRtbC90ZW1wbGF0ZXMvdXNlci1saXN0X2l0ZW1fbmV3Lmh0bWwnXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KS5zdGF0ZSgndXNlci1saXN0Lml0ZW0nLCB7XG4gICAgICAgICAgICB1cmw6ICcvaXRlbS86aWQnLFxuICAgICAgICAgICAgdmlld3M6IHtcbiAgICAgICAgICAgICAgICAnYm9keUAnOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6IFVzZXJMaXN0Q3RybCxcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZTogVXNlckxpc3RDdHJsLnJlc29sdmVyKCksXG4gICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL3MvaHRtbC90ZW1wbGF0ZXMvdXNlci1saXN0X2l0ZW0uaHRtbCdcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pLnN0YXRlKCd1c2VyLWxpc3QuaXRlbS12aWV3Jywge1xuICAgICAgICAgICAgdXJsOiAnL2l0ZW0vOmlkL3ZpZXcnLFxuICAgICAgICAgICAgdmlld3M6IHtcbiAgICAgICAgICAgICAgICAnYm9keUAnOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRyb2xsZXI6IFVzZXJMaXN0Vmlld0N0cmwsXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmU6IFVzZXJMaXN0Vmlld0N0cmwucmVzb2x2ZXIoKSxcbiAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvcy9odG1sL3RlbXBsYXRlcy91c2VyLWxpc3RfaXRlbV92aWV3Lmh0bWwnXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9O1xuICAgIGRpRnVuY3Rpb24uJGluamVjdCA9IFsnJHN0YXRlUHJvdmlkZXInXTtcbiAgICByZXR1cm4gZGlGdW5jdGlvbjtcbn0oKTsiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgZGlGdW5jdGlvbiA9IGZ1bmN0aW9uICgkcm9vdFNjb3BlKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAocykge1xuICAgICAgICAgICAgICAgIChmdW5jdGlvbiAoJGVsZW1lbnQsICRhdHRycykge1xuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLiRvbignJHN0YXRlQ2hhbmdlU3RhcnQnLCBzdGFydCk7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJG9uKCdmb3JtOnN0YXJ0Jywgc3RhcnQpO1xuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLiRvbignJHN0YXRlQ2hhbmdlU3VjY2VzcycsIGVuZCk7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJG9uKCckc3RhdGVDaGFuZ2VFcnJvcicsIGVuZCk7XG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJG9uKCdmb3JtOmVuZCcsIHN0YXJ0KTtcbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gc3RhcnQoZXZlbnQsIHRvU3RhdGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRlbGVtZW50LmFkZENsYXNzKCRhdHRycy5zdGF0ZVBlbmRpbmdDbGFzcyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gZW5kKGV2ZW50LCB0b1N0YXRlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudC5yZW1vdmVDbGFzcygkYXR0cnMuc3RhdGVQZW5kaW5nQ2xhc3MpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfS5hcHBseShzLCBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfTtcbiAgICBkaUZ1bmN0aW9uLiRpbmplY3QgPSBbJyRyb290U2NvcGUnXTtcbiAgICByZXR1cm4gZGlGdW5jdGlvbjtcbn0oKTsiXX0=