from django.conf.urls import include, url
from django.contrib import admin
from adminplus.sites import AdminSitePlus

admin.site = AdminSitePlus()
admin.autodiscover()

urlpatterns = [
    # Examples:
    # url(r'^blog/', include('blog.urls')),

    url(r'^', include('client.urls', namespace='client')),
    url(r'^admin/', include(admin.site.urls)),
    url('^api/', include('api.urls', namespace='api')),
    url('^profile/', include('profile.urls')),
]