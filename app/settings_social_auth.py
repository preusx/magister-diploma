# -*- coding: utf-8 -*-

SOCIAL_AUTH_VK_OAUTH2_KEY = '4920319'
SOCIAL_AUTH_VK_OAUTH2_SECRET = 'QdXQcz5sBiTWMEeaRnuP'

SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_LOGIN_URL = '/profile/auth/social/'

SOCIAL_AUTH_PIPELINE = (
        'social.pipeline.social_auth.social_details',
        'social.pipeline.social_auth.social_uid',
        'social.pipeline.social_auth.auth_allowed',

        'profile.social.pipeline.user.logout_existing',

        # 'social.pipeline.social_auth.social_user',
        'social.pipeline.social_auth.associate_by_email',
        'social.pipeline.user.get_username',
        'social.pipeline.user.create_user',
        'social.pipeline.social_auth.associate_user',
        'social.pipeline.social_auth.load_extra_data',
        # 'social.pipeline.user.user_details',

        'profile.social.pipeline.user.user_profile_data',
    )