from suit_ckeditor import widgets as w

class CKEditorWidget(w.CKEditorWidget):
    class Media(w.CKEditorWidget.Media):
        css = {
            'all': (
                'suit-ckeditor/ckeditor.css',
                'admin/ckeditor.css',
                )
        }