# -*- coding: utf-8 -*-
from django.apps import AppConfig

default_app_config = 'curriculum.CurriculumAppConfig'


class CurriculumAppConfig(AppConfig):
    name = 'curriculum'
    verbose_name = u'Учбовий план'
