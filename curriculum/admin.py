# -*- coding: utf-8 -*-
from django.contrib import admin
from django.forms import ModelForm

from django.utils.translation import ugettext_lazy as _
from suit_ckeditor.widgets import CKEditorWidget
from curriculum import widgets as w

from curriculum.models import Discipline, DisciplineRelation,\
    DisciplineCategory, UserDisciplineList,\
    Material, MaterialType,\
    Speciality, SpecialityToDiscipline,\
    Specialization, SpecializationToDiscipline,\
    Notion, NotionType






class SpecialityToDisciplineAdmin(admin.TabularInline):
    model = SpecialityToDiscipline
    extra = 1
    verbose_name_plural = u'Список дисциплин'

    fieldsets = [
        ('  ', {
                'fields': [
                    'category',
                    # 'type',
                    'discipline',
                    'weight',
                ]
            }
        ),
        ('Жестко задаем позицию(если нужно)', {
                'classes': ('collapse',),
                'fields': [
                    'semester_from',
                    'semester_to',
                ]
            }
        ),
    ]



class SpecialityAdminForm(ModelForm):
    class Meta:
        widgets = {
            'hint': CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'clipboard',   'groups': [ 'clipboard', 'undo' ] },
                    ]
                }),
            'description': CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'paragraph', 'groups': [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                        { 'name': 'links' },
                        { 'name': 'clipboard', 'groups': [ 'clipboard', 'undo' ] },
                        { 'name': 'editing', 'groups': [ 'find', 'selection', 'spellchecker' ] },
                        { 'name': 'tools' },
                        '/',
                        { 'name': 'styles' },
                        { 'name': 'colors' },
                        { 'name': 'insert' },
                    ]
                })
        }



class SpecialityAdmin(admin.ModelAdmin):
    form = SpecialityAdminForm
    inlines = [SpecialityToDisciplineAdmin]

    fieldsets = [
        (None, {
                'fields': [
                    'name',
                    'hint',
                ]
            }
        ),
        (None, {
                'fields': [
                    'goal',
                    'object',
                    # 'subject',
                    # 'tags',
                ]
            }
        ),
        (u'Описание', {
                'classes': ('full-width',),
                'fields': [
                    'description',
                ]
            }
        ),
    ]

    list_display = ('name', 'hint')



admin.site.register(Speciality, SpecialityAdmin)






class SpecializationToDisciplineAdmin(admin.TabularInline):
    model = SpecializationToDiscipline
    extra = 1
    verbose_name_plural = u'Список дисциплин'

    fieldsets = [
        ('  ', {
                'fields': [
                    'category',
                    'type',
                    'discipline',
                    'weight',
                ]
            }
        ),
        ('Жестко задаем позицию(если нужно)', {
                'classes': ('collapse',),
                'fields': [
                    'semester_from',
                    'semester_to',
                ]
            }
        ),
    ]



class SpecializationAdminForm(ModelForm):
    class Meta:
        widgets = {
            'hint': CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'clipboard',   'groups': [ 'clipboard', 'undo' ] },
                    ]
                }),
            'description': CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'paragraph', 'groups': [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                        { 'name': 'links' },
                        { 'name': 'clipboard', 'groups': [ 'clipboard', 'undo' ] },
                        { 'name': 'editing', 'groups': [ 'find', 'selection', 'spellchecker' ] },
                        { 'name': 'tools' },
                        '/',
                        { 'name': 'styles' },
                        { 'name': 'colors' },
                        { 'name': 'insert' },
                    ]
                })
        }



class SpecializationAdmin(admin.ModelAdmin):
    form = SpecializationAdminForm
    inlines = [SpecializationToDisciplineAdmin]

    fieldsets = [
        (None, {
                'fields': [
                    'name',
                ]
            }
        ),
        (None, {
                'fields': [
                    'goal',
                    'object',
                    # 'subject',
                    # 'tags',
                ]
            }
        ),
        (None, {
                'fields': [
                    'speciality',
                    'hint',
                ]
            }
        ),
        (u'Описание', {
                'classes': ('full-width',),
                'fields': [
                    'description',
                ]
            }
        ),
    ]

    list_display = ('name', 'hint')



admin.site.register(Specialization, SpecializationAdmin)






class DisciplineCategoryAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {
                'fields': [
                    'name',
                ]
            }
        ),
    ]

    list_display = ('name',)



admin.site.register(DisciplineCategory, DisciplineCategoryAdmin)





class DisciplineRelationChildrenInline(admin.TabularInline):
    model = DisciplineRelation
    fk_name = 'into'
    can_delete = True
    extra = 1
    verbose_name_plural = u'Дисциплины, требуемые для изучения'
    suit_classes = 'suit-tab suit-tab-general'

    fieldsets = [
        ('  ', {
                'fields': [
                    'type',
                    'out_of',
                ]
            }
        ),
    ]




class DisciplineMaterialInlineForm(ModelForm):
    class Meta:
        widgets = {
            'hint': w.CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'clipboard',   'groups': [ 'clipboard', 'undo' ] },
                    ]
                }),
            'description': w.CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'paragraph', 'groups': [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                        { 'name': 'links' },
                        { 'name': 'clipboard', 'groups': [ 'clipboard', 'undo' ] },
                        { 'name': 'editing', 'groups': [ 'find', 'selection', 'spellchecker' ] },
                        { 'name': 'tools' },
                        '/',
                        { 'name': 'styles' },
                        { 'name': 'colors' },
                        { 'name': 'insert' },
                    ]
                })
        }


class DisciplineMaterialInline(admin.StackedInline):
    model = Material
    extra = 1
    verbose_name_plural = u'Список материалов'
    form = DisciplineMaterialInlineForm
    suit_classes = 'suit-tab suit-tab-material'

    fieldsets = [
        (None, {
                'fields': [
                    'name',
                    'type',
                    'discipline',
                    'hint',
                ]
            }
        ),
        (None, {
                'fields': [
                    'description',
                ]
            }
        ),
        (None, {
                'fields': [
                    'required',
                ]
            }
        ),
    ]




class DisciplineRelationParentInline(admin.TabularInline):
    model = DisciplineRelation
    fk_name = 'out_of'
    can_delete = True
    extra = 1
    verbose_name_plural = u'Дисциплины, которые требуют изучения'
    suit_classes = 'suit-tab suit-tab-general'

    fieldsets = [
        ('  ', {
                'fields': [
                    'type',
                    'into',
                ]
            }
        ),
    ]



class DisciplineAdminForm(ModelForm):
    class Meta:
        widgets = {
            'hint': CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'clipboard',   'groups': [ 'clipboard', 'undo' ] },
                    ]
                }),
            'description': CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'paragraph', 'groups': [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                        { 'name': 'links' },
                        { 'name': 'clipboard', 'groups': [ 'clipboard', 'undo' ] },
                        { 'name': 'editing', 'groups': [ 'find', 'selection', 'spellchecker' ] },
                        { 'name': 'tools' },
                        '/',
                        { 'name': 'styles' },
                        { 'name': 'colors' },
                        { 'name': 'insert' },
                    ]
                })
        }



class DisciplineAdmin(admin.ModelAdmin):
    form = DisciplineAdminForm
    # inlines = [DisciplineRelationChildrenInline,
    #     DisciplineRelationParentInline]

    fieldsets = [
        (None, {
                'classes': ('suit-tab', 'suit-tab-general',),
                'fields': [
                    'name',
                    'hint',
                ]
            }
        ),
        (None, {
                'classes': ('suit-tab', 'suit-tab-general',),
                'fields': [
                    'goal',
                    'object',
                    # 'subject',
                    # 'tags',
                ]
            }
        ),
        (u'Описание', {
                'classes': ('full-width', 'suit-tab', 'suit-tab-general',),
                'fields': [
                    'description',
                ]
            }
        ),
    ]

    list_display = ('name', 'hint')
    suit_form_tabs = (('general', 'Основные данные'),)



admin.site.register(Discipline, DisciplineAdmin)





# class UserDisciplineListInline(admin.TabularInline):
#     model = UserDisciplineList.disciplines.through
#     extra = 0
#     verbose_name_plural = u'Список дисциплин'

#     editable_fields = []

#     fieldsets = [
#         ('  ', {
#                 'fields': [
#                     'name',
#                     'hint',
#                 ]
#             }
#         ),
#     ]

#     def get_readonly_fields(self, request, obj=None):
#         fields = []
#         for field in self.model._meta.get_all_field_names():
#             if (not field == 'id'):
#                 if (field not in self.editable_fields):
#                     fields.append(field)
#         return fields


class UserDisciplineListAdminForm(ModelForm):
    class Meta:
        widgets = {
            'description': CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'paragraph', 'groups': [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                        { 'name': 'links' },
                        { 'name': 'clipboard', 'groups': [ 'clipboard', 'undo' ] },
                        { 'name': 'editing', 'groups': [ 'find', 'selection', 'spellchecker' ] },
                        { 'name': 'tools' },
                        '/',
                        { 'name': 'styles' },
                        { 'name': 'colors' },
                        { 'name': 'insert' },
                    ]
                })
        }



class UserDisciplineListAdmin(admin.ModelAdmin):
    form = UserDisciplineListAdminForm
    # inlines = [UserDisciplineListInline]

    fieldsets = [
        (None, {
                'fields': [
                    'name',
                    'user',
                    'speciality',
                    'description',
                ]
            }
        ),
        (u'Список дисциплин', {
                'fields': [
                    'disciplines',
                ]
            }
        ),
    ]

    list_display = ('name', 'user', 'description')



admin.site.register(UserDisciplineList, UserDisciplineListAdmin)








class MaterialTypeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {
                'fields': [
                    'name',
                ]
            }
        ),
    ]

    list_display = ('name',)



# admin.site.register(MaterialType, MaterialTypeAdmin)



class MaterialInlineForm(ModelForm):
    class Meta:
        widgets = {
            'hint': CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'clipboard',   'groups': [ 'clipboard', 'undo' ] },
                    ]
                }),
            'description': CKEditorWidget(editor_options={
                    'startupFocus': False,
                    'toolbarGroups': [
                        { 'name': 'basicstyles', 'groups': [ 'basicstyles', 'cleanup' ] },
                        { 'name': 'paragraph', 'groups': [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                        { 'name': 'links' },
                        { 'name': 'clipboard', 'groups': [ 'clipboard', 'undo' ] },
                        { 'name': 'editing', 'groups': [ 'find', 'selection', 'spellchecker' ] },
                        { 'name': 'tools' },
                        '/',
                        { 'name': 'styles' },
                        { 'name': 'colors' },
                        { 'name': 'insert' },
                    ]
                })
        }


class MaterialAdmin(admin.ModelAdmin):
    form = MaterialInlineForm

    fieldsets = [
        (None, {
                'fields': [
                    'name',
                    'discipline',
                    'type',
                    'hint',
                ]
            }
        ),
        (u'Описание', {
                'classes': ('full-width',),
                'fields': [
                    'description',
                ]
            }
        ),
        (None, {
                'fields': [
                    'required',
                ]
            }
        ),
    ]

    list_display = ('name', 'discipline', 'type')

# admin.site.register(Material, MaterialAdmin)



class NotionAdmin(admin.ModelAdmin):
    form = MaterialInlineForm

    fieldsets = [
        (None, {
                'fields': [
                    'name',
                    'type',
                ]
            }
        ),
        (u'Описание', {
                'fields': [
                    'hint',
                ]
            }
        ),
    ]

admin.site.register(Notion, NotionAdmin)



class NotionTypeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {
                'fields': [
                    'name',
                ]
            }
        ),
    ]

    list_display = ('name',)



admin.site.register(NotionType, NotionTypeAdmin)