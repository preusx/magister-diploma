# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('curriculum', '0003_auto_20150621_1231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discipline',
            name='goal',
            field=models.ManyToManyField(related_name='disciplinegoal', verbose_name='\u0415\u043b\u0435\u043c\u0435\u043d\u0442\u0438 \u043a\u043e\u043c\u043f\u0435\u0442\u0435\u043d\u0446\u0456\u0439', to='curriculum.Notion', blank=True),
        ),
        migrations.AlterField(
            model_name='speciality',
            name='goal',
            field=models.ManyToManyField(related_name='specialitygoal', verbose_name='\u0415\u043b\u0435\u043c\u0435\u043d\u0442\u0438 \u043a\u043e\u043c\u043f\u0435\u0442\u0435\u043d\u0446\u0456\u0439', to='curriculum.Notion', blank=True),
        ),
        migrations.AlterField(
            model_name='specialization',
            name='goal',
            field=models.ManyToManyField(related_name='specializationgoal', verbose_name='\u0415\u043b\u0435\u043c\u0435\u043d\u0442\u0438 \u043a\u043e\u043c\u043f\u0435\u0442\u0435\u043d\u0446\u0456\u0439', to='curriculum.Notion', blank=True),
        ),
    ]
