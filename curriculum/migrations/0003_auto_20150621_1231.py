# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('curriculum', '0002_auto_20150618_1326'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='notion',
            options={'verbose_name': '\u041d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u0438\u0439 \u0435\u043b\u0435\u043c\u0435\u043d\u0442', 'verbose_name_plural': '\u041d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u0456 \u0435\u043b\u0435\u043c\u0435\u043d\u0442\u0438'},
        ),
        migrations.AlterModelOptions(
            name='notiontype',
            options={'verbose_name': '\u0422\u0438\u043f \u043d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u043e\u0433\u043e \u0435\u043b\u0435\u043c\u0435\u043d\u0442\u0443', 'verbose_name_plural': '\u0422\u0438\u043f\u0438 \u043d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u0438\u0445 \u0435\u043b\u0435\u043c\u0435\u043d\u0442\u0456\u0432'},
        ),
        migrations.AlterField(
            model_name='discipline',
            name='goal',
            field=models.ManyToManyField(related_name='disciplinegoal', verbose_name='\u0415\u043b\u0435\u043c\u0435\u043d\u0442\u0438 \u043a\u043e\u043c\u043f\u0435\u0442\u0435\u043d\u0446\u0456\u0457', to='curriculum.Notion', blank=True),
        ),
        migrations.AlterField(
            model_name='discipline',
            name='object',
            field=models.ManyToManyField(related_name='disciplineobject', verbose_name='\u0411\u0430\u0437\u043e\u0432\u0456 \u043d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u0456 \u0435\u043b\u0435\u043c\u0435\u043d\u0442\u0438', to='curriculum.Notion', blank=True),
        ),
        migrations.AlterField(
            model_name='notion',
            name='type',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u043d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u043e\u0433\u043e \u0435\u043b\u0435\u043c\u0435\u043d\u0442\u0443', to='curriculum.NotionType'),
        ),
        migrations.AlterField(
            model_name='speciality',
            name='goal',
            field=models.ManyToManyField(related_name='specialitygoal', verbose_name='\u0415\u043b\u0435\u043c\u0435\u043d\u0442\u0438 \u043a\u043e\u043c\u043f\u0435\u0442\u0435\u043d\u0446\u0456\u0457', to='curriculum.Notion', blank=True),
        ),
        migrations.AlterField(
            model_name='speciality',
            name='object',
            field=models.ManyToManyField(related_name='specialityobject', verbose_name='\u0411\u0430\u0437\u043e\u0432\u0456 \u043d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u0456 \u0435\u043b\u0435\u043c\u0435\u043d\u0442\u0438', to='curriculum.Notion', blank=True),
        ),
        migrations.AlterField(
            model_name='specialization',
            name='goal',
            field=models.ManyToManyField(related_name='specializationgoal', verbose_name='\u0415\u043b\u0435\u043c\u0435\u043d\u0442\u0438 \u043a\u043e\u043c\u043f\u0435\u0442\u0435\u043d\u0446\u0456\u0457', to='curriculum.Notion', blank=True),
        ),
        migrations.AlterField(
            model_name='specialization',
            name='object',
            field=models.ManyToManyField(related_name='specializationobject', verbose_name='\u0411\u0430\u0437\u043e\u0432\u0456 \u043d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u0456 \u0435\u043b\u0435\u043c\u0435\u043d\u0442\u0438', to='curriculum.Notion', blank=True),
        ),
    ]
