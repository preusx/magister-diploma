# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Discipline',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, null=True, verbose_name="\u0418\u043c'\u044f")),
                ('hint', models.TextField(null=True, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441', blank=True)),
                ('description', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441', blank=True)),
            ],
            options={
                'verbose_name': '\u0414\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0430',
                'verbose_name_plural': '\u0414\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0438',
            },
        ),
        migrations.CreateModel(
            name='DisciplineCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, verbose_name="\u0418\u043c'\u044f")),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0456\u044f \u0434\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0438',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0456\u0457 \u0434\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d',
            },
        ),
        migrations.CreateModel(
            name='DisciplineRelation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(default=b'o', max_length=1, verbose_name='\u0422\u0438\u043f', choices=[(b'm', "\u041e\u0431\u043e\u0432'\u044f\u0437\u043a\u043e\u0432\u0430"), (b'o', '\u041e\u043f\u0446\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u0430')])),
            ],
            options={
                'verbose_name': "\u0417\u0432'\u044f\u0437\u043e\u043a \u0434\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d",
                'verbose_name_plural': "\u0417\u0432'\u044f\u0437\u043a\u0438 \u0434\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d",
            },
        ),
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, null=True, verbose_name="\u0418\u043c'\u044f")),
                ('hint', models.TextField(null=True, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441', blank=True)),
                ('description', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441', blank=True)),
            ],
            options={
                'verbose_name': '\u041c\u0430\u0442\u0435\u0440\u0456\u0430\u043b',
                'verbose_name_plural': '\u041c\u0430\u0442\u0435\u0440\u0456\u0430\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='MaterialType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, null=True, verbose_name="\u0418\u043c'\u044f")),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u043c\u0430\u0442\u0435\u0440\u0456\u0430\u043b\u0443',
                'verbose_name_plural': '\u0422\u0438\u043f\u0438 \u043c\u0430\u0442\u0435\u0440\u0456\u0430\u043b\u0456\u0432',
            },
        ),
        migrations.CreateModel(
            name='Notion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, verbose_name="\u0418\u043c'\u044f")),
                ('hint', models.TextField(null=True, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441', blank=True)),
            ],
            options={
                'verbose_name': '\u041d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u0430 \u043e\u0434\u0438\u043d\u0438\u0446\u044f',
                'verbose_name_plural': '\u041d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u0456 \u043e\u0434\u0438\u043d\u0438\u0446\u0456',
            },
        ),
        migrations.CreateModel(
            name='NotionType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, verbose_name="\u0418\u043c'\u044f")),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u043d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u043e\u0457 \u043e\u0434\u0438\u043d\u0438\u0446\u0456',
                'verbose_name_plural': '\u0422\u0438\u043f\u0438 \u043d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u0438\u0445 \u043e\u0434\u0438\u043d\u0438\u0446\u044c',
            },
        ),
        migrations.CreateModel(
            name='Speciality',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, null=True, verbose_name="\u0418\u043c'\u044f")),
                ('hint', models.TextField(null=True, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441', blank=True)),
                ('description', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441', blank=True)),
            ],
            options={
                'verbose_name': '\u0421\u043f\u0435\u0446\u0456\u0430\u043b\u044c\u043d\u0456\u0441\u0442\u044c',
                'verbose_name_plural': '\u0421\u043f\u0435\u0446\u0456\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u0456',
            },
        ),
        migrations.CreateModel(
            name='SpecialityToDiscipline',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('weight', models.SmallIntegerField(default=3, verbose_name='\u041a\u0456\u043b\u044c\u043a\u0456\u0441\u0442\u044c \u043a\u0440\u0435\u0434\u0438\u0442\u0456\u0432')),
                ('semester_from', models.SmallIntegerField(null=True, verbose_name='\u041f\u043e\u0447\u0430\u0442\u043a\u043e\u0432\u0438\u0439 \u0441\u0435\u043c\u0435\u0441\u0442\u0440', blank=True)),
                ('semester_to', models.SmallIntegerField(null=True, verbose_name='\u041a\u0456\u043d\u0446\u0435\u0432\u0438\u0439 \u0441\u0435\u043c\u0435\u0441\u0442\u0440', blank=True)),
            ],
            options={
                'verbose_name': "\u0417\u0432'\u044f\u0437\u043e\u043a \u0434\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0438 \u0437\u0456 \u0441\u043f\u0435\u0446\u0456\u0430\u043b\u044c\u043d\u0456\u0441\u0442\u044e",
                'verbose_name_plural': "\u0417\u0432'\u044f\u0437\u043a\u0438 \u0434\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0438 \u0437\u0456 \u0441\u043f\u0435\u0446\u0456\u0430\u043b\u044c\u043d\u0456\u0441\u0442\u044e",
            },
        ),
        migrations.CreateModel(
            name='Specialization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, null=True, verbose_name="\u0418\u043c'\u044f")),
                ('hint', models.TextField(null=True, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441', blank=True)),
                ('description', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441', blank=True)),
            ],
            options={
                'verbose_name': '\u0421\u043f\u0435\u0446\u0456\u0430\u043b\u0456\u0437\u0430\u0446\u0456\u044f',
                'verbose_name_plural': '\u0421\u043f\u0435\u0446\u0456\u0430\u043b\u0456\u0437\u0430\u0446\u0456\u0457',
            },
        ),
        migrations.CreateModel(
            name='SpecializationToDiscipline',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(default=b'o', max_length=1, verbose_name='\u0422\u0438\u043f', choices=[(b'm', '\u0412\u0438\u0431\u0456\u0440 \u0412\u041d\u0417'), (b'o', '\u0412\u0438\u0431\u0456\u0440 \u0441\u0442\u0443\u0434\u0435\u043d\u0442\u0430')])),
                ('weight', models.SmallIntegerField(default=3, verbose_name='\u041a\u0456\u043b\u044c\u043a\u0456\u0441\u0442\u044c \u043a\u0440\u0435\u0434\u0438\u0442\u0456\u0432')),
                ('semester_from', models.SmallIntegerField(null=True, verbose_name='\u041f\u043e\u0447\u0430\u0442\u043a\u043e\u0432\u0438\u0439 \u0441\u0435\u043c\u0435\u0441\u0442\u0440', blank=True)),
                ('semester_to', models.SmallIntegerField(null=True, verbose_name='\u041a\u0456\u043d\u0446\u0435\u0432\u0438\u0439 \u0441\u0435\u043c\u0435\u0441\u0442\u0440', blank=True)),
            ],
            options={
                'verbose_name': "\u0417\u0432'\u044f\u0437\u043e\u043a \u0434\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0438 \u0437\u0456 \u0441\u043f\u0435\u0446\u0456\u0430\u043b\u0456\u0437\u0430\u0446\u0456\u0454\u044e",
                'verbose_name_plural': "\u0417\u0432'\u044f\u0437\u043a\u0438 \u0434\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0438 \u0437\u0456 \u0441\u043f\u0435\u0446\u0456\u0430\u043b\u0456\u0437\u0430\u0446\u0456\u0454\u044e",
            },
        ),
        migrations.CreateModel(
            name='UserDisciplineList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, null=True, verbose_name="\u0418\u043c'\u044f")),
                ('description', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441', blank=True)),
                ('disciplines', models.ManyToManyField(to='curriculum.Discipline')),
                ('specialization', models.ForeignKey(verbose_name='\u0421\u043f\u0435\u0446\u0456\u0430\u043b\u0456\u0437\u0430\u0446\u0456\u044f', to='curriculum.Specialization')),
            ],
            options={
                'verbose_name': '\u041a\u043e\u0440\u0438\u0441\u0442\u0438\u0432\u0430\u0446\u044c\u043a\u0438\u0439 \u0441\u043f\u0438\u0441\u043e\u043a',
                'verbose_name_plural': '\u041a\u043e\u0440\u0438\u0441\u0442\u0438\u0432\u0430\u0446\u044c\u043a\u0456 \u0441\u043f\u0438\u0441\u043a\u0438',
            },
        ),
    ]
