# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('curriculum', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='userdisciplinelist',
            name='user',
            field=models.ForeignKey(verbose_name='\u041a\u043e\u0440\u0438\u0441\u0442\u0443\u0432\u0430\u0447', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='specializationtodiscipline',
            name='category',
            field=models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0456\u044f \u0434\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d', to='curriculum.DisciplineCategory'),
        ),
        migrations.AddField(
            model_name='specializationtodiscipline',
            name='discipline',
            field=models.ForeignKey(related_name='specialization_connection', verbose_name='\u0414\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0430', to='curriculum.Discipline'),
        ),
        migrations.AddField(
            model_name='specializationtodiscipline',
            name='specialization',
            field=models.ForeignKey(verbose_name='\u0421\u043f\u0435\u0446\u0456\u0430\u043b\u0456\u0437\u0430\u0446\u0456\u044f', to='curriculum.Specialization'),
        ),
        migrations.AddField(
            model_name='specialization',
            name='goal',
            field=models.ManyToManyField(related_name='specializationgoal', verbose_name='\u041c\u0435\u0442\u0430', to='curriculum.Notion', blank=True),
        ),
        migrations.AddField(
            model_name='specialization',
            name='object',
            field=models.ManyToManyField(related_name='specializationobject', verbose_name="\u041e\u0431'\u0454\u043a\u0442", to='curriculum.Notion', blank=True),
        ),
        migrations.AddField(
            model_name='specialization',
            name='relations',
            field=models.ManyToManyField(to='curriculum.Discipline', through='curriculum.SpecializationToDiscipline'),
        ),
        migrations.AddField(
            model_name='specialization',
            name='speciality',
            field=models.ForeignKey(verbose_name='\u0421\u043f\u0435\u0446\u0456\u0430\u043b\u044c\u043d\u0456\u0441\u0442\u044c', to='curriculum.Speciality'),
        ),
        migrations.AddField(
            model_name='specialitytodiscipline',
            name='category',
            field=models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0456\u044f \u0434\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d', to='curriculum.DisciplineCategory'),
        ),
        migrations.AddField(
            model_name='specialitytodiscipline',
            name='discipline',
            field=models.ForeignKey(related_name='speciality_connection', verbose_name='\u0414\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0430', to='curriculum.Discipline'),
        ),
        migrations.AddField(
            model_name='specialitytodiscipline',
            name='speciality',
            field=models.ForeignKey(verbose_name='\u0421\u043f\u0435\u0446\u0456\u0430\u043b\u044c\u043d\u0456\u0441\u0442\u044c', to='curriculum.Speciality'),
        ),
        migrations.AddField(
            model_name='speciality',
            name='goal',
            field=models.ManyToManyField(related_name='specialitygoal', verbose_name='\u041c\u0435\u0442\u0430', to='curriculum.Notion', blank=True),
        ),
        migrations.AddField(
            model_name='speciality',
            name='object',
            field=models.ManyToManyField(related_name='specialityobject', verbose_name="\u041e\u0431'\u0454\u043a\u0442", to='curriculum.Notion', blank=True),
        ),
        migrations.AddField(
            model_name='speciality',
            name='relations',
            field=models.ManyToManyField(to='curriculum.Discipline', through='curriculum.SpecialityToDiscipline'),
        ),
        migrations.AddField(
            model_name='notion',
            name='type',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u043d\u0430\u0432\u0447\u0430\u043b\u044c\u043d\u043e\u0457 \u043e\u0434\u0438\u043d\u0438\u0446\u0456', to='curriculum.NotionType'),
        ),
        migrations.AddField(
            model_name='material',
            name='discipline',
            field=models.ForeignKey(verbose_name='\u0414\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0430', to='curriculum.Discipline'),
        ),
        migrations.AddField(
            model_name='material',
            name='required',
            field=models.ManyToManyField(to='curriculum.Material', verbose_name='\u041c\u0430\u0442\u0435\u0440\u0456\u0430\u043b\u0438 \u043f\u043e\u0442\u0440\u0456\u0431\u043d\u0456 \u0434\u043b\u044f \u0432\u0438\u0432\u0447\u0435\u043d\u043d\u044f'),
        ),
        migrations.AddField(
            model_name='material',
            name='type',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u043c\u0430\u0442\u0435\u0440\u0456\u0430\u043b\u0443', to='curriculum.MaterialType'),
        ),
        migrations.AddField(
            model_name='disciplinerelation',
            name='into',
            field=models.ForeignKey(related_name='children', verbose_name='\u0414\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0430', to='curriculum.Discipline'),
        ),
        migrations.AddField(
            model_name='disciplinerelation',
            name='out_of',
            field=models.ForeignKey(related_name='parent', verbose_name='\u0414\u0438\u0441\u0446\u0438\u043f\u043b\u0456\u043d\u0430', to='curriculum.Discipline'),
        ),
        migrations.AddField(
            model_name='discipline',
            name='goal',
            field=models.ManyToManyField(related_name='disciplinegoal', verbose_name='\u041c\u0435\u0442\u0430', to='curriculum.Notion', blank=True),
        ),
        migrations.AddField(
            model_name='discipline',
            name='object',
            field=models.ManyToManyField(related_name='disciplineobject', verbose_name="\u041e\u0431'\u0454\u043a\u0442", to='curriculum.Notion', blank=True),
        ),
        migrations.AddField(
            model_name='discipline',
            name='relations',
            field=models.ManyToManyField(to='curriculum.Discipline', through='curriculum.DisciplineRelation'),
        ),
    ]
