from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType

class Command(BaseCommand):
    help = 'Initial database setup.'

    def handle(self, *args, **options):
        group_count = Group.objects.filter(name='user').count()

        if group_count == 0:
            group = Group(name='user')
            group.save()
            group.permissions.add(Permission.objects.get(codename='add_userdisciplinelist'))
            group.permissions.add(Permission.objects.get(codename='change_userdisciplinelist'))

        group_count = Group.objects.filter(name='deanery').count()

        if group_count == 0:
            group = Group(name='deanery')
            group.save()
