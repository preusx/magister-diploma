# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings



def base_notion_connections(table):
    base = table.lower()

    class BaseNotionConnections(m.Model):
        goal = m.ManyToManyField('Notion', related_name=base+'goal', verbose_name=u'Елементи компетенцій', blank=True)
        object = m.ManyToManyField('Notion', related_name=base+'object', verbose_name=u'Базові навчальні елементи', blank=True)
        # subject = m.ManyToManyField('Notion', related_name=base+'subject', verbose_name=u'Предмет', blank=True)
        # tags = m.ManyToManyField('Notion', related_name=base+'tags', verbose_name=u'Теги', blank=True)

        class Meta:
            abstract = True

    return BaseNotionConnections



def link_to_Notion(table, type=None):
    base = table.lower()

    class NotionLink(m.Model):
        __name__ = base + type + '_to_notion'

        parent = m.ForeignKey(table, related_name=base + type + '_parent', null=False, blank=False)
        notion = m.ForeignKey(Notion, related_name=base + type + '_children', null=False, blank=False)

        class Meta:
            db_table = base + type + '_to_notion'

    return NotionLink



class Notion(m.Model):
    name = m.CharField(u'Им\'я', max_length=150, null=False, blank=False)
    hint = m.TextField(u'Короткий опис', null=True, blank=True)

    type = m.ForeignKey('NotionType', null=False, blank=False, verbose_name=u'Тип навчального елементу')

    def __unicode__(self):
        return self.type.name + ': ' + self.name

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Навчальний елемент'
        verbose_name_plural = u'Навчальні елементи'