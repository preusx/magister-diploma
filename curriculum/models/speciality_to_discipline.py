# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings


class SpecialityToDiscipline(m.Model):
    speciality = m.ForeignKey('Speciality', null=False, blank=False, verbose_name=u'Спеціальність')
    discipline = m.ForeignKey('Discipline', null=False, blank=False, verbose_name=u'Дисципліна', related_name='speciality_connection')

    MANDATORY = 'm'
    OPTIONAL = 'o'

    TYPE_CHOICES = (
            (MANDATORY, u'Обязательная'),
            (OPTIONAL, u'Опциональная'),
        )

    # type = m.CharField(max_length=1, choices=TYPE_CHOICES, null=False, default=OPTIONAL, blank=False, verbose_name=u'Тип')
    category = m.ForeignKey('DisciplineCategory', null=False, blank=False, verbose_name=u'Категорія дисциплін')

    weight = m.SmallIntegerField(u'Кількість кредитів', null=False, default=3, blank=False)
    semester_from = m.SmallIntegerField(u'Початковий семестр', null=True, blank=True)
    semester_to = m.SmallIntegerField(u'Кінцевий семестр', null=True, blank=True)

    def type(self):
        return self.MANDATORY

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Зв\'язок дисципліни зі спеціальністю'
        verbose_name_plural = u'Зв\'язки дисципліни зі спеціальністю'