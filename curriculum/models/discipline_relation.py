# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings

from .discipline import Discipline


class DisciplineRelation(m.Model):
    out_of = m.ForeignKey(Discipline, related_name='parent', null=False, blank=False, verbose_name=u'Дисципліна')
    into = m.ForeignKey(Discipline, related_name='children', null=False, blank=False, verbose_name=u'Дисципліна')

    MANDATORY = 'm'
    OPTIONAL = 'o'

    TYPE_CHOICES = (
            (MANDATORY, u'Обов\'язкова'),
            (OPTIONAL, u'Опциональна'),
        )

    type = m.CharField(max_length=1, choices=TYPE_CHOICES, null=False, default=OPTIONAL, blank=False, verbose_name=u'Тип')

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Зв\'язок дисциплін'
        verbose_name_plural = u'Зв\'язки дисциплін'