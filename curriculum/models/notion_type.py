# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings


class NotionType(m.Model):
    name = m.CharField(u'Им\'я', max_length=150, null=False, blank=False)

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Тип навчального елементу'
        verbose_name_plural = u'Типи навчальних елементів'