# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings


class UserDisciplineList(m.Model):
    name = m.CharField(u'Им\'я', max_length=150, null=True, blank=False)
    description = m.TextField(u'Опис', null=True, blank=True)

    specialization = m.ForeignKey('Specialization', null=False, blank=False, verbose_name=u'Спеціалізація')
    user = m.ForeignKey(settings.AUTH_USER_MODEL, null=False, blank=False, verbose_name=u'Користувач')

    disciplines = m.ManyToManyField('Discipline')

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Користивацький список'
        verbose_name_plural = u'Користивацькі списки'