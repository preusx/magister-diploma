# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings


class DisciplineCategory(m.Model):
    name = m.CharField(u'Им\'я', max_length=150, null=False, blank=False)

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Категорія дисципліни'
        verbose_name_plural = u'Категорії дисциплін'