# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings


class MaterialType(m.Model):
    name = m.CharField(u'Им\'я', max_length=150, null=True, blank=False)

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Тип матеріалу'
        verbose_name_plural = u'Типи матеріалів'