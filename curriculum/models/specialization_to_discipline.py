# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings


class SpecializationToDiscipline(m.Model):
    specialization = m.ForeignKey('Specialization', null=False, blank=False, verbose_name=u'Спеціалізація')
    discipline = m.ForeignKey('Discipline', null=False, blank=False, verbose_name=u'Дисципліна', related_name='specialization_connection')

    MANDATORY = 'm'
    OPTIONAL = 'o'

    TYPE_CHOICES = (
            (MANDATORY, u'Вибір ВНЗ'),
            (OPTIONAL, u'Вибір студента'),
        )

    type = m.CharField(max_length=1, choices=TYPE_CHOICES, null=False, default=OPTIONAL, blank=False, verbose_name=u'Тип')
    category = m.ForeignKey('DisciplineCategory', null=False, blank=False, verbose_name=u'Категорія дисциплін')

    weight = m.SmallIntegerField(u'Кількість кредитів', null=False, default=3, blank=False)
    semester_from = m.SmallIntegerField(u'Початковий семестр', null=True, blank=True)
    semester_to = m.SmallIntegerField(u'Кінцевий семестр', null=True, blank=True)

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Зв\'язок дисципліни зі спеціалізацією'
        verbose_name_plural = u'Зв\'язки дисципліни зі спеціалізацією'