# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings

from .specialization_to_discipline import SpecializationToDiscipline
from .notion import base_notion_connections



class Specialization(base_notion_connections('Specialization'), m.Model):
    name = m.CharField(u'Им\'я', max_length=150, null=True, blank=False)
    hint = m.TextField(u'Короткий опис', null=True, blank=True)
    description = m.TextField(u'Опис', null=True, blank=True)

    speciality = m.ForeignKey('Speciality', null=False, blank=False, verbose_name=u'Спеціальність')
    relations = m.ManyToManyField('Discipline', through='SpecializationToDiscipline')

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Спеціалізація'
        verbose_name_plural = u'Спеціалізації'