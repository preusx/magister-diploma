# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings

from .speciality_to_discipline import SpecialityToDiscipline
from .notion import base_notion_connections


class Speciality(base_notion_connections('Speciality'), m.Model):
    name = m.CharField(u'Им\'я', max_length=150, null=True, blank=False)
    hint = m.TextField(u'Короткий опис', null=True, blank=True)
    description = m.TextField(u'Опис', null=True, blank=True)

    relations = m.ManyToManyField('Discipline', through='SpecialityToDiscipline')

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Спеціальність'
        verbose_name_plural = u'Спеціальності'