# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings



class Material(m.Model):
    name = m.CharField(u'Им\'я', max_length=150, null=True, blank=False)
    hint = m.TextField(u'Короткий опис', null=True, blank=True)
    description = m.TextField(u'Опис', null=True, blank=True)

    discipline = m.ForeignKey('Discipline', null=False, blank=False, verbose_name=u'Дисципліна')
    type = m.ForeignKey('MaterialType', null=False, blank=False, verbose_name=u'Тип матеріалу')
    required = m.ManyToManyField(u'self', symmetrical=False, verbose_name=u'Матеріали потрібні для вивчення')


    def __unicode__(self):
        return self.name

    class Meta:
        app_label = 'curriculum'
        verbose_name = u'Матеріал'
        verbose_name_plural = u'Матеріали'