# -*- coding: utf-8 -*-

import os
import sys

dirname = os.path.dirname(os.path.abspath(__file__))
sys.path.append(dirname)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")

from django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise

os.environ['HTTPS'] = "on"

application = get_wsgi_application()
application = DjangoWhiteNoise(application)